package br.ufc.petrigame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "PetriGame";
		cfg.useGL20 = false;
		cfg.width = 320*2;
		cfg.height = 240*2;
		
		new LwjglApplication(new PetriGame(), cfg);
	}
}
