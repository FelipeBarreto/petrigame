/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package petrigameserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.badlogic.gdx.Gdx;

import br.ufc.petrigame.multiplayer.ServerHandler;

/**
 *
 * @author FelipeDesk
 */
public class PetriGameServer {
    private static final int SERVER_PORT = 10555;
    private boolean running = true;
    
    public PetriGameServer(){
    }
    
    public void start(){
    	ServerSocket serverSocket = null; 
    	try {
    		serverSocket = new ServerSocket(SERVER_PORT);
            new Thread(new ServerDiscovery()).start();
            Gdx.app.log("Server", "LocalServer Online");
            ServerHandler.setLocalHost(true);
            while(running){
                new Thread(new ProcessClient(serverSocket.accept())).start();
            }
        } catch (IOException ex) {
        	Gdx.app.log("Server", "Cannot create LocalServer, Finding another server");
        	Gdx.app.log("Server", "conn3");
        	ServerHandler.connectMutex = false;
        	ServerHandler.getInstance(null).connect();
            running = false;
        }
        finally{
          try {
              if(serverSocket != null)
                serverSocket.close();
          } catch (IOException ex) {
              Logger.getLogger(PetriGameServer.class.getName()).log(Level.SEVERE, null, ex);
          }
        }
    }
}
