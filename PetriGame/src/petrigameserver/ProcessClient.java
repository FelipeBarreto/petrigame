/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package petrigameserver;

import br.ufc.petrigame.messages.ClientMessage;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FelipeDesk
 */
public class ProcessClient implements Runnable{

    private final Socket client;
    private InputStream is;
    private OutputStream os;
    
    private ClientMessage message;
    
    private static PetriUpdater updater;
    
    public ProcessClient(Socket client) {
        
        this.client = client;
        try {
            is = client.getInputStream();  
            os = client.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(ProcessClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Override
    public void run() {
        
        updater = PetriUpdater.getInstance();
        if(!updater.isRunning()){
            new Thread(updater).start();
        }
        
        ObjectInputStream input = null;   
        ObjectOutputStream output = null;   
        try {
            input = new ObjectInputStream(is);
            output = new ObjectOutputStream(os);
        } catch (IOException ex) {
            Logger.getLogger(ProcessClient.class.getName()).log(Level.SEVERE, null, ex);
        }
  
        try {
            
            message = (ClientMessage) input.readObject();
            
            updater.registerClient(message, output);
            
            while(!client.isClosed()){
                message = (ClientMessage) input.readObject();
                updater.updateClient(output, message);  
            }
            
            
        } catch (IOException ex) {
            //Logger.getLogger(ProcessClient.class.getName()).log(Level.SEVERE, null, ex);
            updater.unRegisterClient(message.getGameName(), output);
        }  
        catch (ClassNotFoundException ex){
        	updater.unRegisterClient(message.getGameName(), output);
        }

        finally{
            try { 
                client.close();
            } catch (IOException ex) {
                Logger.getLogger(ProcessClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void updateLocal(ClientMessage message){
    	if(updater != null)
    		updater.updateLocalClient(message);
    }

}
