/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package petrigameserver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.badlogic.gdx.Gdx;

import br.ufc.petrigame.multiplayer.ServerHandler;

/**
 *
 * @author felipebarreto
 */
public class ServerDiscovery implements Runnable{
    
    private static final int PORT = 10554;
    private boolean running = false;

    @Override
    public void run() {
        running = true;
        DatagramSocket s = null;
        try {
        	s = new DatagramSocket(PORT);
            s.setBroadcast(true);         
            
            byte sendBuf[] = new byte[1024];
            DatagramPacket pack2 = new DatagramPacket(sendBuf, sendBuf.length);

            while(running){
            	
                s.receive(pack2);
                String sss = new String(pack2.getData());
                if(sss.contains("PetriServer Ip Request")){

                    System.out.println("Received data from: " + pack2.getAddress().toString() +
                                ":" + pack2.getPort() + " with length: " +
                                pack2.getLength());
                    System.out.write(pack2.getData(),0,pack2.getLength());
                    System.out.println();

                    // Fill the buffer with some data
                    String ss = InetAddress.getLocalHost().getHostAddress();
                    byte buf[] = ss.getBytes();


                    // Create a DatagramPacket 
                    DatagramPacket pack = new DatagramPacket(buf, buf.length,
                                                             pack2.getAddress(), pack2.getPort());
                    // Do a send.
                    s.send(pack);

                }
            }
        } 
        catch (IOException e) {
        	Gdx.app.log("Server", "Unable to Create ServerDiscovery");
        }

    }
    
}
