/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package petrigameserver;

import br.ufc.petrigame.messages.ClientMessage;
import br.ufc.petrigame.messages.ServerMessage;
import br.ufc.petrigame.multiplayer.ServerHandler;
import br.ufc.petrigame.multiplayer.ServerListener;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Felipe
 */
public class PetriUpdater implements Runnable{
    
    private static PetriUpdater instance;

    private final Map<String,CopyOnWriteArrayList<ObjectOutputStream>> clients;
    private final Map<ObjectOutputStream, ClientMessage> clientsState;
    
    private boolean hadClients = false;
    private ClientMessage localMessage;
    
    private static boolean running = false;
    
    private PetriUpdater(){    
        clients = Collections.synchronizedMap(new HashMap<String, CopyOnWriteArrayList<ObjectOutputStream>>());
        clientsState = Collections.synchronizedMap(new HashMap<ObjectOutputStream, ClientMessage>());
    }
    
    public synchronized static PetriUpdater getInstance(){
        if(instance == null){
            instance = new PetriUpdater();
        }
        return instance;
    }
    
    
    public synchronized void registerClient(ClientMessage message, ObjectOutputStream client){

        String game = message.getGameName();
        if(clients.containsKey(game)){
            clients.get(game).add(client);
        }
        else{
            CopyOnWriteArrayList<ObjectOutputStream> tempClients = new CopyOnWriteArrayList<ObjectOutputStream>();
            tempClients.add(client);
            clients.put(game, tempClients);
        }

        updateClient(client, message);
        hadClients = true;

    }
    
    public synchronized void unRegisterClient(String game, ObjectOutputStream client){
        clients.get(game).remove(client);
        if(clients.get(game).isEmpty()){
            clients.remove(game);
        }
        clientsState.remove(client);
        
    }
    
    public synchronized void updateClient(ObjectOutputStream client, ClientMessage game){
        
        Map<String, CopyOnWriteArrayList<ObjectOutputStream>> clientsTemp = Collections.unmodifiableMap(clients);
        
        for(String key : clientsTemp.keySet()){
            if(clientsTemp.get(key).contains(client) && !key.equals(game.getGameName())){
                clients.get(key).remove(client);
                if(clients.get(key).isEmpty()){
                    clients.remove(key);
                }
                
                if(clientsTemp.containsKey(game.getGameName())){
                    clients.get(game.getGameName()).add(client);
                }
                else{
                    CopyOnWriteArrayList<ObjectOutputStream> temp = new CopyOnWriteArrayList<ObjectOutputStream>();
                    temp.add(client);
                    clients.put(game.getGameName(), temp);
                }
                break;
            }
        }

        clientsState.put(client, game);
    }
    
    public void updateLocalClient(ClientMessage message){
    	this.localMessage = message;
    }
    
    public synchronized boolean isRunning(){
        // solucao estranha pra concorrencia :D
        if(!running){
            running = true;
            return false;
        }
        else
            return running;
    }
    
    @Override
    public void run() {
        running = true;
        
        ServerMessage message = new ServerMessage();
        System.out.println("Broadcast criado");
        while(!clients.isEmpty() || !hadClients){
         
            Map<String,CopyOnWriteArrayList<ObjectOutputStream>> clientsTemp = new HashMap<String, CopyOnWriteArrayList<ObjectOutputStream>>(clients);
            Map<ObjectOutputStream, ClientMessage> clientsStateTemp = new HashMap<ObjectOutputStream, ClientMessage>(clientsState);
            
            for(String game : clientsTemp.keySet()){
                message.clear();
                for(Iterator<ObjectOutputStream> it = clientsTemp.get(game).iterator(); it.hasNext();){
                    ObjectOutputStream client = it.next();
                    String s = client.toString();
                    message.add(s, clientsStateTemp.get(client));
                }
                
                if(ServerHandler.isLocalHost()){
            		ServerListener.getInstance().updateLocal(new HashMap<String, ClientMessage>(message.getPlayersGame()));
                }
                
                if(localMessage != null)
                	message.add("local", localMessage);
                
                for(Iterator<ObjectOutputStream> it = clientsTemp.get(game).iterator(); it.hasNext();){
                    ObjectOutputStream client = it.next();
                    message.remove(client.toString());
                    try {
                        client.writeObject(message);
                        client.reset();
                    } catch (IOException ex) {                        

                    }
                    message.add(client.toString(), clientsStateTemp.get(client));
                }
            }
            
            try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        }
        
        ServerListener.getInstance().updateLocal(new HashMap<String, ClientMessage>());
        running = false;
        instance = null;
    }
    
}
