package br.ufc.petrigame.screens;

import br.ufc.petrigame.gameworld.GameRenderer;
import br.ufc.petrigame.gameworld.GameWorld;
import br.ufc.petrigame.helpers.AssetLoader;
import br.ufc.petrigame.helpers.InputHandler;
import br.ufc.petrigame.multiplayer.ServerHandler;
import br.ufc.petrigame.netObjects.PlayGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class GameScreen implements Screen{
	
	private GameWorld world;
	private GameRenderer renderer;
	private PlayGame playGame;
	

	public GameScreen(PlayGame playGame, GameWorld world, GameRenderer renderer){
		this.playGame = playGame;
		this.world = world;
		this.renderer = renderer;
	}
	

	@Override
	public void render(float delta) {
		world.update(delta);
		renderer.render();	
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {

		playGame.init();
	
		
		float gameWidth = 400.0f;
		float gameHeight = 300.0f;
		if(!world.useAccel())
			Gdx.input.setInputProcessor(new InputHandler(gameWidth / Gdx.graphics.getWidth() , 
													 gameHeight / Gdx.graphics.getHeight(),
													 world));
		
	}

	@Override
	public void hide() {
		ServerHandler.getInstance(world).dispose();
	}

	@Override
	public void pause() {
		world.pause();
		
	}

	@Override
	public void resume() {
		world.resume();
		
	}

	@Override
	public void dispose() {
		AssetLoader.dispose();
		ServerHandler.getInstance(world).dispose();
	}

}
