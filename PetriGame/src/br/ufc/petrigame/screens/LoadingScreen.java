package br.ufc.petrigame.screens;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import br.ufc.petrigame.PetriGame;
import br.ufc.petrigame.gameworld.GameRenderer;
import br.ufc.petrigame.gameworld.GameWorld;
import br.ufc.petrigame.helpers.AssetLoader;
import br.ufc.petrigame.helpers.GameLoader;
import br.ufc.petrigame.multiplayer.ServerHandler;
import br.ufc.petrigame.netObjects.PlayGame;
import br.ufc.petrigame.netObjects.RedeHierarquica;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class LoadingScreen implements Screen{
	
	private PetriGame myGame;
	public static String gameName;
	private SpriteBatch batcher;
	private ExecutorService executor;
	private OrthographicCamera camera;
	private boolean useAccel;
	
	private Vector2 position;
	private float speedX = 4;
	private float speedY = 3;
	
	public LoadingScreen(String gameName, PetriGame myGame, boolean useAccel){
		this.myGame = myGame;
		LoadingScreen.gameName = gameName;
		this.useAccel = useAccel;
	}

	@Override
	public void show() {
		camera = new OrthographicCamera();
		camera.setToOrtho(true, 400, 300);
		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(camera.combined);
		executor = Executors.newSingleThreadExecutor();
		executor.execute(new Loader(gameName, myGame));
		
		position = new Vector2(200, 150);
	}


	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0 / 255.0f, 255 / 255.0f, 0 / 255.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		batcher.begin();
		batcher.enableBlending();
		batcher.draw(AssetLoader.loadingScreen, position.x, position.y);
		batcher.end();
		
		position.x += speedX;
		position.y += speedY;
		
		if(position.x > 400){
			position.x -= 400;
		}
		if(position.y > 300){
			position.y -= 300;
		}
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
	private class Loader implements Runnable{
		private String gameName;
		private PetriGame myGame;
		private br.ufc.petrigame.netObjects.Game cpnGame;
		private PlayGame playGame;
		private GameWorld world;
		private GameRenderer renderer;
		
		public Loader(String gameName, PetriGame myGame){
			this.gameName = gameName;
			this.myGame = myGame;
		}

		@Override
		public void run() {
			cpnGame = GameLoader.load("data/Games/" + gameName);
			playGame = new PlayGame();
			playGame.setRedeProperties(cpnGame.getRedeProperties());
			playGame.setRedeHierarquica(RedeHierarquica.getRedeHierarquica(playGame.getRedeProperties()));
			world = new GameWorld(playGame, useAccel);
			renderer = new GameRenderer(world);
			ServerHandler serverHandler = ServerHandler.getInstance(world);
        	Gdx.app.log("Server", "conn2");
			serverHandler.connect();
			
			Gdx.app.postRunnable(new Runnable(){

				@Override
				public void run() {
					AssetLoader.loadGame(cpnGame, "data/Games/" + gameName);
					myGame.setScreen(new GameScreen(playGame, world, renderer));
				}
				
			});
			
		}	
		
	}

}
