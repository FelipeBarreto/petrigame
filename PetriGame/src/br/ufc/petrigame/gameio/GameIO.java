package br.ufc.petrigame.gameio;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.badlogic.gdx.Gdx;

import br.ufc.petrigame.netObjects.Audio;
import br.ufc.petrigame.netObjects.EstadoLugar;
import br.ufc.petrigame.netObjects.FactoryPlayMp3Interface;
import br.ufc.petrigame.netObjects.Game;
import br.ufc.petrigame.netObjects.LugarProperties;
import br.ufc.petrigame.netObjects.RedeProperties;
import br.ufc.petrigame.netObjects.TransicaoProperties;

public class GameIO 
{
	private static final String diretoryFig = "figures";
	private static final String diretoryMusic = "musics";
	
	public GameIO()
	{		
	}
	
	public void saveGame( Game game, String local ) throws FileNotFoundException, IOException
	{
		File fileDir = new File( local + System.getProperty( "file.separator" ) + game.getNome() );
		File file = new File( local + System.getProperty( "file.separator" ) + game.getNome() + 
			System.getProperty( "file.separator" ) + game.getNome() + ".properties");
		
		// copia as figuras e audios para a pasta do jogo
		for( RedeProperties r : game.getRedesPropertiesList() )
		{
			copyMusic( fileDir, r.getMusicaMundo() );
			copyFigures( fileDir, r );
			
			// copia os efeitos sonoros das transi��es
			for( TransicaoProperties t : r.getTransicoesPropertiesList() )
				copyMusic( fileDir, t.getEfeitoSonoro() );
		} // fim de for
		
		if( !fileDir.exists() )
			fileDir.mkdirs();
		
		ObjectOutputStream objectOutputStream = new ObjectOutputStream( new FileOutputStream( file ) );		
		objectOutputStream.writeObject( game );
		objectOutputStream.close();
	} // fim de m�todo
	
	private void copyMusic( File local, Audio audio ) throws IOException
	{
		if( audio == null || audio.getAudioPath() == null || audio.getAudioPath().equals( "" ) )
			return;
			
		File fileDestino = new File( local.getAbsolutePath() + System.getProperty( "file.separator" ) + diretoryMusic );
		
		if( !fileDestino.exists() )
			fileDestino.mkdirs();
		
		// copia a figura do estado
		File origem = new File( audio.getAudioPath() );
		File destino = new File( 
			fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( audio.getAudioPath() ) );
					
		copy( origem, destino );
	} // fim de m�todo
		
	private void copyFigures( File local, RedeProperties redeProperties ) throws IOException
	{
		File fileDestino = new File( local.getAbsolutePath() + System.getProperty( "file.separator" ) + diretoryFig );
		
		if( !fileDestino.exists() )
			fileDestino.mkdirs();
		
		// copia as imagens dos estados dos lugares e personagens
		for( LugarProperties lugarProperties : redeProperties.getLugaresPropertiesList() )
		{
			for( EstadoLugar estadoLugar : lugarProperties.getEstadosPossiveisLugar() )
			{
				// copia a figura do estado
				File origem = new File( estadoLugar.getFigura() );
				File destino = new File( 
					fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getFigura() ) );
							
				copy( origem, destino );
				
				// copia a figura do personagem1
				if( estadoLugar.getPersonagemPresente1() != null )
				{
					// figura default
					origem = new File( estadoLugar.getPersonagemPresente1().getFiguraDefault() );
					destino = new File( 
						fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraDefault() ) );
					copy( origem, destino );
					
					// figura cima1
					if( estadoLugar.getPersonagemPresente1().getFiguraCima1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraCima1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraCima1() ) );
						copy( origem, destino );
					} // fim de if
					// figura cima2
					if( estadoLugar.getPersonagemPresente1().getFiguraCima2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraCima2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraCima2() ) );
						copy( origem, destino );
					} // fim de if
					// figura cima3
					if( estadoLugar.getPersonagemPresente1().getFiguraCima3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraCima3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraCima3() ) );
						copy( origem, destino );
					} // fim de if					
					// figura direita1
					if( estadoLugar.getPersonagemPresente1().getFiguraDireita1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraDireita1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraDireita1() ) );
						copy( origem, destino );
					} // fim de if					
					// figura direita2
					if( estadoLugar.getPersonagemPresente1().getFiguraDireita2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraDireita2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraDireita2() ) );
						copy( origem, destino );
					} // fim de if			
					// figura direita3
					if( estadoLugar.getPersonagemPresente1().getFiguraDireita3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraDireita3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraDireita3() ) );
						copy( origem, destino );
					} // fim de if			
					// figura baixo1
					if( estadoLugar.getPersonagemPresente1().getFiguraBaixo1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraBaixo1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraBaixo1() ) );
						copy( origem, destino );
					} // fim de if			
					// figura baixo2
					if( estadoLugar.getPersonagemPresente1().getFiguraBaixo2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraBaixo2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraBaixo2() ) );
						copy( origem, destino );
					} // fim de if			
					// figura baixo3
					if( estadoLugar.getPersonagemPresente1().getFiguraBaixo3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraBaixo3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraBaixo3() ) );
						copy( origem, destino );
					} // fim de if			
					// figura esquerda1
					if( estadoLugar.getPersonagemPresente1().getFiguraEsquerda1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraEsquerda1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraEsquerda1() ) );
						copy( origem, destino );
					} // fim de if			
					// figura esquerda2
					if( estadoLugar.getPersonagemPresente1().getFiguraEsquerda2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraEsquerda2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraEsquerda2() ) );
						copy( origem, destino );
					} // fim de if			
					// figura esquerda3
					if( estadoLugar.getPersonagemPresente1().getFiguraEsquerda3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente1().getFiguraEsquerda3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente1().getFiguraEsquerda3() ) );
						copy( origem, destino );
					} // fim de if
				} // fim de if

				// copia a figura do personagem2
				if( estadoLugar.getPersonagemPresente2() != null )
				{
					// figura default
					origem = new File( estadoLugar.getPersonagemPresente2().getFiguraDefault() );
					destino = new File( 
						fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraDefault() ) );
					copy( origem, destino );
					
					// figura cima1
					if( estadoLugar.getPersonagemPresente2().getFiguraCima1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraCima1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraCima1() ) );
						copy( origem, destino );
					} // fim de if
					// figura cima2
					if( estadoLugar.getPersonagemPresente2().getFiguraCima2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraCima2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraCima2() ) );
						copy( origem, destino );
					} // fim de if
					// figura cima3
					if( estadoLugar.getPersonagemPresente2().getFiguraCima3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraCima3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraCima3() ) );
						copy( origem, destino );
					} // fim de if					
					// figura direita1
					if( estadoLugar.getPersonagemPresente2().getFiguraDireita1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraDireita1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraDireita1() ) );
						copy( origem, destino );
					} // fim de if					
					// figura direita2
					if( estadoLugar.getPersonagemPresente2().getFiguraDireita2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraDireita2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraDireita2() ) );
						copy( origem, destino );
					} // fim de if			
					// figura direita3
					if( estadoLugar.getPersonagemPresente2().getFiguraDireita3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraDireita3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraDireita3() ) );
						copy( origem, destino );
					} // fim de if			
					// figura baixo1
					if( estadoLugar.getPersonagemPresente2().getFiguraBaixo1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraBaixo1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraBaixo1() ) );
						copy( origem, destino );
					} // fim de if			
					// figura baixo2
					if( estadoLugar.getPersonagemPresente2().getFiguraBaixo2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraBaixo2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraBaixo2() ) );
						copy( origem, destino );
					} // fim de if			
					// figura baixo3
					if( estadoLugar.getPersonagemPresente2().getFiguraBaixo3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraBaixo3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraBaixo3() ) );
						copy( origem, destino );
					} // fim de if			
					// figura esquerda1
					if( estadoLugar.getPersonagemPresente2().getFiguraEsquerda1() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraEsquerda1() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraEsquerda1() ) );
						copy( origem, destino );
					} // fim de if			
					// figura esquerda2
					if( estadoLugar.getPersonagemPresente2().getFiguraEsquerda2() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraEsquerda2() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraEsquerda2() ) );
						copy( origem, destino );
					} // fim de if			
					// figura esquerda3
					if( estadoLugar.getPersonagemPresente2().getFiguraEsquerda3() != null )
					{
						origem = new File( estadoLugar.getPersonagemPresente2().getFiguraEsquerda3() );
						destino = new File( 
							fileDestino.getAbsolutePath() + System.getProperty( "file.separator" ) + getName( estadoLugar.getPersonagemPresente2().getFiguraEsquerda3() ) );
						copy( origem, destino );
					} // fim de if
				} // fim de if
			} // fim de for interno
		} // fim de for
	} // fim de m�todo
	
	private void copy( File origem, File destino ) throws IOException
	{
		int lidos;
		byte[] buffer = new byte[ 2048 ];
		
		if( origem.getAbsolutePath().equalsIgnoreCase( destino.getAbsolutePath() ) )
			return;

		DataInputStream input = new DataInputStream( new FileInputStream( origem ) );
		DataOutputStream output = new DataOutputStream( new FileOutputStream( destino ) );
		do
		{
			lidos = input.read( buffer );
			if( lidos != -1 )
				output.write( buffer, 0, lidos );
		} while( lidos != -1 );
		input.close();
		output.close();
	} // fim de m�todo
	
	private String getName( String path )
	{		
		String name = "";		
		char[] chars = path.toCharArray();
		
		for( int i = chars.length - 1; i >= 0 && chars[ i ] != '/' && chars[ i ] != '\\'; i-- )
			name = chars[ i ] + name;
		
		return name;
	} // fim de m�todo
	
	public Game carregarGame( String local ) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		return this.carregarGame( local, null );
	} // fim de m�todo
	
	public Game carregarGame( String local, FactoryPlayMp3Interface factoryPlayMp3Interface ) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		char separator = "/".charAt(0);
		
		// recupera o nome do jogo
		String nameGame = "";
		char[] chars = local.toCharArray();
		for( int i = chars.length - 1; i >= 0 && chars[ i ] != separator; i-- )
			nameGame = chars[ i ] + nameGame;
		
		InputStream file = Gdx.files.internal(local + separator + nameGame + ".properties").read();
		
		if( file == null) 
			return null;
		
		Game game;
		ObjectInputStream objectInputStream = new ObjectInputStream( file );
		game = ( Game ) objectInputStream.readObject();
		objectInputStream.close();

		// ajusta referencias de figuras dos estados de lugares e personagens
		for( RedeProperties r : game.getRedesPropertiesList() )
		{
			if( r.getMusicaMundo() != null && r.getMusicaMundo().getAudioPath() != null && !r.getMusicaMundo().equals( "" ) )
			{
				r.getMusicaMundo().setAudioPath( local + separator + diretoryMusic + separator + getName( r.getMusicaMundo().getAudioPath() ) );
				if( factoryPlayMp3Interface != null )
					r.getMusicaMundo().setPlayMp3Interface( factoryPlayMp3Interface.getPlayMp3Interface( r.getMusicaMundo().getAudioPath() ) );
			} // fim de if
						
			for( TransicaoProperties transicaoProperties : r.getTransicoesPropertiesList() )
			{
				if( transicaoProperties.getEfeitoSonoro() != null && transicaoProperties.getEfeitoSonoro().getAudioPath() != null &&
					!transicaoProperties.getEfeitoSonoro().getAudioPath().equals( "" ) )
				{
					transicaoProperties.getEfeitoSonoro().setAudioPath( local + separator + diretoryMusic + separator + getName( 
						transicaoProperties.getEfeitoSonoro().getAudioPath() ) );
					if( factoryPlayMp3Interface != null )
						transicaoProperties.getEfeitoSonoro().setPlayMp3Interface( factoryPlayMp3Interface.getPlayMp3Interface( 
							transicaoProperties.getEfeitoSonoro().getAudioPath() ) );					
				} // fim de if
			} // fim de for
			
			for( LugarProperties lugarProperties : r.getLugaresPropertiesList() )
			{
				for( EstadoLugar estadoLugar : lugarProperties.getEstadosPossiveisLugar() )
				{
					estadoLugar.setFigura( local + separator + diretoryFig + separator + getName( estadoLugar.getFigura() ) );
					
					if( estadoLugar.getPersonagemPresente1() != null )
					{
						estadoLugar.getPersonagemPresente1().setFiguraDefault(
							local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraDefault() ) );
						
						// cima1
						if( estadoLugar.getPersonagemPresente1().getFiguraCima1() != null )
							estadoLugar.getPersonagemPresente1().setFiguraCima1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraCima1() ) );
						// cima2
						if( estadoLugar.getPersonagemPresente1().getFiguraCima2() != null )
							estadoLugar.getPersonagemPresente1().setFiguraCima2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraCima2() ) );
						// cima3
						if( estadoLugar.getPersonagemPresente1().getFiguraCima3() != null )
							estadoLugar.getPersonagemPresente1().setFiguraCima3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraCima3() ) );
						// direita1
						if( estadoLugar.getPersonagemPresente1().getFiguraDireita1() != null )
							estadoLugar.getPersonagemPresente1().setFiguraDireita1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraDireita1() ) );
						// direita2
						if( estadoLugar.getPersonagemPresente1().getFiguraDireita2() != null )
							estadoLugar.getPersonagemPresente1().setFiguraDireita2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraDireita2() ) );
						// direita3
						if( estadoLugar.getPersonagemPresente1().getFiguraDireita3() != null )
							estadoLugar.getPersonagemPresente1().setFiguraDireita3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraDireita3() ) );
						// baixo1
						if( estadoLugar.getPersonagemPresente1().getFiguraBaixo1() != null )
							estadoLugar.getPersonagemPresente1().setFiguraBaixo1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraBaixo1() ) );
						// baixo2
						if( estadoLugar.getPersonagemPresente1().getFiguraBaixo2() != null )
							estadoLugar.getPersonagemPresente1().setFiguraBaixo2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraBaixo2() ) );
						// baixo3
						if( estadoLugar.getPersonagemPresente1().getFiguraBaixo3() != null )
							estadoLugar.getPersonagemPresente1().setFiguraBaixo3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraBaixo3() ) );
						// esquerda1
						if( estadoLugar.getPersonagemPresente1().getFiguraEsquerda1() != null )
							estadoLugar.getPersonagemPresente1().setFiguraEsquerda1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraEsquerda1() ) );
						// esquerda2
						if( estadoLugar.getPersonagemPresente1().getFiguraEsquerda2() != null )
							estadoLugar.getPersonagemPresente1().setFiguraEsquerda2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraEsquerda2() ) );
						// esquerda3
						if( estadoLugar.getPersonagemPresente1().getFiguraEsquerda3() != null )
							estadoLugar.getPersonagemPresente1().setFiguraEsquerda3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente1().getFiguraEsquerda3() ) );
					} // fim de if
					if( estadoLugar.getPersonagemPresente2() != null )
					{
						estadoLugar.getPersonagemPresente2().setFiguraDefault(
							local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraDefault() ) );
						
						// cima1
						if( estadoLugar.getPersonagemPresente2().getFiguraCima1() != null )
							estadoLugar.getPersonagemPresente2().setFiguraCima1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraCima1() ) );
						// cima2
						if( estadoLugar.getPersonagemPresente2().getFiguraCima2() != null )
							estadoLugar.getPersonagemPresente2().setFiguraCima2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraCima2() ) );
						// cima3
						if( estadoLugar.getPersonagemPresente2().getFiguraCima3() != null )
							estadoLugar.getPersonagemPresente2().setFiguraCima3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraCima3() ) );
						// direita1
						if( estadoLugar.getPersonagemPresente2().getFiguraDireita1() != null )
							estadoLugar.getPersonagemPresente2().setFiguraDireita1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraDireita1() ) );
						// direita2
						if( estadoLugar.getPersonagemPresente2().getFiguraDireita2() != null )
							estadoLugar.getPersonagemPresente2().setFiguraDireita2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraDireita2() ) );
						// direita3
						if( estadoLugar.getPersonagemPresente2().getFiguraDireita3() != null )
							estadoLugar.getPersonagemPresente2().setFiguraDireita3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraDireita3() ) );
						// baixo1
						if( estadoLugar.getPersonagemPresente2().getFiguraBaixo1() != null )
							estadoLugar.getPersonagemPresente2().setFiguraBaixo1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraBaixo1() ) );
						// baixo2
						if( estadoLugar.getPersonagemPresente2().getFiguraBaixo2() != null )
							estadoLugar.getPersonagemPresente2().setFiguraBaixo2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraBaixo2() ) );
						// baixo3
						if( estadoLugar.getPersonagemPresente2().getFiguraBaixo3() != null )
							estadoLugar.getPersonagemPresente2().setFiguraBaixo3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraBaixo3() ) );
						// esquerda1
						if( estadoLugar.getPersonagemPresente2().getFiguraEsquerda1() != null )
							estadoLugar.getPersonagemPresente2().setFiguraEsquerda1(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraEsquerda1() ) );
						// esquerda2
						if( estadoLugar.getPersonagemPresente2().getFiguraEsquerda2() != null )
							estadoLugar.getPersonagemPresente2().setFiguraEsquerda2(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraEsquerda2() ) );
						// esquerda3
						if( estadoLugar.getPersonagemPresente2().getFiguraEsquerda3() != null )
							estadoLugar.getPersonagemPresente2().setFiguraEsquerda3(
								local + separator + diretoryFig + separator + getName( estadoLugar.getPersonagemPresente2().getFiguraEsquerda3() ) );
					} // fim de if
				} // fim de for
			} // fim de for
		} // fim de for
		
		return game;
	} // fim de m�todo
	
} // fim de classe
