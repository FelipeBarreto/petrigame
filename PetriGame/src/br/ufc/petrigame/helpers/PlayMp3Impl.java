package br.ufc.petrigame.helpers;

import br.ufc.petrigame.netObjects.PlayMp3Interface;

public class PlayMp3Impl implements PlayMp3Interface{
	
	private String soundPath;
	
	public PlayMp3Impl(String path){
		setSoundPath(path);
	}

	@Override
	public void play() {
		AssetLoader.musics.get(soundPath).play();
	}

	@Override
	public void loop() {
		AssetLoader.musics.get(soundPath).play();	
		AssetLoader.musics.get(soundPath).setVolume(.8f);	
		AssetLoader.musics.get(soundPath).setLooping(true);	
	}

	@Override
	public void stop() {
		AssetLoader.musics.get(soundPath).stop();
		
	}

	public String getSoundPath() {
		return soundPath;
	}

	public void setSoundPath(String soundPath) {
		this.soundPath = soundPath;
	}

}
