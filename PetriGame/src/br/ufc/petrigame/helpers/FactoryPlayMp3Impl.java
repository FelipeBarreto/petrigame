package br.ufc.petrigame.helpers;

import br.ufc.petrigame.netObjects.FactoryPlayMp3Interface;
import br.ufc.petrigame.netObjects.PlayMp3Interface;

public class FactoryPlayMp3Impl implements FactoryPlayMp3Interface{

	@Override
	public PlayMp3Interface getPlayMp3Interface(String path) {		
		return new PlayMp3Impl( path );
	}

}
