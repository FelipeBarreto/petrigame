package br.ufc.petrigame.helpers;

import com.badlogic.gdx.Gdx;

import br.ufc.petrigame.gameworld.GameWorld;

public class AccelerometerListener {
	
	private GameWorld world;
	private float accelX;
	private float accelY;
	private float accelZ;
	

	public AccelerometerListener(GameWorld gameWorld) {
		world = gameWorld;
	}
	
	public void update(){
		
		accelX = Gdx.input.getAccelerometerX();
		accelY = Gdx.input.getAccelerometerY();
		accelZ = Gdx.input.getAccelerometerZ();

		if(accelY < -2 && accelX > 6){
			world.setDirection(GameWorld.DIRECTION_BOTTON_LEFT);
		}
		else if(accelY < -2 && accelX < 2){
			world.setDirection(GameWorld.DIRECTION_TOP_LEFT);
		}
		else if(accelY > 2 && accelX < 2){
			world.setDirection(GameWorld.DIRECTION_TOP_RIGHT);
		}
		else if(accelY > 2 && accelX > 6){
			world.setDirection(GameWorld.DIRECTION_BOTTON_RIGHT);
		}
		else if(accelX > 6){
			world.setDirection(GameWorld.DIRECTION_DOWN);
		}
		else if(accelX < 2){
			world.setDirection(GameWorld.DIRECTION_UP);
		}
		else if(accelY < -2){
			world.setDirection(GameWorld.DIRECTION_LEFT);
		}
		else if(accelY > 2){
			world.setDirection(GameWorld.DIRECTION_RIGHT);
		}
		
		else{
			world.setDirection(GameWorld.STOP_MOVING);
		}
		
		Gdx.app.log("AccelDebug", String.valueOf(accelX) + " : " + String.valueOf(accelY) + " : " + String.valueOf(accelZ));
	}

}
