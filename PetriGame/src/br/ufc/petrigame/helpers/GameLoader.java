package br.ufc.petrigame.helpers;

import br.ufc.petrigame.netObjects.Game;
import br.ufc.petrigame.netObjects.legacy.Reader;

public class GameLoader {
	
	private static Game game;
	
	public static Game load(String path){
		game = null;
		try {
			game = Reader.getGame(path,
								  new FactoryPlayMp3Impl(), 
								  new DefaultFiguresImpl() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return game;
	}

}
