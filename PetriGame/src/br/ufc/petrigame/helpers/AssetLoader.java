package br.ufc.petrigame.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

import br.ufc.petrigame.netObjects.EstadoLugar;
import br.ufc.petrigame.netObjects.Game;
import br.ufc.petrigame.netObjects.LugarProperties;
import br.ufc.petrigame.netObjects.Personagem;
import br.ufc.petrigame.netObjects.RedeProperties;

public class AssetLoader {
	
	// loader for all images
	private static Texture tx;
	public static TextureRegion logo;
	public static TextureRegion loadingScreen;
	
	public static ObjectMap<String,TextureRegion> personagem = new ObjectMap<String, TextureRegion>();
	public static ObjectMap<String,TextureRegion> lugares = new ObjectMap<String, TextureRegion>();
	public static ObjectMap<String, TextureRegion> transicoes = new ObjectMap<String, TextureRegion>();
	public static ObjectMap<String, Sound> audios = new ObjectMap<String, Sound>();
	public static ObjectMap<String, Music> musics = new ObjectMap<String, Music>();
	public static TextureRegion gameOver;

	public static void loadGame(Game game, String path){
		
		for(RedeProperties rede : game.getRedesPropertiesList()){
			for (LugarProperties places : rede.getLugaresPropertiesList()){
				for(EstadoLugar state: places.getEstadosPossiveisLugar()){
					tx = new Texture(Gdx.files.internal(state.getFigura()));
					tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
					TextureRegion tr = new TextureRegion(tx);
					//String name = state.getFigura().replace(path + "/figures/", "");
					state.setNome(state.getFigura());
					lugares.put(state.getFigura(), tr);
				}
			}
			Music music = Gdx.audio.newMusic(Gdx.files.internal(rede.getMusicaMundo().getAudioPath()));
			musics.put(rede.getMusicaMundo().getAudioPath(), music);
					
			tx = new Texture(Gdx.files.internal(rede.getTransicaoSubstituicaHorizontalPath()));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			TextureRegion tr = new TextureRegion(tx);
			transicoes.put(rede.getTransicaoSubstituicaHorizontalPath(), tr);
			
			tx = new Texture(Gdx.files.internal(rede.getTransicaoSubstituicaVerticalPath()));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			transicoes.put(rede.getTransicaoSubstituicaVerticalPath(), tr);
		}
		
		TextureRegion tr;
		String figura;
		for(Personagem player : game.getPersonagens()){
			figura = player.getFiguraDefault();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraBaixo1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraBaixo2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraBaixo3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraCima1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraCima2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraCima3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagInfDir1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagInfDir2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagInfDir3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagInfEsq1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagInfEsq2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagInfEsq3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagSupDir1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagSupDir2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagSupDir3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagSupEsq1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagSupEsq2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDiagSupEsq3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDireita1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDireita2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraDireita3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraEsquerda1();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraEsquerda2();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
			
			figura = player.getFiguraEsquerda3();
			tx = new Texture(Gdx.files.internal(figura));
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
			tr.flip(false, true);
			player.setNome(figura);
			personagem.put(figura, tr);
		}
		
		
		tx = new Texture(Gdx.files.internal("data/gameOver.png"));
		tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		gameOver = new TextureRegion(tx);
		gameOver.flip(false, true);
		
	}
	
	public static void loadMisc(){
		tx = new Texture(Gdx.files.internal("data/logo.png"));
		tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		logo = new TextureRegion(tx);
		
		tx = new Texture(Gdx.files.internal("data/Games/resource_rede5/loading.png"));
		tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		loadingScreen = new TextureRegion(tx, 0, 0, 200, 100);
		loadingScreen.flip(false, true);
	}
	
	public static void dispose(){
		tx.dispose();
	}
	
	
}
