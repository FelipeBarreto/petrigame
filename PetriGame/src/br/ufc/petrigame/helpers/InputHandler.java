package br.ufc.petrigame.helpers;

import br.ufc.petrigame.gameworld.GameWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

public class InputHandler implements InputProcessor{
	
	private float scaleFactorX;
	private float scaleFactorY;
	
	private GameWorld world;
	
	public InputHandler(float scaleX, float scaleY, GameWorld world){
		scaleFactorX = scaleX;
		scaleFactorY = scaleY;
		
		this.world = world;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		screenX = (int) (screenX * scaleFactorX);
		screenY = (int) (screenY * scaleFactorY);
		
		if(((screenX > 15) && (screenX < 45)) && ((screenY > 230) && (screenY < 260))){
			world.setDirection(GameWorld.DIRECTION_LEFT);
		}
		
		if(((screenX > 75) && (screenX < 105)) && ((screenY > 230) && (screenY < 260))){
			world.setDirection(GameWorld.DIRECTION_RIGHT);
		}
		
		if(((screenX > 45) && (screenX < 75)) && ((screenY > 200) && (screenY < 230))){
			world.setDirection(GameWorld.DIRECTION_UP);
		}
		
		if(((screenX > 45) && (screenX < 75)) && ((screenY > 260) && (screenY < 290))){
			world.setDirection(GameWorld.DIRECTION_DOWN);
		}
		
		if(((screenX > 15) && (screenX < 45)) && ((screenY > 200) && (screenY < 230))){
			world.setDirection(GameWorld.DIRECTION_TOP_LEFT);
		}
		
		if(((screenX > 75) && (screenX < 105)) && ((screenY > 200) && (screenY < 230))){
			world.setDirection(GameWorld.DIRECTION_TOP_RIGHT);
		}
		
		if(((screenX > 15) && (screenX < 45)) && ((screenY > 260) && (screenY < 290))){
			world.setDirection(GameWorld.DIRECTION_BOTTON_LEFT);
		}
		
		if(((screenX > 75) && (screenX < 105)) && ((screenY > 260) && (screenY < 290))){
			world.setDirection(GameWorld.DIRECTION_BOTTON_RIGHT);
		}
		
		Gdx.app.log("InputDebug", String.valueOf(screenX) + ";" + String.valueOf(screenY));
		
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		world.setDirection(GameWorld.STOP_MOVING);
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
