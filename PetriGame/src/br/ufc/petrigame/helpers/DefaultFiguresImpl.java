package br.ufc.petrigame.helpers;

import br.ufc.petrigame.netObjects.legacy.DefaultFiguresInterface;

public class DefaultFiguresImpl implements DefaultFiguresInterface{

	@Override
	public String getDefaultFigure(String figure) {
		return "data/Games/figDefault/" + figure;
	}

}
