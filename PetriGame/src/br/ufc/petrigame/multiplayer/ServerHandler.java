package br.ufc.petrigame.multiplayer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Formatter;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import petrigameserver.PetriGameServer;
import petrigameserver.ProcessClient;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;

import br.ufc.petrigame.gameworld.GameWorld;
import br.ufc.petrigame.messages.ClientMessage;

public class ServerHandler {
	
	public static final int SERVER_PORT = 10555;
	private static final int DISCOVERY_PORT = 10554;
	
	private static volatile boolean connected = false;
	private static volatile boolean isLocalHost = false;
	public static volatile boolean connectMutex = false;
	public static volatile boolean updateMutex = false;
	
	private static GameWorld world;
	private static Socket server;
	private ObjectOutputStream os;
	private ExecutorService executorRun, executorCall;
	
	private static ServerHandler instance;
	
	private ServerHandler(GameWorld world){	

		ServerHandler.world = world;
		executorRun = Executors.newSingleThreadExecutor();
		executorCall = Executors.newSingleThreadExecutor();

	}
	
	public static ServerHandler getInstance(GameWorld world){
		if(instance == null){
			instance = new ServerHandler(world);
		}
		return instance;
	}
	
	// GDX n�o d� suporte
	public synchronized void connect(){
		if(connectMutex == true){
			return;
		}
		connectMutex = true; // Lock
		try {
			String ServerIp = executorCall.submit(new Callable<String>(){

				@Override
				public String call() throws Exception {
					DatagramSocket serverDiscovery = new DatagramSocket();
					serverDiscovery.setBroadcast(true);
					serverDiscovery.setSoTimeout(300);
					
					String bCastIp = getLocalAddress();

					StringTokenizer tok = new StringTokenizer(bCastIp, ".");
					String tok1 = tok.nextToken();
					String tok2 = tok.nextToken();
					String tok3 = tok.nextToken();
					String tok4 = "255"; // FIXME
					
					bCastIp = tok1 + "." + tok2 + "." + tok3 + "." + tok4;

					
					byte[] message = "PetriServer Ip Request".getBytes();
					DatagramPacket packet = new DatagramPacket(message, 
															   message.length, 
															   InetAddress.getByName(bCastIp), 
															   DISCOVERY_PORT);
					
					serverDiscovery.send(packet);
					Gdx.app.log("Server", "Discovery Packet Sent");
					
					boolean searching = true;
					String ip = null;
					while(searching){

    					byte buf[] = new byte[1024];
    					DatagramPacket pack = new DatagramPacket(buf, buf.length);
						serverDiscovery.receive(pack);
						
						String sss = new String(pack.getData());
						if(!(sss.contains("PetriServer Ip Request"))){
							 ip = pack.getAddress().toString().substring(1); 
							searching = false;
							serverDiscovery.close();
						}		
						
					}
					
					return ip;
				}
				
			}).get();	
			
			if(ServerIp != null){
				try {
					connected = true;
					server = Gdx.net.newClientSocket(Net.Protocol.TCP, ServerIp, SERVER_PORT, new SocketHints());
					new Thread(new ServerListener(world, server)).start();
					os = new ObjectOutputStream(server.getOutputStream());
					Gdx.app.log("Server", "Connected");
					connectMutex = false;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Gdx.app.log("Server", "Server Offline");
		} catch (ExecutionException e) {
			if(!executorRun.isShutdown()){
				Gdx.app.log("Server", "Server Offline");
				executorRun.execute(new LocalServer());
				Gdx.app.log("Server", "Trying Start LocalServer");
			}
		}	
	}
	
	protected String getLocalAddress() {
	    try {
	        for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
	            NetworkInterface intf = en.nextElement();
	            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
	                InetAddress inetAddress = enumIpAddr.nextElement();
	                if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
	                    String ip = inetAddress.getHostAddress();
	                    return ip;
	                }
	            }
	        }
	    } catch (SocketException ex) {
	       
	    }
	    return null;
	}

	public static synchronized boolean isConnected(){
		return connected;
	}
	
	public static synchronized boolean isLocalHost(){
		return isLocalHost;
	}
	
	public static synchronized void setLocalHost(boolean state){
		isLocalHost = state;
		if(isLocalHost){
			ServerHandler.connectMutex = false;
			connected = true;
			new ServerListener(world, null);
		}
	}

	public void notifyServer(String netName, String gameName, String figure, int x, int y) {
		ClientMessage message = new ClientMessage(netName, gameName, figure, x, y);
		
		Deliver deliver = new Deliver(os, message);
		if(!isLocalHost && isConnected() && !updateMutex && !executorRun.isShutdown()){
			executorRun.execute(deliver);
		}
		else{
			ProcessClient.updateLocal(message);
		}
		
	}
	
	private class Deliver implements Runnable{

		private ClientMessage message;
		private ObjectOutputStream os;
		
		public Deliver(ObjectOutputStream os, ClientMessage message){
			this.os = os;
			this.message = message;
		}
		@Override
		public void run() {
			updateMutex = true;
			try {		
			    os.writeObject(message);
			    os.reset();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				connected = false;
	        	Gdx.app.log("Server", "conn1");
	        	int rand = new Random().nextInt(5000);
	        	Gdx.app.log("Server", String.valueOf(rand));
	        	try {
					Thread.sleep(rand);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				connect();
			}
			updateMutex = false;
		}
		
	}
	
	private class LocalServer implements Runnable{

		@Override
		public void run() {
			PetriGameServer server = new PetriGameServer();
			server.start();
			connected = true;
		}
		
	}
	
	
	public void dispose(){
		executorRun.shutdown();
		executorCall.shutdown();
		if(server != null)
			server.dispose();
	}
	
}
