package br.ufc.petrigame.multiplayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import br.ufc.petrigame.gameworld.GameWorld;
import br.ufc.petrigame.messages.ClientMessage;
import br.ufc.petrigame.messages.ServerMessage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.net.Socket;

public class ServerListener implements Runnable {
	
	private Socket server;
	private static GameWorld myWorld;
	private static ServerListener instance;
	
	public ServerListener(GameWorld world, Socket server){
		this.server = server;
		ServerListener.myWorld = world;
		instance = this;
	}
	
	public static ServerListener getInstance(){
		return instance;
	}

	@Override
	public void run() {
		ServerMessage message = new ServerMessage();
		ObjectInputStream is;
		
		try {
			if(ServerHandler.isLocalHost()){
				return;
			}
			if(ServerHandler.isConnected()){
				is = new ObjectInputStream(server.getInputStream());
			}
			else
				return;
			
			while(ServerHandler.isConnected()){
				message = (ServerMessage) is.readObject();
				Gdx.app.postRunnable(new Deliver(myWorld, message.getPlayersGame()));
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Gdx.app.postRunnable(new Deliver(myWorld, new HashMap<String, ClientMessage>()));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public synchronized void updateLocal(HashMap<String, ClientMessage> message){
		Gdx.app.postRunnable(new Deliver(myWorld, message));
	}
	private class Deliver implements Runnable{
		
		private Map<String, ClientMessage> playersGames;
		private GameWorld myWorld;
		
		public Deliver(GameWorld world, Map<String, ClientMessage> playersGames){
			this.myWorld = world;
			this.playersGames = playersGames;
		}

		@Override
		public void run() {
			//Gdx.app.log("DEBUG", String.valueOf(playersGames.size()));
			myWorld.updateMultiPlayer(playersGames);
		}
		
	}
	
}
	
