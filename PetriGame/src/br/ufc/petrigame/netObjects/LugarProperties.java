package br.ufc.petrigame.netObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Joel
 */
public class LugarProperties  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private int posLinha;
	private int posColuna;
	private List<EstadoLugar> estadosPossiveisLugar = new ArrayList<EstadoLugar>();
	private int proximoValorEstadoLugar;
	private boolean pontoEntrada;
	private boolean pontoSaida;
	
	public LugarProperties()
	{		
		proximoValorEstadoLugar = 0;
	} // fim de construtor

	public LugarProperties( String nome, int posLinha, int posColuna )
	{
		this( nome, posLinha, posColuna, false );
	} // fim de construtor
	
	public LugarProperties( String nome, int posLinha, int posColuna, boolean ehEntrada )
	{
		this.nome = nome;
		this.posLinha = posLinha;
		this.posColuna = posColuna;
	} // fim de construtor

	public EstadoLugar getEstadoInicialLugar()
	{
		for( EstadoLugar estadoLugar : estadosPossiveisLugar )
			if( estadoLugar.isInicial() )
				return estadoLugar;
		
		return null;
	} // fim de m�todo
	
	public boolean containsEstadoFinal()
	{
		for( EstadoLugar e : estadosPossiveisLugar )
			if( e.isFinal() )
				return true;
		return false;
	} // fim de m�todo
	
	public EstadoLugar getEstadoLugar( int valorFicha )
	{
		for( EstadoLugar estadoLugar : estadosPossiveisLugar )
			if( estadoLugar.getValor() == valorFicha )
				return estadoLugar;
		
		return null;
	} // fim de m�todo
	
	public int getProximoValorEstadoLugar() 
	{
		return proximoValorEstadoLugar++;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public int getPosLinha() 
	{
		return posLinha;
	}

	public void setPosLinha(int posLinha) 
	{
		this.posLinha = posLinha;
	}

	public int getPosColuna() 
	{
		return posColuna;
	}

	public void setPosColuna(int posColuna) 
	{
		this.posColuna = posColuna;
	}

	public boolean isPontoEntrada() 
	{
		return pontoEntrada;
	}

	public void setPontoEntrada(boolean pontoEntrada) 
	{
		this.pontoEntrada = pontoEntrada;
	}

	public boolean isPontoSaida() 
	{
		return pontoSaida;
	}

	public void setPontoSaida(boolean pontoSaida) 
	{
		this.pontoSaida = pontoSaida;
	}

	public List<EstadoLugar> getEstadosPossiveisLugar() 
	{
		return estadosPossiveisLugar;
	}

	public void setEstadosPossiveisLugar(List<EstadoLugar> estadosPossiveisLugar) 
	{
		this.estadosPossiveisLugar = estadosPossiveisLugar;
	}
	
} // fim de classe
