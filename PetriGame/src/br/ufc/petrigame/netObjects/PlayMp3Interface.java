package br.ufc.petrigame.netObjects;

public interface PlayMp3Interface 
{
	public void play();
	
	public void loop();
	
	public void stop();
} // fim de interface
