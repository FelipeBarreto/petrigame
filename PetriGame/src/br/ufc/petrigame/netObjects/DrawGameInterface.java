package br.ufc.petrigame.netObjects;

public interface DrawGameInterface 
{
	int DIRECTION_UP = 1;
	int DIRECTION_DOWN = 2;
	int DIRECTION_RIGHT = 3;
	int DIRECTION_LEFT = 4;
	int DIRECTION_RIGHT_TOP = 5;
	int DIRECTION_LEFT_TOP = 6;
	int DIRECTION_RIGHT_BOTTOM = 7;
	int DIRECTION_LEFT_BOTTOM = 8;
	
	void draw();
	
} // fim de interface
