package br.ufc.petrigame.netObjects.legacy;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.badlogic.gdx.Gdx;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import br.ufc.petrigame.netObjects.Audio;
import br.ufc.petrigame.netObjects.EstadoLugar;
import br.ufc.petrigame.netObjects.FactoryPlayMp3Interface;
import br.ufc.petrigame.netObjects.Game;
import br.ufc.petrigame.netObjects.LugarProperties;
import br.ufc.petrigame.netObjects.Personagem;
import br.ufc.petrigame.netObjects.PosDisparoTransicao;
import br.ufc.petrigame.netObjects.PreDisparoTransicao;
import br.ufc.petrigame.netObjects.RedeProperties;
import br.ufc.petrigame.netObjects.TransicaoProperties;

public class Reader {
	
	private String path;
	
	private boolean isFile;
	
	private DocumentBuilderFactory dbf;
	private DocumentBuilder db;
	private Document doc;
	private Element elem;
	
	private ArrayList<Element> pages;
	private ArrayList<Place> places;
	private ArrayList<Transition> transitions;
	private ArrayList<Arc> arcs;
	private ArrayList<Rede> redes;
	
	private RelacaoHierarquia relacaoHie;
	
	public Reader(String path)  throws ParserConfigurationException, SAXException, IOException {
		this.path = path;
		
		this.places = new ArrayList<Place>();
		this.transitions = new ArrayList<Transition>();
		this.arcs = new ArrayList<Arc>();
		this.redes = new ArrayList<Rede>();
		
		this.relacaoHie = new RelacaoHierarquia();
		
		dbf = DocumentBuilderFactory.newInstance();
		db = dbf.newDocumentBuilder();
		
		// Funcionar sem internet: desabilitar verificacao de pacotes
		db.setEntityResolver(new EntityResolver() {
			public InputSource resolveEntity(String publicId, String systemId)
					throws SAXException, IOException {
					System.out.println("Ignoring " + publicId + ", " + systemId);
					return new InputSource(new StringReader(""));
			}
		});
		
		isFile = Gdx.files.internal(path).exists();
		
		if (isFile) {
			doc = db.parse(Gdx.files.internal(path).read());
			elem = doc.getDocumentElement();
		}
		
		
	}

	public void read () throws Exception {
		if (isFile) {
			//ler arquivo
			
			this.pages = new ArrayList<Element>();
			
			NodeList nl = elem.getElementsByTagName("page");

			for (int i = 0; i < nl.getLength(); i++)
				pages.add( (Element) nl.item(i));
			
			for (int i = 0; i < pages.size(); i++) { // define a page atual
				String pageID = pages.get(i).getAttribute("id");
				
				Rede rede = new Rede(pageID);

				Element root = pages.get(i);
				
				// busca o nome da rede
				NodeList n = root.getElementsByTagName("pageattr");
				if( n.getLength() > 0 )
				{
					Element e = (Element) n.item( 0 );
					rede.setName( e.getAttribute( "name" ) );
				} // fim de if				

				// encontrar as tags dentro do root
				NodeList elemPlaces = root.getElementsByTagName("place");
				NodeList elemTransitions = root.getElementsByTagName("trans");
				NodeList elemArc = root.getElementsByTagName("arc");

							
				// organizar os elementos
				// place:
				for (int j = 0; j < elemPlaces.getLength(); j++) {
					Element elemAtual = (Element) elemPlaces.item(j);
					

					//pegar cor
					NodeList elemInitmark = elemAtual.getElementsByTagName("initmark");
					
//					System.out.println("numero de initmarks: "+elemInitmark.getLength());	//sempre tera somente um
					Element textInitMark = (Element) elemInitmark.item(0);
					String textoInitmark = getChildTagValue(textInitMark, "text");
//					System.out.println("cor: "+textoInitmark+" / page: "+pageID);
					
					//fim de pegar cor
					
					
					String id = elemAtual.getAttribute("id");
					String text = getChildTagValue(elemAtual, "text");
					
					NodeList posicao = elemAtual.getElementsByTagName("posattr");
					Node posicaoNode = posicao.item(0);
					NamedNodeMap attributosNoPosition = posicaoNode.getAttributes();
					Node x = attributosNoPosition.getNamedItem("x");
					Node y = attributosNoPosition.getNamedItem("y");
					
					int xi = (int) Double.parseDouble(x.getNodeValue());
					int yi = (int) Double.parseDouble(y.getNodeValue());
					
					NodeList portNode = elemAtual.getElementsByTagName("port");
					boolean hasNode;
					if (portNode.getLength()>0)
						hasNode = true;
					else
						hasNode = false;
					
//					System.out.println("quantidade de port no place "+text+": "+portNode.getLength());
					
					boolean hasPortOut;
					if (hasNode && portNode.item(0).getAttributes().getNamedItem("type").getNodeValue().equals("Out"))	//se tem port Out (saida)
						hasPortOut = true;
					else
						hasPortOut = false;
					
					
					Place place = new Place(id, text, textoInitmark, textoInitmark, xi, yi, pageID, hasNode, hasPortOut);

					places.add(place);
					
					rede.addPlace(place);
					
				}
				// trans:
				for (int j = 0; j < elemTransitions.getLength(); j++) {
					Element elemAtual = (Element) elemTransitions.item(j);

					String id = elemAtual.getAttribute("id");
					String text = getChildTagValue(elemAtual, "text");
					
					NodeList hasHierarquia = elemAtual.getElementsByTagName("subst");
//					System.out.println("em "+i+", transicao "+j+" size hasHierarquia: "+hasHierarquia.getLength());
					
					Transition tr = null;
					
					if (hasHierarquia.getLength() == 0) 
						tr = new Transition(id, text, false, pageID);
					else {
						String idSubpage = hasHierarquia.item(0).getAttributes().getNamedItem("subpage").getNodeValue();
//						System.out.println("===> idSubpage = "+idSubpage);
						relacaoHie.addRelacao(id, idSubpage);
						tr = new Transition(id, text, true, pageID);
					}

					transitions.add(tr);
					
					rede.addTransitions(tr);
				}
				// arc:
				for (int j = 0; j < elemArc.getLength(); j++) {
					// transend se tem um elemento por isso nl.item(0)
					
					Element elemAtual = (Element) elemArc.item(j);
					
					NodeList nlTransend = elemAtual.getElementsByTagName("transend");
					Element tagTransend = (Element) nlTransend.item(0);

					// placeend se tem um elemento por isso nl.item(0)
					NodeList nlPlaceend = elemAtual.getElementsByTagName("placeend");
					Element tagPlaceend = (Element) nlPlaceend.item(0);

					NodeList nlAnnot = elemAtual.getElementsByTagName("annot");
					String text = "";
		
					
					// // pega os dados do text do annot filho que eh filho do arc
					// atual
					for (int k = 0; k < nlAnnot.getLength(); k++) {
						Element tagText = (Element) nlAnnot.item(k);
						text = getChildTagValue(tagText, "text");
					}

					String id = elemAtual.getAttribute("id");
					String orientation = elemAtual.getAttribute("orientation");
					String transend = tagTransend.getAttribute("idref");
					String placeend = tagPlaceend.getAttribute("idref");
					
					Arc arc = new Arc(id, orientation, transend, placeend, text, pageID);

					arcs.add(arc);
					rede.addArcs(arc);
				}
				
				redes.add(rede);
			}

		reordenar();	
					
		}
		else {
			//erro na leitura do path
			System.err.println("erro na leitura do path: <"+path+">");
		}
		
	}
	
	
//	public ArrayList<Place> getPlaces() {
//		return places;
//	}
//	
//	public ArrayList<Transition> getTransitions() {
//		return transitions;
//	}
//	
//	public ArrayList<Arc> getArcs() {
//		return arcs;
//	}
	
	public ArrayList<Rede> getRedes() {
		return redes;
	}
	
	public Rede getRedeByID(String ID) {
		for (int i=0; i<redes.size(); i++)
			if (redes.get(i).getID().equals(ID))
				return redes.get(i);
		
		return null;
	}
	
	public RelacaoHierarquia getRelacoesHierarquicas() {
		return relacaoHie;
	}
	
	public void showPlaces() {
		System.out.println("- Places:");
		for (int i=0; i<places.size(); i++)
			System.out.println(places.get(i).toString());
		
		System.out.println();
	}

	public void showTransitions() {
		System.out.println("- Transitions:");
		for (int i=0; i<transitions.size(); i++)
			System.out.println(transitions.get(i).toString());
		
		System.out.println();
	}
	
	public void showArcs() {
		System.out.println("- Arcs:");
		for (int i=0; i<arcs.size(); i++)
			System.out.println(arcs.get(i).toString());
		
		System.out.println();
	}
	
	
	
	public ArrayList<Place> getOutputPort (String subredeID) {
		ArrayList<Place> placesComOutput = new ArrayList<Place>();
		
		for (int i=0; i<redes.size(); i++)
			if (redes.get(i).getID().equals(subredeID)) {
				ArrayList<Place> placesComPort = redes.get(i).getPlacesComPort();
				for (int j=0; j<placesComPort.size(); j++)
					if (placesComPort.get(j).hasPortOut()) 
						placesComOutput.add(placesComPort.get(j));				
			}
		
		
		return placesComOutput;
	}
	
	
	/////PRIVATE METHODS
	
	private void reordenar() {
		// separar arcos PtoT e TtoP
		ArrayList<Arc> arcsPtoT = new ArrayList<Arc>();
		ArrayList<Arc> arcsTtoP = new ArrayList<Arc>();

		for (int i = 0; i < arcs.size(); i++) {
			if (arcs.get(i).getOrientation().equals("PtoT")) {
				arcsPtoT.add(arcs.get(i));
			} else if (arcs.get(i).getOrientation().equals("TtoP")) {
				arcsTtoP.add(arcs.get(i));
			}
		}
		
		// P -> T
		for (int i = 0; i < arcsPtoT.size(); i++) {
			String idPlace = arcsPtoT.get(i).getPlaceend();
			String idTransition = arcsPtoT.get(i).getTransend();
			
			Place pl = getPlaceByID(idPlace);
			Transition tr = getTransitionByID(idTransition);
			
			pl.addSaida(tr);
			tr.addEntrada(pl);
		}
		
		// T -> P
		for (int i = 0; i < arcsTtoP.size(); i++) {
			String idPlace = arcsTtoP.get(i).getPlaceend();
			String idTransition = arcsTtoP.get(i).getTransend();
			
			Place pl = getPlaceByID(idPlace);
			Transition tr = getTransitionByID(idTransition);
			
			tr.addSaida(pl);
			pl.addEntrada(tr);
		}
				
	}
	
	
	private Place getPlaceByID (String ID) {
		for (int i=0; i<places.size(); i++)
			if (places.get(i).getID().equals(ID))
				return places.get(i);
		
		return null;
	}
	
	private Transition getTransitionByID (String ID) {
		for (int i=0; i<transitions.size(); i++)
			if (transitions.get(i).getID().equals(ID))
				return transitions.get(i);
		
		return null;
	}
	
	// este metodo le e retorna o conteudo (texto) de uma tag (elemento)
	// filho da tag informada como parametro. A tag filho a ser pesquisada
	// eh a tag informada pelo nome (string)
	private String getChildTagValue(Element elem, String tagName)
			throws Exception {
		NodeList children = elem.getElementsByTagName(tagName);
		if (children == null)
			return null;

		Element child = (Element) children.item(0);

		if (child == null)
			return null;
		else {
			if (child.getFirstChild() != null) {
				// System.out.println("child.getFirstChild().getNodeValue(): "+child.getFirstChild().getNodeValue().toString());
				return child.getFirstChild().getNodeValue();
			}

			else
				return null;
		}
	}
	
	public static Game getGame( String fileCPN, FactoryPlayMp3Interface factoryPlayMp3Interface, 
		DefaultFiguresInterface defaultFiguresInterface ) throws Exception
	{
		String prefixoPlayer1 = "P1_";
		String prefixoPlayer2 = "P2_";
		String prefixoPlayer1Out = "P1OUT_";
		String prefixoPlayer1In = "P1IN_";
		String prefixoPlayer2Out = "P2OUT_";
		String prefixoPlayer2In = "P2IN_";
		
		List<RedeProperties> redesPropertiesList = new ArrayList<RedeProperties>();
		List<LugarProperties> lugaresList;
		List<TransicaoProperties> transicoesList;
		
		// faz a leitura
		Reader reader = new Reader( fileCPN );
		reader.read();
		ArrayList<Rede> redes = reader.getRedes();
		RelacaoHierarquia relacoesHierarquicas = reader.getRelacoesHierarquicas();
		Map<String, String> configuracoes = new HashMap<String, String>();

		carregarConfiguracoes( fileCPN, configuracoes );
		Personagem p1 = getPersonagem1( configuracoes, defaultFiguresInterface );
		Personagem p2 = getPersonagem2( configuracoes, defaultFiguresInterface ); 
		
		// instancia as redes
		for( Rede r : redes )
		{
			RedeProperties redeProperties2 = new RedeProperties();
			redeProperties2.setNome( r.getName() + "$" + r.getID() );
			redesPropertiesList.add( redeProperties2 );
		} // fim de for
		
		// faz o parce das redes
		for( Rede r : redes )
		{
			int proxValorTransicao = 0;
			lugaresList = new ArrayList<LugarProperties>();
			transicoesList = new ArrayList<TransicaoProperties>();
			
			// configura os lugares desta rede
			for( Place p : r.getPlaces() )
			{				
				// verifica se n�o � porta de entrada
				if( !( p.hasPort() && !p.hasPortOut() ) )
				{
					LugarProperties lugarProperties = new LugarProperties();
					lugarProperties.setNome( p.getName() );
					lugarProperties.setPontoSaida( p.hasPortOut() );
					lugarProperties.setPosLinha( p.getYi() );
					lugarProperties.setPosColuna( p.getXi() );
					
					// configura os estados
					EstadoLugar estado = new EstadoLugar();
					estado.setFigura( getConfiguracao( p.getName(), configuracoes, defaultFiguresInterface ) );
					estado.setValor( 0 );
					lugarProperties.getEstadosPossiveisLugar().add( estado );

					estado = new EstadoLugar();
					estado.setFigura( getConfiguracao( p.getName(), configuracoes, defaultFiguresInterface ) );
					estado.setValor( 1 );
					estado.setPersonagemPresente1( p1 );
					lugarProperties.getEstadosPossiveisLugar().add( estado );

					estado = new EstadoLugar();
					estado.setFigura( getConfiguracao( p.getName(), configuracoes, defaultFiguresInterface ) );
					estado.setValor( 2 );
					estado.setPersonagemPresente2( p2 );
					lugarProperties.getEstadosPossiveisLugar().add( estado );

					estado = new EstadoLugar();
					estado.setFigura( getConfiguracao( p.getName(), configuracoes, defaultFiguresInterface ) );
					estado.setValor( 3 );
					estado.setPersonagemPresente1( p1 );
					estado.setPersonagemPresente2( p2 );
					lugarProperties.getEstadosPossiveisLugar().add( estado );
					
					lugaresList.add( lugarProperties );
					
					// configura transi��es out para porta de sa�da
					if( p.hasPortOut() )
					{						
						// [(p1, null) : (p1,null)] -> [(null,null) : (null,null)] 
						TransicaoProperties transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarProperties );
						transicaoProperties.setLugarDestinoProperties( lugarProperties ); 
						transicaoProperties.setNome( prefixoPlayer1Out + ( proxValorTransicao++ ) );
						
						PreDisparoTransicao pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, p1, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarProperties, p1, null ) );
						PosDisparoTransicao pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarProperties, null, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, p2) : (null,null)] -> [(null,p2) : (null,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarProperties );
						transicaoProperties.setLugarDestinoProperties( lugarProperties ); 
						transicaoProperties.setNome( prefixoPlayer1Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, p1, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarProperties, p1, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, null, p2 ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarProperties, null, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(null, p2) : (null,p2)] -> [(null,null) : (null,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarProperties );
						transicaoProperties.setLugarDestinoProperties( lugarProperties ); 
						transicaoProperties.setNome( prefixoPlayer2Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, null, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarProperties, null, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarProperties, null, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, p2) : (null,null)] -> [(p1,null) : (p1,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarProperties );
						transicaoProperties.setLugarDestinoProperties( lugarProperties ); 
						transicaoProperties.setNome( prefixoPlayer2Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, p1, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarProperties, p1, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarProperties, p1, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarProperties, p1, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );
					} // fim de if
				} // fim de if
			} // fim de for
			// configura transicoes desta rede
			for( Transition t : r.getTransicoes() )
			{
				// recupera arcos de entrada e sa�da da transicao
				Arc aEntrada = getArc( r.getArcs(), t.getID(), "PtoT" );
				Arc aSaida = getArc( r.getArcs(), t.getID(), "TtoP" );
				
				// verifica se � transi��o de n�vel
				if( t.hasHierarquia() )
				{
					// verifica se � para player 1
					if( aEntrada.getTexto() == null || aEntrada.getTexto().equalsIgnoreCase( "1`1" ) || 
						aEntrada.getTexto().equalsIgnoreCase( "1`a" ) )
					{
						LugarProperties lugarOri = getLugarProperties( 
							aEntrada.getPlaceend(), lugaresList, r.getPlaces() );
						LugarProperties lugarDest = getLugarProperties( 
							aSaida.getPlaceend(), lugaresList, r.getPlaces() );
						
						// configura in
						// [(null, null) : (null,null)] -> [(p1,null) : (p1,null)] 
						TransicaoProperties transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarDest );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer1In + ( proxValorTransicao++ ) );
						
						PreDisparoTransicao pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
						PosDisparoTransicao pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(null, p2) : (null,p2)] -> [(p1,p2) : (p1,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarDest );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer1In + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, p2 ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );
						
						// configura out
						// [(p1, null) : (p1,null)] -> [(null,null) : (null,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarOri ); 
						transicaoProperties.setNome( prefixoPlayer1Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarOri, p1, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarOri, null, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, p2) : (p1,p2)] -> [(null,p2) : (null,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarOri ); 
						transicaoProperties.setNome( prefixoPlayer1Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarOri, p1, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarOri, null, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// transi��es 'normais' de n�vel
						RedeProperties redeInf = getRedeProperties( t.getID(), relacoesHierarquicas, redesPropertiesList );
						
						// [(p1, null) : (null,null)] -> [(null,null) : (p1,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer1 + ( proxValorTransicao++ ) );
						transicaoProperties.setRedeNivelInferior( redeInf );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, p2) : (null,null)] -> [(null,p2) : (p1,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer1 + ( proxValorTransicao++ ) );
						transicaoProperties.setRedeNivelInferior( redeInf );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, null) : (null,p2)] -> [(null,null) : (p1,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer1 + ( proxValorTransicao++ ) );
						transicaoProperties.setRedeNivelInferior( redeInf );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );
					} // fim de if
					// verifica se � para player 2
					if( aEntrada.getTexto() == null || aEntrada.getTexto().equalsIgnoreCase( "1`2" ) || 
						aEntrada.getTexto().equalsIgnoreCase( "1`a" ) )
					{

						LugarProperties lugarOri = getLugarProperties( 
							aEntrada.getPlaceend(), lugaresList, r.getPlaces() );
						LugarProperties lugarDest = getLugarProperties( 
							aSaida.getPlaceend(), lugaresList, r.getPlaces() );
						
						// configura in
						// [(null, null) : (null,null)] -> [(null,p2) : (null,p2)] 
						TransicaoProperties transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarDest );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer2In + ( proxValorTransicao++ ) );
						
						PreDisparoTransicao pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
						PosDisparoTransicao pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, p2 ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1,null) : (p1,null)] -> [(p1,p2) : (p1,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarDest );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer2In + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, null ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, p2 ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );
						
						// configura out
						// [(null, p2) : (null,p2)] -> [(null,null) : (null,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarOri ); 
						transicaoProperties.setNome( prefixoPlayer2Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarOri, null, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarOri, null, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, p2) : (p1,p2)] -> [(p1,null) : (p1,null)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarOri ); 
						transicaoProperties.setNome( prefixoPlayer2Out + ( proxValorTransicao++ ) );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarOri, p1, p2 ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarOri, p1, null ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );
						
						// transi��es 'normais' de n�vel
						RedeProperties redeInf = getRedeProperties( t.getID(), relacoesHierarquicas, redesPropertiesList );
						
						// [(null, p2) : (null,null)] -> [(null,null) : (null,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer2 + ( proxValorTransicao++ ) );
						transicaoProperties.setRedeNivelInferior( redeInf );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(p1, p2) : (null,null)] -> [(p1,null) : (null,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer2 + ( proxValorTransicao++ ) );
						transicaoProperties.setRedeNivelInferior( redeInf );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );

						// [(null, p2) : (p1,null)] -> [(null,null) : (p1,p2)] 
						transicaoProperties = new TransicaoProperties();
						transicaoProperties.setLugarOrigemProperties( lugarOri );
						transicaoProperties.setLugarDestinoProperties( lugarDest ); 
						transicaoProperties.setNome( prefixoPlayer2 + ( proxValorTransicao++ ) );
						transicaoProperties.setRedeNivelInferior( redeInf );
						
						pre = new PreDisparoTransicao();
						pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
						pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );
						pos = new PosDisparoTransicao();
						pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
						pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

						transicaoProperties.setPreDisparoTransicao( pre );
						transicaoProperties.setPosDisparoTransicao( pos );
						transicoesList.add( transicaoProperties );					
					} // fim de if
					
				} // fim de if
				else
				{				
					// verifica se � para player 1
					if( aEntrada.getTexto() == null || aEntrada.getTexto().equalsIgnoreCase( "1`1" ) || 
						aEntrada.getTexto().equalsIgnoreCase( "1`a" ) )
					{
						
						LugarProperties lugarOri = getLugarProperties( 
							aEntrada.getPlaceend(), lugaresList, r.getPlaces() );
						// verifica � transi��o de entrada na rede -> configurar in 
						if( lugarOri == null )
						{
							LugarProperties lugarDest = getLugarProperties( 
								aSaida.getPlaceend(), lugaresList, r.getPlaces() );
							lugarDest.setPontoEntrada( true );
							
							// configura in
							// [(null, null) : (null,null)] -> [(p1,null) : (p1,null)] 
							TransicaoProperties transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarDest );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer1In + ( proxValorTransicao++ ) );
							
							PreDisparoTransicao pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, null ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
							PosDisparoTransicao pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, null ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );

							// [(null, p2) : (null,p2)] -> [(p1,p2) : (p1,p2)] 
							transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarDest );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer1In + ( proxValorTransicao++ ) );
							
							pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, p2 ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );
							pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, p2 ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );
						} // fim de if
						else // transi��o interna da rede
						{
							LugarProperties lugarDest = getLugarProperties( 
									aSaida.getPlaceend(), lugaresList, r.getPlaces() );
							
							// [(p1, null) : (null,null)] -> [(null,null) : (p1,null)] 
							TransicaoProperties transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarOri );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer1 + ( proxValorTransicao++ ) );
							
							PreDisparoTransicao pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
							PosDisparoTransicao pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );

							// [(p1, p2) : (null,null)] -> [(null,p2) : (p1,null)] 
							transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarOri );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer1 + ( proxValorTransicao++ ) );
							
							pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, p2 ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
							pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );

							// [(p1, null) : (null,p2)] -> [(null,null) : (p1,p2)] 
							transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarOri );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer1 + ( proxValorTransicao++ ) );
							
							pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );
							pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );
						} // fim de else						
					} // fim de if
					// verifica se � para player 2
					if( aEntrada.getTexto() == null || aEntrada.getTexto().equalsIgnoreCase( "1`2" ) || 
						aEntrada.getTexto().equalsIgnoreCase( "1`a" ) )
					{						
						LugarProperties lugarOri = getLugarProperties( 
							aEntrada.getPlaceend(), lugaresList, r.getPlaces() );
						// verifica � transi��o de entrada na rede -> configurar in 
						if( lugarOri == null )
						{
							LugarProperties lugarDest = getLugarProperties( 
								aSaida.getPlaceend(), lugaresList, r.getPlaces() );
							lugarDest.setPontoEntrada( true );
							
							// configura in
							// [(null, null) : (null,null)] -> [(null,p2) : (null,p2)] 
							TransicaoProperties transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarDest );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer2In + ( proxValorTransicao++ ) );
							
							PreDisparoTransicao pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, null ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
							PosDisparoTransicao pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, null, p2 ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );

							// [(p1, null) : (p1,null)] -> [(p1,p2) : (p1,p2)] 
							transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarDest );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer2In + ( proxValorTransicao++ ) );
							
							pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, null ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );
							pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarDest, p1, p2 ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );
						} // fim de if
						else // transi��o interna da rede
						{
							LugarProperties lugarDest = getLugarProperties( 
									aSaida.getPlaceend(), lugaresList, r.getPlaces() );
							
							// [(null, p2) : (null,null)] -> [(null,null) : (null,p2)] 
							TransicaoProperties transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarOri );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer2 + ( proxValorTransicao++ ) );
							
							PreDisparoTransicao pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
							PosDisparoTransicao pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );

							// [(p1, p2) : (null,null)] -> [(p1,null) : (null,p2)] 
							transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarOri );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer2 + ( proxValorTransicao++ ) );
							
							pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, p2 ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, null ) );
							pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, p1, null ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, null, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );

							// [(null, p2) : (p1,null)] -> [(null,null) : (p1,p2)] 
							transicaoProperties = new TransicaoProperties();
							transicaoProperties.setLugarOrigemProperties( lugarOri );
							transicaoProperties.setLugarDestinoProperties( lugarDest ); 
							transicaoProperties.setNome( prefixoPlayer2 + ( proxValorTransicao++ ) );
							
							pre = new PreDisparoTransicao();
							pre.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, p2 ) );
							pre.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, null ) );
							pos = new PosDisparoTransicao();
							pos.setEstadoLugarOrigem( getEstadoLugar( lugarOri, null, null ) );
							pos.setEstadoLugarDestino( getEstadoLugar( lugarDest, p1, p2 ) );

							transicaoProperties.setPreDisparoTransicao( pre );
							transicaoProperties.setPosDisparoTransicao( pos );
							transicoesList.add( transicaoProperties );
						} // fim de else						
					} // fim de if
				} // fim de else
			} // fim de for transi��es

			configurarPosicoes( lugaresList );
			
			// procura a rede
			for( RedeProperties redeProperties : redesPropertiesList )
			{
				if( redeProperties.getNome().split( "\\$" )[ 1 ].equals( r.getID() ) )
				{
					redeProperties.setLugaresPropertiesList( lugaresList );
					redeProperties.setTransicoesPropertiesList( transicoesList );
					configurarDimensoes( redeProperties );
					break;
				} // fim de if
			} // fim de for
						
			// define a marca��o inicial
			for( Place p : r.getPlaces() )
			{
				if( p.getInitmark() != null && p.getInitmark().contains( "1`1" ) &&
					p.getInitmark().contains( "1`2" ) )
				{
					for( LugarProperties l : lugaresList )
					{
						if( l.getNome().equals( p.getName() ) )
						{
							EstadoLugar e = getEstadoLugar( l, p1, p2 );
							e.setInicial( true );
						} // fim de if
					} // fim de for
				} // fim de if
				else if( p.getInitmark() != null && p.getInitmark().contains( "1`1" ) )
				{
					for( LugarProperties l : lugaresList )
					{
						if( l.getNome().equals( p.getName() ) )
						{
							EstadoLugar e = getEstadoLugar( l, p1, null );
							e.setInicial( true );
						} // fim de if
					} // fim de for
				} // fim de if
				else if( p.getInitmark() != null && p.getInitmark().contains( "1`2" ) )
				{
					for( LugarProperties l : lugaresList )
					{
						if( l.getNome().equals( p.getName() ) )
						{
							EstadoLugar e = getEstadoLugar( l, null, p2 );
							e.setInicial( true );
						} // fim de if
					} // fim de for
				} // fim de if
				else
				{
					for( LugarProperties l : lugaresList )
					{
						if( l.getNome().equals( p.getName() ) )
						{
							EstadoLugar e = getEstadoLugar( l, null, null );
							e.setInicial( true );
						} // fim de if
					} // fim de for
				} // fim de if
			} // fim de for marca��o

		} // fim de for redes
		
		Game game = new Game( fileCPN, redesPropertiesList.get( 0 ) );
		game.setRedesPropertiesList( redesPropertiesList );
		List<Personagem> pers = new ArrayList<Personagem>();
		pers.add( p1 );
		pers.add( p2 );
		game.setPersonagens( pers );
		
		// ajusta o nome das redes e suas configuracoes
		for( RedeProperties redeProperties : redesPropertiesList )
		{
			redeProperties.setNome( redeProperties.getNome().split( "\\$" )[ 0 ] );
			setConfiguracoes( redeProperties, configuracoes, factoryPlayMp3Interface, defaultFiguresInterface );
		} // fim de for
		
		return game;
	} // fim de m�todo
	
	private static void setConfiguracoes( RedeProperties redeProperties, Map<String, String> configuracoes, 
		FactoryPlayMp3Interface factoryPlayMp3Interface, DefaultFiguresInterface defaultFiguresInterface )
	{
		String file = configuracoes.get( redeProperties.getNome() + ".musica" );

		if( file != null )
		{
			Audio audio = new Audio( file );
			if( factoryPlayMp3Interface != null )
				audio.setPlayMp3Interface( factoryPlayMp3Interface.getPlayMp3Interface( file ) );
			redeProperties.setMusicaMundo( audio );
		} // fim de if
		
		String transVert = configuracoes.get( redeProperties.getNome() + ".transicao_subs_vertical" );
		
		if( transVert != null )
			redeProperties.setTransicaoSubstituicaVerticalPath( transVert );
		else
			redeProperties.setTransicaoSubstituicaVerticalPath( defaultFiguresInterface.getDefaultFigure( "trans_vertical.png" ) );

		String transHor = configuracoes.get( redeProperties.getNome() + ".transicao_subs_horizontal" );
		if( transHor != null )
			redeProperties.setTransicaoSubstituicaHorizontalPath( transHor );
		else
			redeProperties.setTransicaoSubstituicaHorizontalPath( defaultFiguresInterface.getDefaultFigure( "trans_horizontal.png" ) );
	} // fim de m�todo
	
	private static void carregarConfiguracoes( String file, Map<String, String> configuracoes )
	{
		char s = "/".charAt(0);
		String newFile = file.substring( 0, file.length() - 3 ) + "properties";
		File fileProps = new File( newFile );
		
		int i;
		String baseDir = file;
		
		for( i = baseDir.length() - 1; i >= 0 && baseDir.charAt( i ) != s; i-- );
		baseDir = baseDir.substring( 0, i );
		
		
		if( fileProps.exists() || !fileProps.exists())
		{
			Scanner in = new Scanner( Gdx.files.internal(newFile).read() );
			
			while( in.hasNext() )
			{
				String linha = in.nextLine();
				String tokens[] = linha.split( "\\=" );				

				String caminho = tokens[ 1 ].replace( '/', s );
				caminho = caminho.replace( '\\', s ).trim();
						
				configuracoes.put( tokens[ 0 ].trim(), baseDir + s + caminho );
			} // fim de while
			in.close();
		} // fim de if		
	
	} // fim de m�todo
	
	private static void configurarDimensoes( RedeProperties redeProperties )
	{
		int maxLinha = 0, maxColuna = 0;
		
		for( LugarProperties l : redeProperties.getLugaresPropertiesList() )
		{
			if( l.getPosLinha() > maxLinha )
				maxLinha = l.getPosLinha();
			if( l.getPosColuna() > maxColuna )
				maxColuna = l.getPosColuna();
		} // fim de for
		redeProperties.setNumeroLinhas( maxLinha + 1 );
		redeProperties.setNumeroColunas( maxColuna + 1 );
	} // fim de m�todo
	
	private static void configurarPosicoes( List<LugarProperties> lugaresList )
	{
		List<LugarProperties> lista = new ArrayList<LugarProperties>();
		List<List<LugarProperties>> horizontaisList = new ArrayList<List<LugarProperties>>();
		List<List<LugarProperties>> verticaisList = new ArrayList<List<LugarProperties>>();
		
		// calcula listas horizontais
		lista.addAll( lugaresList );	
		for( int i = 0; i < lista.size(); i++ )
		{			
			List<LugarProperties> l = new ArrayList<LugarProperties>();			
			horizontaisList.add( l );
			l.add( lista.get( i ) );
			
			for( int j = i + 1; j < lista.size(); j++ )
			{
				// verifica se s�o vizinhos na horizontal
				if( lista.get( i ).getPosColuna() < lista.get( j ).getPosColuna() + 20 && 
					lista.get( i ).getPosColuna() > lista.get( j ).getPosColuna() - 20 )
				{
					l.add( lista.get( j ) );
					lista.remove( j-- );
				} // fim de if
			} // fim de for
		} // fim de for
		
		lista.clear();
		lista.addAll( lugaresList );
		// calcula listas verticais
		for( int i = 0; i < lista.size(); i++ )
		{			
			List<LugarProperties> l = new ArrayList<LugarProperties>();			
			verticaisList.add( l );
			l.add( lista.get( i ) );
			
			for( int j = i + 1; j < lista.size(); j++ )
			{
				// verifica se s�o vizinhos na verticais
				if( lista.get( i ).getPosLinha() < lista.get( j ).getPosLinha() + 20 && 
					lista.get( i ).getPosLinha() > lista.get( j ).getPosLinha() - 20 )
				{
					l.add( lista.get( j ) );
					lista.remove( j-- );
				} // fim de if
			} // fim de for
		} // fim de for

		// ordena elementos das lista horizontais
		for( List<LugarProperties> l : horizontaisList )
		{
			Collections.sort( l, new Comparator<LugarProperties>() 
			{
				@Override
				public int compare(LugarProperties o1, LugarProperties o2) 
				{
					if( o1.getPosLinha() < o2.getPosLinha() )
						return -1;
					else if( o1.getPosLinha() == o2.getPosLinha() )
						return 0;
					return 1;
				} // fim de m�todo
			}); // fim de chamada
		} // fim de for

		// ordena de forma reversa os elementos das listas verticais
		for( List<LugarProperties> l : verticaisList )
		{
			Collections.sort( l, new Comparator<LugarProperties>() 
			{
				@Override
				public int compare(LugarProperties o1, LugarProperties o2) 
				{
					if( o1.getPosColuna() < o2.getPosColuna() )
						return 1;
					else if( o1.getPosColuna() == o2.getPosColuna() )
						return 0;
					return -1;
				} // fim de m�todo
			}); // fim de chamada
		} // fim de for
		
		// ordena a lista horozontal
		Collections.sort( horizontaisList, new Comparator<List<LugarProperties>>() 
		{
			@Override
			public int compare(List<LugarProperties> o1, List<LugarProperties> o2) 
			{
				if( o1.get( 0 ).getPosColuna() < o2.get( 0 ).getPosColuna() )
					return -1;
				return 1;
			} // fim de m�todo					
		}); // fim de chamada

		// ordena a lista vertical
		Collections.sort( verticaisList, new Comparator<List<LugarProperties>>() 
		{
			@Override
			public int compare(List<LugarProperties> o1, List<LugarProperties> o2) 
			{
				if( o1.get( 0 ).getPosLinha() < o2.get( 0 ).getPosLinha() )
					return 1;
				return -1;
			} // fim de m�todo					
		}); // fim de chamada
	
		// calcula as posi��es dos lugares
		for( LugarProperties lugarProperties : lugaresList )
		{
			// procura o elemento na lista horizontal
			for( int i = 0; i < horizontaisList.size(); i++ )
			{
				if( horizontaisList.get( i ).contains( lugarProperties ) )
				{
					lugarProperties.setPosColuna( i );
					break;
				} // fim de if
			} // fim de for
			// procura o elemento na lista vertical
			for( int i = 0; i < verticaisList.size(); i++ )
			{
				if( verticaisList.get( i ).contains( lugarProperties ) )
				{
					lugarProperties.setPosLinha( i );
					break;
				} // fim de if
			} // fim de for
			
		} // fim de for
	} // fim de m�todo
	
	private static RedeProperties getRedeProperties( String idTransicao, RelacaoHierarquia relacaoHierarquia,
		List<RedeProperties> redes )
	{
		for( ElementoRelacaoHierarquia e : relacaoHierarquia.getRelacaoTransSubpage() )
		{
			if( e.getTransitionID().equals( idTransicao ) )
			{
				for( RedeProperties r : redes )
				{
					if( r.getNome().split( "\\$" )[ 1 ].equals( e.getSubpageID() ) )
						return r;
				} // fim de for
			} // fim de if
		} // fim de if
		
		return null;
	} // fim de m�todo
	
	private static EstadoLugar getEstadoLugar( LugarProperties lugar, Personagem p1, Personagem p2 )
	{
		for( EstadoLugar e : lugar.getEstadosPossiveisLugar() )
		{
			if( e.getPersonagemPresente1() == p1 && e.getPersonagemPresente2() == p2 )
				return e;
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	private static LugarProperties getLugarProperties( String idLugar, List<LugarProperties> lugares, List<Place> places )
	{
		for( Place p : places )
		{
			if( p.getID().equals( idLugar ) )
			{
				for( LugarProperties l : lugares )
				{
					if( p.getName().equals( l.getNome() ) )
						return l;
				} // fim de for
			} // fim de for
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	
	private static Arc getArc( List<Arc> arcs, String transicaoId, String orientacao )
	{
		for( Arc a : arcs )
		{
			if( a.getOrientation().equals( orientacao ) && a.getTransend().equals( transicaoId ) )
				return a;
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	private static Personagem getPersonagem1( Map<String, String> configuracoes, DefaultFiguresInterface defaultFiguresInterface )
	{
		Personagem p = new Personagem();
		p.setFiguraDefault( defaultFiguresInterface.getDefaultFigure( "defaultp1.png" ) );
		p.setFiguraBaixo1( defaultFiguresInterface.getDefaultFigure( "baixo1p1.png" ) );
		p.setFiguraBaixo2( defaultFiguresInterface.getDefaultFigure( "baixo2p1.png" ) );
		p.setFiguraBaixo3( defaultFiguresInterface.getDefaultFigure( "baixo3p1.png" ) );
		p.setFiguraDireita1( defaultFiguresInterface.getDefaultFigure( "dir1p1.png" ) );
		p.setFiguraDireita2( defaultFiguresInterface.getDefaultFigure( "dir2p1.png" ) );
		p.setFiguraDireita3( defaultFiguresInterface.getDefaultFigure( "dir3p1.png" ) );
		p.setFiguraCima1( defaultFiguresInterface.getDefaultFigure( "cima1p1.png" ) );
		p.setFiguraCima2( defaultFiguresInterface.getDefaultFigure( "cima2p1.png" ) );
		p.setFiguraCima3( defaultFiguresInterface.getDefaultFigure( "cima3p1.png" ) );
		p.setFiguraEsquerda1( defaultFiguresInterface.getDefaultFigure( "esq1p1.png" ) );
		p.setFiguraEsquerda2( defaultFiguresInterface.getDefaultFigure( "esq2p1.png" ) );
		p.setFiguraEsquerda3( defaultFiguresInterface.getDefaultFigure( "esq3p1.png" ) );
		p.setFiguraDiagInfDir1( defaultFiguresInterface.getDefaultFigure( "diag_inf_dir1p1.png" ) );
		p.setFiguraDiagInfDir2( defaultFiguresInterface.getDefaultFigure( "diag_inf_dir2p1.png" ) );
		p.setFiguraDiagInfDir3( defaultFiguresInterface.getDefaultFigure( "diag_inf_dir3p1.png" ) );
		p.setFiguraDiagInfEsq1( defaultFiguresInterface.getDefaultFigure( "diag_inf_esq1p1.png" ) );
		p.setFiguraDiagInfEsq2( defaultFiguresInterface.getDefaultFigure( "diag_inf_esq2p1.png" ) );
		p.setFiguraDiagInfEsq3( defaultFiguresInterface.getDefaultFigure( "diag_inf_esq3p1.png" ) );
		p.setFiguraDiagSupDir1( defaultFiguresInterface.getDefaultFigure( "diag_sup_dir1p1.png" ) );
		p.setFiguraDiagSupDir2( defaultFiguresInterface.getDefaultFigure( "diag_sup_dir2p1.png" ) );
		p.setFiguraDiagSupDir3( defaultFiguresInterface.getDefaultFigure( "diag_sup_dir3p1.png" ) );
		p.setFiguraDiagSupEsq1( defaultFiguresInterface.getDefaultFigure( "diag_sup_esq1p1.png" ) );
		p.setFiguraDiagSupEsq2( defaultFiguresInterface.getDefaultFigure( "diag_sup_esq2p1.png" ) );
		p.setFiguraDiagSupEsq3( defaultFiguresInterface.getDefaultFigure( "diag_sup_esq3p1.png" ) );
		
		p.setPlayer1( true );
		
		return p;		
	} // fim de m�todo

	private static Personagem getPersonagem2( Map<String, String> configuracoes, DefaultFiguresInterface defaultFiguresInterface )
	{
		Personagem p = new Personagem();
		p.setFiguraDefault( defaultFiguresInterface.getDefaultFigure( "defaultp2.png" ) );
		p.setFiguraBaixo1( defaultFiguresInterface.getDefaultFigure( "baixo1p2.png" ) );
		p.setFiguraBaixo2( defaultFiguresInterface.getDefaultFigure( "baixo2p2.png" ) );
		p.setFiguraBaixo3( defaultFiguresInterface.getDefaultFigure( "baixo3p2.png" ) );
		p.setFiguraDireita1( defaultFiguresInterface.getDefaultFigure( "dir1p2.png" ) );
		p.setFiguraDireita2( defaultFiguresInterface.getDefaultFigure( "dir2p2.png" ) );
		p.setFiguraDireita3( defaultFiguresInterface.getDefaultFigure( "dir3p2.png" ) );
		p.setFiguraCima1( defaultFiguresInterface.getDefaultFigure( "cima1p2.png" ) );
		p.setFiguraCima2( defaultFiguresInterface.getDefaultFigure( "cima2p2.png" ) );
		p.setFiguraCima3( defaultFiguresInterface.getDefaultFigure( "cima3p2.png" ) );
		p.setFiguraEsquerda1( defaultFiguresInterface.getDefaultFigure( "esq1p2.png" ) );
		p.setFiguraEsquerda2( defaultFiguresInterface.getDefaultFigure( "esq2p2.png" ) );
		p.setFiguraEsquerda3( defaultFiguresInterface.getDefaultFigure( "esq3p2.png" ) );
		p.setFiguraDiagInfDir1( defaultFiguresInterface.getDefaultFigure( "diag_inf_dir1p2.png" ) );
		p.setFiguraDiagInfDir2( defaultFiguresInterface.getDefaultFigure( "diag_inf_dir2p2.png" ) );
		p.setFiguraDiagInfDir3( defaultFiguresInterface.getDefaultFigure( "diag_inf_dir3p2.png" ) );
		p.setFiguraDiagInfEsq1( defaultFiguresInterface.getDefaultFigure( "diag_inf_esq1p2.png" ) );
		p.setFiguraDiagInfEsq2( defaultFiguresInterface.getDefaultFigure( "diag_inf_esq2p2.png" ) );
		p.setFiguraDiagInfEsq3( defaultFiguresInterface.getDefaultFigure( "diag_inf_esq3p2.png" ) );
		p.setFiguraDiagSupDir1( defaultFiguresInterface.getDefaultFigure( "diag_sup_dir1p2.png" ) );
		p.setFiguraDiagSupDir2( defaultFiguresInterface.getDefaultFigure( "diag_sup_dir2p2.png" ) );
		p.setFiguraDiagSupDir3( defaultFiguresInterface.getDefaultFigure( "diag_sup_dir3p2.png" ) );
		p.setFiguraDiagSupEsq1( defaultFiguresInterface.getDefaultFigure( "diag_sup_esq1p2.png" ) );
		p.setFiguraDiagSupEsq2( defaultFiguresInterface.getDefaultFigure( "diag_sup_esq2p2.png" ) );
		p.setFiguraDiagSupEsq3( defaultFiguresInterface.getDefaultFigure( "diag_sup_esq3p2.png" ) );
		
		return p;		
	} // fim de m�todo
	
	
	private static String getConfiguracao( String key, Map<String, String> configuracoes, DefaultFiguresInterface defaultFiguresInterface )
	{
		char s = "/".charAt(0);
		
		String img = configuracoes.get( key );	
		if( img == null )
			return defaultFiguresInterface.getDefaultFigure( "lugar.png" );
		else
		{
			String caminho = img.replace( '/', s );
			caminho = caminho.replace( '\\', s );
			
			return caminho;
		} // fim de else
	} // fim de m�todo
	
} // fim de classe
