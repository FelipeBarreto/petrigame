package br.ufc.petrigame.netObjects.legacy;

public class ElementoRelacaoHierarquia {

	private String transitionID;
	private String subpageID;
	
	public ElementoRelacaoHierarquia (String transicaoID, String subpageID) {
		this.transitionID = transicaoID;
		this.subpageID = subpageID;
	}
	
	public String getSubpageID() {
		return subpageID;
	}
	
	public String getTransitionID() {
		return transitionID;
	}
	
	@Override
	public String toString() {
		return "ElementoRelacaoHie [transitionID=" + transitionID + ", subpageID=" + subpageID +"]";
	}
}
