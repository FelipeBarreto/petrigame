package br.ufc.petrigame.netObjects.legacy;

public class Arc {

	private String id;
	private String orientation;
	private String transend; //idRef
	private String placeend; //idRef
	private String texto;
	
	private String pageID;
	
	public Arc(String id, String orientation, String transend, String placeend, String text, String pageID) {
		this.id = id;
		this.orientation = orientation;
		this.transend = transend;
		this.placeend = placeend;
		this.texto = text;
		
		this.pageID = pageID;
	}

	public String getId() {
		return id;
	}

	public String getOrientation() {
		return orientation;
	}

	public String getTransend() {
		return transend;
	}

	public String getPlaceend() {
		return placeend;
	}
	
	public String getTexto() {
		return texto;
	}

	public String getPageID() {
		return pageID;
	}
	
	public String toString() {
		return "Arc [id="            + id + 
				    ", transend="    + transend + 
				    ", placeend="    + placeend +  
//				    ", text="        + text     +
				    ", orientation=" + orientation +
				    ", pageID="	 	 + pageID +
				    "]";
	}
	
}
