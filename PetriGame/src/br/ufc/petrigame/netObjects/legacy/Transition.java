package br.ufc.petrigame.netObjects.legacy;

import java.util.ArrayList;


public class Transition {

	private ArrayList<Place> pEntrada;
	private ArrayList<Place> pSaida;
	
	private String ID;
	private String name;
	
	private String pageID;
	private boolean hasHierarquia;
	
	public Transition (String ID, String name, boolean hasHierarquia, String pageID) {
		this.ID = ID;
		this.name = name;
		
		this.pEntrada = new ArrayList<Place>();
		this.pSaida = new ArrayList<Place>();
		
		this.pageID = pageID;
		this.hasHierarquia = hasHierarquia;
	}
	
	public String getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPageID() {
		return pageID;
	}
	
	public boolean hasHierarquia() {
		return hasHierarquia;
	}
	
	public void addEntrada (Place p) {
		this.pEntrada.add(p);
	}
	
	public void addSaida (Place p) {
		this.pSaida.add(p);
	}
	
	public ArrayList<Place> getEntradas() {
		return this.pEntrada;
	}
	
	public ArrayList<Place> getSaidas() {
		return this.pSaida;
	}
	
	
	public Place getEntradaByID (String ID) {
		for (int i=0; i<pEntrada.size(); i++) 
			if (pEntrada.get(i).getID().equals(ID)) 
				return pEntrada.get(i);		
		
		return null;
	}
	
	public Place getEntradaByName (String name) {
		for (int i=0; i<pEntrada.size(); i++) 
			if (pEntrada.get(i).getName().equals(name)) 
				return pEntrada.get(i);
		
		return null;
	}
	
	public Place getSaidaByID (String ID) {
		for (int i=0; i<pSaida.size(); i++) 
			if (pSaida.get(i).getID().equals(ID)) 
				return pSaida.get(i);		
		
		return null;
	}
	
	public Place getSaidaByName (String name) {
		for (int i=0; i<pSaida.size(); i++) 
			if (pSaida.get(i).getName().equals(name)) 
				return pSaida.get(i);		
		
		return null;
	}

	public String toString() {
		return "Transition [id=" + ID + ", text=" + name + ", haveHierarquia="+hasHierarquia+", pageID=" + pageID +"]";
	}
	
}
