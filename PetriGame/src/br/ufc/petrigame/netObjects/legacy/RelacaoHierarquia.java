package br.ufc.petrigame.netObjects.legacy;

import java.util.ArrayList;


public class RelacaoHierarquia {

	private ArrayList<ElementoRelacaoHierarquia> relacaoTransSubpage;
	
	public RelacaoHierarquia() {
		relacaoTransSubpage = new ArrayList<ElementoRelacaoHierarquia>();
	}
	
	public void addRelacao (String idTransicao, String idSubpage) {
		ElementoRelacaoHierarquia elem = new ElementoRelacaoHierarquia(idTransicao, idSubpage);
		relacaoTransSubpage.add(elem);
	}
	
	public void addRelacao (ElementoRelacaoHierarquia elemento) {
		relacaoTransSubpage.add(elemento);
	}

	public String getSubPageID (String idTransitioHie) {
		System.out.println("idTransitionHie: "+idTransitioHie);
		for (int i=0; i<relacaoTransSubpage.size(); i++) {
//			System.out.println("relacaoTransSubpage.get(i).getTransitionID(): "+ relacaoTransSubpage.get(i).getTransitionID());
			if (relacaoTransSubpage.get(i).getTransitionID().equals(idTransitioHie)) {
//				System.out.println("sao iguais");
				return relacaoTransSubpage.get(i).getSubpageID();
			}
		}
		return null;
	}
		
	public ArrayList<ElementoRelacaoHierarquia> getRelacaoTransSubpage() {
		return relacaoTransSubpage;
	}
	
	public boolean existeHierarquiaNaTransicao (String idTransition) {
		String idCompleto = "ID"+idTransition;
		for (int i=0; i<relacaoTransSubpage.size(); i++)
			if (relacaoTransSubpage.get(i).getTransitionID().equals(idCompleto))
				return true;
	
		return false;
	}
	
	public String myToString() {
		String relacoes = "";
		for (int i=0; i<relacaoTransSubpage.size(); i++)
			relacoes = relacoes + "\n" + relacaoTransSubpage.get(i).toString();
		return relacoes;
	}
	
}
