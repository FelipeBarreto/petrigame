package br.ufc.petrigame.netObjects.legacy;
import java.util.ArrayList;


public class Place {

	private ArrayList<Transition> tEntrada;
	private ArrayList<Transition> tSaida;
	
	private int xi;
	private int yi;
	
	private String ID;
	private String name;
	
	private String pageID;
	private boolean hasPort;
	private boolean hasPortOut;
	
	private String initmark;
	private String atualMark;
	
	public Place (String ID, String name, String initmark, String atualMark, int xi, int yi, String pageID, boolean hasPort, boolean hasPortOut) {
		this.ID = ID;
		this.name = name;
		
		this.tEntrada = new ArrayList<Transition>();
		this.tSaida = new ArrayList<Transition>();
		
		this.pageID = pageID;
		this.hasPort = hasPort;
		this.hasPortOut = hasPortOut;
		
		this.initmark = initmark;
		this.atualMark = atualMark;
		
		this.setXi(xi);
		this.setYi(yi);
	}
	
	public String getPageID() {
		return pageID;
	}
	
	public String getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}
	
	public String getInitmark() {
		return initmark;
	}
	
	public String getAtualMark() {
		return atualMark;
	}
	
	public int getXi() {
		return xi;
	}

	public void setXi(int xi) {
		this.xi = xi;
	}

	public int getYi() {
		return yi;
	}

	public void setYi(int yi) {
		this.yi = yi;
	}

	public void setAtualMark(String atualMark) {
		this.atualMark = atualMark;
	}
	
	public void addEntrada (Transition t) {
		tEntrada.add(t);
	}
	
	public void addSaida (Transition t) {
		tSaida.add(t);
	}
	
	public ArrayList<Transition> getEntradas() {
		return tEntrada;
	}
	
	public ArrayList<Transition> getSaidas() {
		return tSaida;
	}
	
	public void setHasPort(boolean hasPort) {
		this.hasPort = hasPort;
	}
	
	public boolean hasPort() {
		return hasPort;
	}
	
	public void setHasPortOut(boolean hasPortOut) {
		this.hasPortOut = hasPortOut;
	}
	
	public boolean hasPortOut() {
		return hasPortOut;
	}

	public Transition getEntradaByID (String ID) {
		for (int i=0; i<tEntrada.size(); i++) 
			if (tEntrada.get(i).getID().equals(ID)) 
				return tEntrada.get(i);		
		
		return null;
	}
	
	public Transition getEntradaByName (String name) {
		for (int i=0; i<tEntrada.size(); i++) 
			if (tEntrada.get(i).getName().equals(name)) 
				return tEntrada.get(i);		
		
		return null;
	}
	
	public Transition getSaidaByID (int ID) {
		for (int i=0; i<tSaida.size(); i++) 
			if (tSaida.get(i).getID().equals(ID)) 
				return tSaida.get(i);		
		
		return null;
	}
	
	public Transition getSaidaByName (String name) {
		for (int i=0; i<tSaida.size(); i++) 
			if (tSaida.get(i).getName().equals(name)) 
				return tSaida.get(i);
		
		return null;
	}

	public boolean popMark (String mark) {
		String[] markSplit = mark.split("`");
		
		if (atualMark.contains("++")) {
			String[] atualMarkSplit = atualMark.split("\\+\\+");
			String newAtualMark = "";
			for (int i = 0; i < atualMarkSplit.length; i++) {
				String[] splitAtualMarkSplit = atualMarkSplit[i].split("`");
				
				//se tiverem o mesmo tipo, decrementar
				if (markSplit[1].equals(splitAtualMarkSplit[1]))
					splitAtualMarkSplit[0] = Integer.toString(Integer.parseInt(splitAtualMarkSplit[0]) - Integer.parseInt(markSplit[0])); 
				
				
				if (Integer.parseInt(splitAtualMarkSplit[0]) != 0) {
					String shrinkMark = splitAtualMarkSplit[0]+"`"+splitAtualMarkSplit[1]; 
					
					if (i==0)
						newAtualMark = shrinkMark;
					else
						newAtualMark = newAtualMark +"++"+shrinkMark;
				}
				
			}
			
			atualMark = newAtualMark;

			return true;
		}
		else {
			String newAtualMark = "";
			String[] splitAtualMarkSplit = atualMark.split("`");
			
			//se tiverem o mesmo tipo, decrementar
			if (markSplit[1].equals(splitAtualMarkSplit[1])) {
				splitAtualMarkSplit[0] = Integer.toString(Integer.parseInt(splitAtualMarkSplit[0]) - Integer.parseInt(markSplit[0]));
				
				if (Integer.parseInt(splitAtualMarkSplit[0]) != 0) {
					String shrinkMark = splitAtualMarkSplit[0]+"`"+splitAtualMarkSplit[1]; 
				
					newAtualMark = shrinkMark;
				}
				else
					newAtualMark = "0`0";
				
				atualMark = newAtualMark;
				return true;
			}
			else {	//nao possui o mesmo tipo. retornar false
				return false;
			}
			
		}
	}
	
	public ArrayList<String> getSeparetedMarksByType () {
		ArrayList<String> marks = new ArrayList<String>();
		
		//se contem mais de um tipo de ficha
		if (atualMark.contains("++")) {		
			String[] separateMarks = atualMark.split("\\+\\+");
			
			for (int i=0; i<separateMarks.length; i++)
				marks.add(separateMarks[i]);			
		}
		
		//se possui apenas um tipo de ficha
		else
			marks.add(atualMark);
		
		
		return marks;		
	}
	
	
	public String toString() {
		return "Place [id =" + getID() + ", " +	"name = " + getName() + ", " +
//				      "initmark = " + getInitmark() + ", "+ 
//				      "x = " + getX() + ", " +
//				      "y = " + getY() + ", " +
				      "pageID = " + pageID + ", " +
				      "hasPort = " + hasPort() + ", " +
				      "hasPortOut = " + hasPortOut() +"]";
	}
	
}
