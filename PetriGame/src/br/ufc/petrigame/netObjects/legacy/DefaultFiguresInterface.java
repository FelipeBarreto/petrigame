package br.ufc.petrigame.netObjects.legacy;

public interface DefaultFiguresInterface 
{
	public String getDefaultFigure( String figure );
} 
