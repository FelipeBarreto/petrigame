package br.ufc.petrigame.netObjects.legacy;

import java.util.ArrayList;

public class Rede {
	
	private String ID;
	private String name;
	private ArrayList<Place> places = new ArrayList<Place>();
	private ArrayList<Transition> transicoes = new ArrayList<Transition>();
	private ArrayList<Arc> arcs = new ArrayList<Arc>();
	
	public Rede (String id) {
		this.ID = id;
	}
	
	public void addPlace (Place p) {
		this.places.add(p);
	}
	
	public void addTransitions (Transition t) {
		this.transicoes.add(t);
	}
	
	public void addArcs (Arc a) {
		this.arcs.add(a);
	}
	
	public String getID() {
		return ID;
	}
	
	public ArrayList<Place> getPlaces() {
		return places;
	}
	
	public ArrayList<Place> getPlacesSemPort() {
		ArrayList<Place> placesSemPort = new ArrayList<Place>();
		
		for(int i=0; i<places.size(); i++)
			if (!places.get(i).hasPort())
				placesSemPort.add(places.get(i));
				
		return placesSemPort;
	}
	
	public ArrayList<Place> getPlacesComPort() {
		ArrayList<Place> placesComPort = new ArrayList<Place>();
		
		for(int i=0; i<places.size(); i++)
			if (places.get(i).hasPort())
				placesComPort.add(places.get(i));
				
		return placesComPort;
	}
	
	public void setPlaces(ArrayList<Place> places) {
		this.places = places;
	}
	
	public void addPlaces (Place place) {
		this.places.add(place);
	}
	
	public ArrayList<Transition> getTransicoes() {
		return transicoes;
	}
	
	public void setTransicoes(ArrayList<Transition> transicoes) {
		this.transicoes = transicoes;
	}
	
	public ArrayList<Arc> getArcs() {
		return arcs;
	}
	
	public void setArcs(ArrayList<Arc> arcs) {
		this.arcs = arcs;
	}
	
	public ArrayList<Arc> getArcsPtoT() {
		ArrayList<Arc> arcsPtoT = new ArrayList<Arc>();
			
		for (int i = 0; i < arcs.size(); i++)
			if (arcs.get(i).getOrientation().equals("PtoT"))
				arcsPtoT.add(arcs.get(i));
		
		return arcsPtoT;
	}
	
	public ArrayList<Arc> getArcsTtoP() {
		ArrayList<Arc> arcsTtoP = new ArrayList<Arc>();
		
		for (int i = 0; i < arcs.size(); i++)
			if (arcs.get(i).getOrientation().equals("TtoP"))
				arcsTtoP.add(arcs.get(i));
		
		return arcsTtoP;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
