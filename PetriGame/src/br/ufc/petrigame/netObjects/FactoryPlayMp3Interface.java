package br.ufc.petrigame.netObjects;

public interface FactoryPlayMp3Interface 
{
	public PlayMp3Interface getPlayMp3Interface( String path );
	
} // fim de interface
