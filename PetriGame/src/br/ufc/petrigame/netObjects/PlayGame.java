package br.ufc.petrigame.netObjects;

import java.util.LinkedList;
import java.util.List;

import static br.ufc.petrigame.netObjects.DrawGameInterface.*;

public class PlayGame 
{
	private RedeProperties redeProperties;
	private RedeHierarquica redeHierarquica;
		
	private double percentualXRelPlayer1 = 0.5;
	private double percentualYRelPlayer1 = 0.5;
	private double percentualXRelPlayer2 = 0.5;
	private double percentualYRelPlayer2 = 0.5;

	public static final String prefixoPlayer1 = "P1_";
	public static final String prefixoPlayer2 = "P2_";
	public static final String prefixoPlayer1Player2 = "P1P2-";

	// prefixo para transi��o de n�vel
	public static final String prefixoPlayer1Out = "P1OUT_";
	public static final String prefixoPlayer1In = "P1IN_";
	public static final String prefixoPlayer2Out = "P2OUT_";
	public static final String prefixoPlayer2In = "P2IN_";
	public static final String prefixoPlayer1Player2Out = "P1P2OUT_";
	public static final String prefixoPlayer1Player2In = "P1P2IN_";

	private boolean player1EmTransicao = false;
	private boolean player2EmTransicao = false;
	
	private double passoAndar = 0.01;
	private double percentualParaDisparo = 0.03; // 3% pr�ximo da periferia da imagem, dispara a transi��o
	private double percentualParaDisparoDiagonal = 0.2; // 2% 

	private int direcaoCorrente1 = 0;
	private int direcaoCorrente2 = 0;
	private int up1 = 0; // indica qual a figura up deve utilizar: 1, 2 ou 3
	private int down1 = 0;
	private int right1 = 0;
	private int left1 = 0;
	private int leftTop1 = 0;
	private int rigthTop1 = 0;
	private int rigthBottom1 = 0;
	private int leftBottom1 = 0;

	private int up2 = 0;
	private int down2 = 0;
	private int right2 = 0;
	private int left2 = 0;
	private int leftTop2 = 0;
	private int rigthTop2 = 0;
	private int rigthBottom2 = 0;
	private int leftBottom2 = 0;

	private LinkedList<String> pilhaRedesSuperioresPlayer1 = new LinkedList<String>();
	private LinkedList<String> pilhaRetornoLugarRedeSuperiorPlayer1 = new LinkedList<String>();
	private LinkedList<String> pilhaRedesSuperioresPlayer2 = new LinkedList<String>();
	private LinkedList<String> pilhaRetornoLugarRedeSuperiorPlayer2 = new LinkedList<String>();
	
	private Audio musicaTocando;
	
	private boolean gameOver;
	
	public PlayGame()
	{

	} // fim de construtor
				
	public RedeProperties getRedeProperties() {
		return redeProperties;
	}

	public void setRedeProperties(RedeProperties redeProperties) {
		this.redeProperties = redeProperties;
	}

	public RedeHierarquica getRedeHierarquica() {
		return redeHierarquica;
	}

	public void setRedeHierarquica(RedeHierarquica redeHierarquica) {
		this.redeHierarquica = redeHierarquica;
	}

	public double getPercentualXRelPlayer1() {
		return percentualXRelPlayer1;
	}

	public void setPercentualXRelPlayer1(double percentualXRelPlayer1) {
		this.percentualXRelPlayer1 = percentualXRelPlayer1;
	}

	public double getPercentualYRelPlayer1() {
		return percentualYRelPlayer1;
	}

	public void setPercentualYRelPlayer1(double percentualYRelPlayer1) {
		this.percentualYRelPlayer1 = percentualYRelPlayer1;
	}

	public double getPercentualXRelPlayer2() {
		return percentualXRelPlayer2;
	}

	public void setPercentualXRelPlayer2(double percentualXRelPlayer2) {
		this.percentualXRelPlayer2 = percentualXRelPlayer2;
	}

	public double getPercentualYRelPlayer2() {
		return percentualYRelPlayer2;
	}

	public void setPercentualYRelPlayer2(double percentualYRelPlayer2) {
		this.percentualYRelPlayer2 = percentualYRelPlayer2;
	}

	public int getDirecaoCorrente1() {
		return direcaoCorrente1;
	}

	public void setDirecaoCorrente1(int direcaoCorrente1) {
		this.direcaoCorrente1 = direcaoCorrente1;
	}

	public int getDirecaoCorrente2() {
		return direcaoCorrente2;
	}

	public void setDirecaoCorrente2(int direcaoCorrente2) {
		this.direcaoCorrente2 = direcaoCorrente2;
	}

	public int getUp1() {
		return up1;
	}

	public void setUp1(int up1) {
		this.up1 = up1;
	}

	public int getDown1() {
		return down1;
	}

	public void setDown1(int down1) {
		this.down1 = down1;
	}

	public int getRight1() {
		return right1;
	}

	public void setRight1(int right1) {
		this.right1 = right1;
	}

	public int getLeft1() {
		return left1;
	}

	public void setLeft1(int left1) {
		this.left1 = left1;
	}

	public int getUp2() {
		return up2;
	}

	public void setUp2(int up2) {
		this.up2 = up2;
	}

	public int getDown2() {
		return down2;
	}

	public void setDown2(int down2) {
		this.down2 = down2;
	}

	public int getRight2() {
		return right2;
	}

	public void setRight2(int right2) {
		this.right2 = right2;
	}

	public int getLeft2() {
		return left2;
	}

	public void setLeft2(int left2) {
		this.left2 = left2;
	}

	public int getLeftTop1() {
		return leftTop1;
	}

	public void setLeftTop1(int leftTop1) {
		this.leftTop1 = leftTop1;
	}

	public int getRigthTop1() {
		return rigthTop1;
	}

	public void setRigthTop1(int rigthTop1) {
		this.rigthTop1 = rigthTop1;
	}

	public int getRigthBottom1() {
		return rigthBottom1;
	}

	public void setRigthBottom1(int rigthBottom1) {
		this.rigthBottom1 = rigthBottom1;
	}

	public int getLeftBottom1() {
		return leftBottom1;
	}

	public void setLeftBottom1(int leftBottom1) {
		this.leftBottom1 = leftBottom1;
	}

	public int getLeftTop2() {
		return leftTop2;
	}

	public void setLeftTop2(int leftTop2) {
		this.leftTop2 = leftTop2;
	}

	public int getRigthTop2() {
		return rigthTop2;
	}

	public void setRigthTop2(int rigthTop2) {
		this.rigthTop2 = rigthTop2;
	}

	public int getRigthBottom2() {
		return rigthBottom2;
	}

	public void setRigthBottom2(int rigthBottom2) {
		this.rigthBottom2 = rigthBottom2;
	}

	public int getLeftBottom2() {
		return leftBottom2;
	}

	public void setLeftBottom2(int leftBottom2) {
		this.leftBottom2 = leftBottom2;
	}

	public Audio getMusicaTocando() 
	{
		return musicaTocando;
	}
	
	public void rightPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualXRelPlayer1 < 1.0 )
		{
			percentualXRelPlayer1 += passoAndar;
			direcaoCorrente1 = DIRECTION_RIGHT;
			right1 = ( right1 + 1 ) % 3;
			;

			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�todo
	
	public void leftPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualXRelPlayer1 > 0.0 )
		{
			percentualXRelPlayer1 -= passoAndar;
			direcaoCorrente1 = DIRECTION_LEFT;
			left1 = ( left1 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�todo
	
	public void upPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualYRelPlayer1 > 0.0 )
		{
			percentualYRelPlayer1 -= passoAndar;
			direcaoCorrente1 = DIRECTION_UP;
			up1 = ( up1 + 1 ) % 3;
			;

			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�todo
	
	public void downPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualYRelPlayer1 < 1.0 )
		{
			percentualYRelPlayer1 += passoAndar;
			direcaoCorrente1 = DIRECTION_DOWN;
			down1 = ( down1 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�todo

	public void rightTopPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualYRelPlayer1 > 0.0 &&
			percentualXRelPlayer1 < 1.0 )
		{
			percentualYRelPlayer1 -= passoAndar/1.4;
			percentualXRelPlayer1 += passoAndar/1.4;
			direcaoCorrente1 = DIRECTION_RIGHT_TOP;
			rigthTop1 = ( rigthTop1 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�todo

	public void leftTopPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualYRelPlayer1 > 0.0 &&
			percentualXRelPlayer1 > 0.0 )
		{
			percentualYRelPlayer1 -= passoAndar/1.4;
			percentualXRelPlayer1 -= passoAndar/1.4;
			direcaoCorrente1 = DIRECTION_LEFT_TOP;
			leftTop1 = ( leftTop1 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�tod

	public void rightBottomPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualYRelPlayer1 < 1.0 &&
			percentualXRelPlayer1 < 1.0 )
		{
			percentualYRelPlayer1 += passoAndar/1.4;
			percentualXRelPlayer1 += passoAndar/1.4;
			direcaoCorrente1 = DIRECTION_RIGHT_BOTTOM;
			rigthBottom1 = ( rigthBottom1 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�todo

	public void leftBottomPersonagem1()
	{
		if( !gameOver && !player1EmTransicao && percentualYRelPlayer1 < 1.0 &&
			percentualXRelPlayer1 > 0.0 )
		{
			percentualYRelPlayer1 += passoAndar/1.4;
			percentualXRelPlayer1 -= passoAndar/1.4;
			direcaoCorrente1 = DIRECTION_LEFT_BOTTOM;
			leftBottom1 = ( leftBottom1 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem1();
		} // fim de if
	} // fim de m�tod
	
	public void rightPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualXRelPlayer2 < 1.0 )
		{
			percentualXRelPlayer2 += passoAndar;
			direcaoCorrente2 = DIRECTION_RIGHT;
			right2 = ( right2 + 1 ) % 3;
			;

			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�todo
	
	public void leftPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualXRelPlayer2 > 0.0 )
		{
			percentualXRelPlayer2 -= passoAndar;
			direcaoCorrente2 = DIRECTION_LEFT;
			left2 = ( left2 + 1 ) % 3;
			;

			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�todo
	
	public void upPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualYRelPlayer2 > 0.0 )
		{
			percentualYRelPlayer2 -= passoAndar;
			direcaoCorrente2 = DIRECTION_UP;
			up2 = ( up2 + 1 ) % 3;
			;

			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�todo
	
	public void downPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualYRelPlayer2 < 1.0 )
		{
			percentualYRelPlayer2 += passoAndar;
			direcaoCorrente2 = DIRECTION_DOWN;
			down2 = ( down2 + 1 ) % 3;
			;

			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�todo

	public void rightTopPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualYRelPlayer2 > 0.0 &&
			percentualXRelPlayer2 < 1.0 )
		{
			percentualYRelPlayer2 -= passoAndar/1.4;
			percentualXRelPlayer2 += passoAndar/1.4;
			direcaoCorrente2 = DIRECTION_RIGHT_TOP;
			rigthTop2 = ( rigthTop2 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�todo

	public void leftTopPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualYRelPlayer2 > 0.0 &&
			percentualXRelPlayer2 > 0.0 )
		{
			percentualYRelPlayer2 -= passoAndar/1.4;
			percentualXRelPlayer2 -= passoAndar/1.4;
			direcaoCorrente2 = DIRECTION_LEFT_TOP;
			leftTop2 = ( leftTop2 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�tod

	public void rightBottomPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualYRelPlayer2 < 1.0 &&
			percentualXRelPlayer2 < 1.0 )
		{
			percentualYRelPlayer2 += passoAndar/1.4;
			percentualXRelPlayer2 += passoAndar/1.4;
			direcaoCorrente2 = DIRECTION_RIGHT_BOTTOM;
			rigthBottom2 = ( rigthBottom2 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�todo

	public void leftBottomPersonagem2()
	{
		if( !gameOver && !player2EmTransicao && percentualYRelPlayer2 < 1.0 &&
			percentualXRelPlayer2 > 0.0 )
		{
			percentualYRelPlayer2 += passoAndar/1.4;
			percentualXRelPlayer2 -= passoAndar/1.4;
			direcaoCorrente2 = DIRECTION_LEFT_BOTTOM;
			leftBottom2 = ( leftBottom2 + 1 ) % 3;
			;
			
			verificarDisparoPersonagem2();
		} // fim de if
	} // fim de m�tod
	
	private void verificarDisparoPersonagem1()
	{
		if( percentualXRelPlayer1 + percentualParaDisparo >= 1.0 ||
			percentualXRelPlayer1 <= percentualParaDisparo ||
			percentualYRelPlayer1 <= percentualParaDisparo ||
			percentualYRelPlayer1 + percentualParaDisparo >= 1.0 )
			efetuarDisparoTransicaoPlayer1();
	} // fim de m�todo

	private void verificarDisparoPersonagem2()
	{
		if( percentualXRelPlayer2 + percentualParaDisparo >= 1.0 ||
			percentualXRelPlayer2 <= percentualParaDisparo ||
			percentualYRelPlayer2 <= percentualParaDisparo ||
			percentualYRelPlayer2 + percentualParaDisparo >= 1.0 )
			efetuarDisparoTransicaoPlayer2();
	} // fim de m�todo
	
	private synchronized void efetuarDisparoTransicaoPlayer1()
	{
		List<TransicaoProperties> transicoes = null;
		LugarProperties lugarDestino = null;
		int direcao = DIRECTION_UP, idDisparo = 1;
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeHierarquica redeHierarquica1 = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		
		// recupera os lugares de origem
		LugarProperties lugarOrigem = redePlayer1.getLugarPropertiesPlayer1( redeHierarquica1 );
		
		// verifica possibilidade de disparo na diagora superior-direita
		if( direcaoCorrente1 == DIRECTION_RIGHT_TOP && percentualXRelPlayer1 >= 1 - percentualParaDisparoDiagonal &&
			percentualYRelPlayer1 <= percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha() - 1, lugarOrigem.getPosColuna() + 1 );
			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_RIGHT_TOP;
		} // fim de if
		// verifica possibilidade de disparo na diagora superior-esquerda
		else if( direcaoCorrente1 == DIRECTION_LEFT_TOP && percentualXRelPlayer1 <= percentualParaDisparoDiagonal &&
			percentualYRelPlayer1 <= percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha() - 1, lugarOrigem.getPosColuna() - 1 );
			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_LEFT_TOP;
		} // fim de if
		// verifica possibilidade de disparo na diagora inferior-esquerda
		else if( direcaoCorrente1 == DIRECTION_LEFT_BOTTOM && percentualXRelPlayer1 <= percentualParaDisparoDiagonal &&
			percentualYRelPlayer1 >= 1 - percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha() + 1, lugarOrigem.getPosColuna() - 1 );
			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_LEFT_BOTTOM;
		} // fim de if
		// verifica possibilidade de disparo na diagora inferior-direita
		else if( direcaoCorrente1 == DIRECTION_RIGHT_BOTTOM && percentualXRelPlayer1 >= 1 - percentualParaDisparoDiagonal &&
			percentualYRelPlayer1 >= 1 - percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha() + 1, lugarOrigem.getPosColuna() + 1 );
			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_RIGHT_BOTTOM;
		} // fim de if
		// verifica a possibilidade de disparo para esquerda
		else if( percentualXRelPlayer1 <= percentualParaDisparo )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha(), lugarOrigem.getPosColuna() - 1 );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_LEFT;

			// verifica a possibilidade de disparo duplo
			if( percentualXRelPlayer2 <= percentualParaDisparo )
				transicoes.addAll( redePlayer1.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if
		// verifica a possibilidade de disparo para direita
		else if( percentualXRelPlayer1 >= 1 - percentualParaDisparo )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha(), lugarOrigem.getPosColuna() + 1 );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_RIGHT;
			
			// verifica a possibilidade de disparo duplo
			if( percentualXRelPlayer2 >= 1 - percentualParaDisparo )
				transicoes.addAll( redePlayer1.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if
		// verifica a possibilidade de disparo para cima
		else if( percentualYRelPlayer1 <= percentualParaDisparo )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha() - 1, lugarOrigem.getPosColuna() );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_UP;
			
			// verifica a possibilidade de disparo duplo
			if( percentualYRelPlayer2 <= percentualParaDisparo )
				transicoes.addAll( redePlayer1.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if
		// verifica a possibilidade de disparo para baixo
		else if( percentualYRelPlayer1 >= 1 - percentualParaDisparo )
		{
			lugarDestino = redePlayer1.getLugarProperties( lugarOrigem.getPosLinha() + 1, lugarOrigem.getPosColuna() );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_DOWN;
			
			// verifica a possibilidade de disparo duplo
			if( percentualYRelPlayer2 >= 1 - percentualParaDisparo )
				transicoes.addAll( redePlayer1.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if 	
		
		// verifica se est� habilitada e executa o disparo
		for( int i = 0; transicoes != null && i < transicoes.size(); i++ )
		{			
			if( redeHierarquica1.isTransicaoHabilitada( transicoes.get( i ).getNome() ) )
			{
				// verifica se � uma transi��o de n�vel
				if( transicoes.get( i ).getRedeNivelInferior() != null )
				{
					// executa o efeito sonoro
					if( transicoes.get( i ).getEfeitoSonoro() != null )
						transicoes.get( i ).getEfeitoSonoro().play();
					
					// verifica se � transi��o dupla
					if( transicoes.get( i ).getNome().contains( prefixoPlayer1Player2 ) )
						dispararTransicaoNivelInferiorPlayer1Player2( transicoes.get( i ), direcao );
					else // transi��o simples
						dispararTransicaoNivelInferiorPlayer1( transicoes.get( i ), direcao );
					playMusicGame();
					//;
					return;
				} // fim de if
				// verifica se � transi��o dupla
				else if( transicoes.get( i ).getNome().contains( prefixoPlayer1Player2 ) )
				{
					if( direcao == DIRECTION_UP )
						percentualYRelPlayer2 = 1.0;
					else if( direcao == DIRECTION_DOWN )
						percentualYRelPlayer2 = 0.0;
					else if( direcao == DIRECTION_LEFT )
						percentualXRelPlayer2 = 1.0;
					else if( direcao == DIRECTION_RIGHT )
						percentualXRelPlayer2 = 0.0;

					// executa anima��o do player 2
					player2EmTransicao = true;		
					idDisparo = 3;
					new AnimationThread( 2, 3, direcao ).start();
				} // fim de else if
				
				// executa a transi��o
				redeHierarquica1.executeTransicao( transicoes.get( i ).getNome() );
				
				// executa o efeito sonoro
				if( transicoes.get( i ).getEfeitoSonoro() != null )
					transicoes.get( i ).getEfeitoSonoro().play();
				
				if( direcao == DIRECTION_UP )
					percentualYRelPlayer1 = 1.0;
				else if( direcao == DIRECTION_DOWN )
					percentualYRelPlayer1 = 0.0;
				else if( direcao == DIRECTION_LEFT )
					percentualXRelPlayer1 = 1.0;
				else if( direcao == DIRECTION_RIGHT )
					percentualXRelPlayer1 = 0.0;
				else if( direcao == DIRECTION_RIGHT_TOP )
				{
					percentualYRelPlayer1 = 1.0;
					percentualXRelPlayer1 = 0.0;
				} // fim de else if
				else if( direcao == DIRECTION_LEFT_TOP )
				{
					percentualYRelPlayer1 = 1.0;
					percentualXRelPlayer1 = 1.0;
				} // fim de else if
				else if( direcao == DIRECTION_RIGHT_BOTTOM )
				{
					percentualYRelPlayer1 = 0.0;
					percentualXRelPlayer1 = 0.0;
				} // fim de else if
				else if( direcao == DIRECTION_LEFT_BOTTOM )
				{
					percentualYRelPlayer1 = 0.0;
					percentualXRelPlayer1 = 1.0;
				} // fim de else if

				// executa anima��o
				player1EmTransicao = true;							
				new AnimationThread( 1, idDisparo, direcao ).start();
				break;
			} // fim de if
		} // fim de if
	} // fim de m�todo
	
	private synchronized void efetuarDisparoTransicaoPlayer2()
	{
		List<TransicaoProperties> transicoes = null;
		LugarProperties lugarDestino = null;
		int direcao = DIRECTION_UP, idDisparo = 2;
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		RedeHierarquica redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
		
		// recupera os lugares de origem
		LugarProperties lugarOrigem = redePlayer2.getLugarPropertiesPlayer2( redeHierarquica2 );
		// verifica possibilidade de disparo na diagora superior-direita
		if( direcaoCorrente2 == DIRECTION_RIGHT_TOP && percentualXRelPlayer2 >= 1 - percentualParaDisparoDiagonal &&
			percentualYRelPlayer2 <= percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha() - 1, lugarOrigem.getPosColuna() + 1 );
					
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_RIGHT_TOP;
		} // fim de if
		// verifica possibilidade de disparo na diagora superior-esquerda
		else if( direcaoCorrente2 == DIRECTION_LEFT_TOP && percentualXRelPlayer2 <= percentualParaDisparoDiagonal &&
			percentualYRelPlayer2 <= percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha() - 1, lugarOrigem.getPosColuna() - 1 );
					
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_LEFT_TOP;
		} // fim de if
		// verifica possibilidade de disparo na diagora inferior-esquerda
		else if( direcaoCorrente2 == DIRECTION_LEFT_BOTTOM && percentualXRelPlayer2 <= percentualParaDisparoDiagonal &&
			percentualYRelPlayer2 >= 1 - percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha() + 1, lugarOrigem.getPosColuna() - 1 );
					
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_LEFT_BOTTOM;
		} // fim de if
		// verifica possibilidade de disparo na diagora inferior-direita
		else if( direcaoCorrente2 == DIRECTION_RIGHT_BOTTOM && percentualXRelPlayer2 >= 1 - percentualParaDisparoDiagonal &&
			percentualYRelPlayer2 >= 1 - percentualParaDisparoDiagonal )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha() + 1, lugarOrigem.getPosColuna() + 1 );
					
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_RIGHT_BOTTOM;
		} // fim de if
				
		// verifica a possibilidade de disparo para esquerda
		else if( percentualXRelPlayer2 <= percentualParaDisparo )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha(), lugarOrigem.getPosColuna() - 1 );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_LEFT;
			
			// verifica a possibilidade de disparo duplo
			if( percentualXRelPlayer1 <= percentualParaDisparo )
				transicoes.addAll( redePlayer2.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if
		// verifica a possibilidade de disparo para direita
		else if( percentualXRelPlayer2 >= 1 - percentualParaDisparo )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha(), lugarOrigem.getPosColuna() + 1 );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_RIGHT;

			// verifica a possibilidade de disparo duplo
			if( percentualXRelPlayer1 >= 1 - percentualParaDisparo )
				transicoes.addAll( redePlayer2.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if
		// verifica a possibilidade de disparo para cima
		else if( percentualYRelPlayer2 <= percentualParaDisparo )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha() - 1, lugarOrigem.getPosColuna() );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_UP;

			// verifica a possibilidade de disparo duplo
			if( percentualYRelPlayer1 <= percentualParaDisparo )
				transicoes.addAll( redePlayer2.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if
		// verifica a possibilidade de disparo para baixo
		else if( percentualYRelPlayer2 >= 1 - percentualParaDisparo )
		{
			lugarDestino = redePlayer2.getLugarProperties( lugarOrigem.getPosLinha() + 1, lugarOrigem.getPosColuna() );			
			// recupera a transi��o entre origem e destino
			transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2, lugarOrigem, lugarDestino );
			// define a dire��o
			direcao = DIRECTION_DOWN;

			// verifica a possibilidade de disparo duplo
			if( percentualYRelPlayer1 >= 1 - percentualParaDisparo )
				transicoes.addAll( redePlayer2.getTransicoesProperties( prefixoPlayer1Player2, lugarOrigem, lugarDestino ) );
		} // fim de if 	
		
		// verifica se est� habilitada e executa o disparo
		for( int i = 0; transicoes != null && i < transicoes.size(); i++ )
		{
			if( redeHierarquica2.isTransicaoHabilitada( transicoes.get( i ).getNome() ) )
			{
				// verifica se � uma transi��o de n�vel
				if( transicoes.get( i ).getRedeNivelInferior() != null )
				{
					// executa o efeito sonoro
					if( transicoes.get( i ).getEfeitoSonoro() != null )
						transicoes.get( i ).getEfeitoSonoro().play();
					
					// verifica se � transi��o dupla
					if( transicoes.get( i ).getNome().contains( prefixoPlayer1Player2 ) )
						dispararTransicaoNivelInferiorPlayer1Player2( transicoes.get( i ), direcao );
					else
						dispararTransicaoNivelInferiorPlayer2( transicoes.get( i ), direcao );
					playMusicGame();
					//;
					return;
				} // fim de if
				// verifica se � transi��o dupla
				else if( transicoes.get( i ).getNome().contains( prefixoPlayer1Player2 ) )
				{
					if( direcao == DIRECTION_UP )
						percentualYRelPlayer1 = 1.0;
					else if( direcao == DIRECTION_DOWN )
						percentualYRelPlayer1 = 0.0;
					else if( direcao == DIRECTION_LEFT )
						percentualXRelPlayer1 = 1.0;
					else if( direcao == DIRECTION_RIGHT )
						percentualXRelPlayer1 = 0.0;

					// executa anima��o do player 1
					player1EmTransicao = true;			
					idDisparo = 3;
					new AnimationThread( 1, 3, direcao ).start();
				} // fim de else if
				
				// executa a transi��o
				redeHierarquica2.executeTransicao( transicoes.get( i ).getNome() );
				
				// executa o efeito sonoro
				if( transicoes.get( i ).getEfeitoSonoro() != null )
					transicoes.get( i ).getEfeitoSonoro().play();
				
				if( direcao == DIRECTION_UP )
					percentualYRelPlayer2 = 1.0;
				else if( direcao == DIRECTION_DOWN )
					percentualYRelPlayer2 = 0.0;
				else if( direcao == DIRECTION_LEFT )
					percentualXRelPlayer2 = 1.0;
				else if( direcao == DIRECTION_RIGHT )
					percentualXRelPlayer2 = 0.0;
				else if( direcao == DIRECTION_RIGHT_TOP )
				{
					percentualYRelPlayer2 = 1.0;
					percentualXRelPlayer2 = 0.0;
				} // fim de else if
				else if( direcao == DIRECTION_LEFT_TOP )
				{
					percentualYRelPlayer2 = 1.0;
					percentualXRelPlayer2 = 1.0;
				} // fim de else if
				else if( direcao == DIRECTION_RIGHT_BOTTOM )
				{
					percentualYRelPlayer2 = 0.0;
					percentualXRelPlayer2 = 0.0;
				} // fim de else if
				else if( direcao == DIRECTION_LEFT_BOTTOM )
				{
					percentualYRelPlayer2 = 0.0;
					percentualXRelPlayer2 = 1.0;
				} // fim de else if
				
				// executa anima��o
				player2EmTransicao = true;							
				new AnimationThread( 2, idDisparo, direcao ).start();
				break;
			} // fim de if
		} // fim de if
	} // fim de m�todo
		
	private void dispararTransicaoNivelInferiorPlayer1( TransicaoProperties transicaoProperties, int direcao )
	{
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeHierarquica redeHierarquicaSup = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		
		LugarProperties lugarPontoEntrada = transicaoProperties.getRedeNivelInferior().getLugarPropertiesPontoEntrada();		
		if( lugarPontoEntrada != null )
		{
			// executa in do player 1 na rede inferior
			boolean inOk = false;
			List<TransicaoProperties> transicoes = transicaoProperties.getRedeNivelInferior().getTransicoesProperties( prefixoPlayer1In, lugarPontoEntrada, lugarPontoEntrada );
			RedeHierarquica redeHierarquicaInf = redeHierarquica.getSubRedeHierarquica( transicaoProperties.getRedeNivelInferior().getNome() );
			for( TransicaoProperties t : transicoes )
			{
				if( redeHierarquicaInf.isTransicaoHabilitada( t.getNome() ) )
				{
					redeHierarquicaInf.executeTransicao( t.getNome() );
					inOk = true;
					break;
				} // fim de if
			} // fim de for
						
			// executa out do player 1 na rede superior
			if( inOk )
			{
				transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1Out, transicaoProperties.getLugarOrigemProperties(),
					transicaoProperties.getLugarOrigemProperties() );
				for( TransicaoProperties t : transicoes )
				{
					if( redeHierarquicaSup.isTransicaoHabilitada( t.getNome() ) )
					{
						redeHierarquicaSup.executeTransicao( t.getNome() );
						break;
					} // fim de if
				} // fim de for
					
				percentualYRelPlayer1 = 0.5;
				percentualXRelPlayer1 = 0.5;
				
				// define que a rede do personagem 1 � a inferior
				pilhaRedesSuperioresPlayer1.push( redeHierarquicaSup.getNome() );
				pilhaRetornoLugarRedeSuperiorPlayer1.push( transicaoProperties.getLugarDestinoProperties().getNome() );
				checkReturnPlayer();
			} // fim de if
		} // fim de if
		else
		{
			// n�o existindo ponto de entrada na rede inferior � executado a transi��o para o estado de sa�da da transi��o de n�vel
			redeHierarquicaSup.executeTransicao( transicaoProperties.getNome() );
			
			// executa anima��o
			player1EmTransicao = true;							
			new AnimationThread( 1, 1, direcao ).start();			
		} // fim de else
	} // fim de m�todo

	private void dispararTransicaoNivelInferiorPlayer2( TransicaoProperties transicaoProperties, int direcao )
	{
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		RedeHierarquica redeHierarquicaSup = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
		
		LugarProperties lugarPontoEntrada = transicaoProperties.getRedeNivelInferior().getLugarPropertiesPontoEntrada();	
		if( lugarPontoEntrada != null )
		{
			// executa in do player 2 na rede inferior
			boolean inOk = false;
			List<TransicaoProperties> transicoes = transicaoProperties.getRedeNivelInferior().getTransicoesProperties( prefixoPlayer2In, lugarPontoEntrada, lugarPontoEntrada );
			RedeHierarquica redeHierarquicaInf = redeHierarquica.getSubRedeHierarquica( transicaoProperties.getRedeNivelInferior().getNome() );
			for( TransicaoProperties t : transicoes )
			{
				if( redeHierarquicaInf.isTransicaoHabilitada( t.getNome() ) )
				{
					redeHierarquicaInf.executeTransicao( t.getNome() );
					inOk = true;
					break;
				} // fim de if
			} // fim de for
						
			// executa out do player 2 na rede superior
			if( inOk )
			{
				transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2Out, transicaoProperties.getLugarOrigemProperties(),
					transicaoProperties.getLugarOrigemProperties() );
				for( TransicaoProperties t : transicoes )
				{
					if( redeHierarquicaSup.isTransicaoHabilitada( t.getNome() ) )
					{
						redeHierarquicaSup.executeTransicao( t.getNome() );
						break;
					} // fim de if
				} // fim de for
			
				percentualYRelPlayer2 = 0.5;
				percentualXRelPlayer2 = 0.5;
				
				// define que a rede do personagem 2 � a inferior
				pilhaRedesSuperioresPlayer2.push( redeHierarquicaSup.getNome() );
				pilhaRetornoLugarRedeSuperiorPlayer2.push( transicaoProperties.getLugarDestinoProperties().getNome() );
				checkReturnPlayer();
			} // fim de if
		} // fim de if
		else
		{
			// n�o existindo ponto de entrada na rede inferior � executado a transi��o para o estado de sa�da da transi��o de n�vel
			redeHierarquicaSup.executeTransicao( transicaoProperties.getNome() );
			
			// executa anima��o
			player2EmTransicao = true;							
			new AnimationThread( 2, 2, direcao ).start();			
		} // fim de else
	} // fim de m�todo

	private void dispararTransicaoNivelInferiorPlayer1Player2( TransicaoProperties transicaoProperties, int direcao )
	{
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeHierarquica redeHierarquicaSup = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		
		LugarProperties lugarPontoEntrada = transicaoProperties.getRedeNivelInferior().getLugarPropertiesPontoEntrada();		
		if( lugarPontoEntrada != null )
		{
			// executa in do player 1 e 2 na rede inferior
			boolean inOk = false;
			List<TransicaoProperties> transicoes = transicaoProperties.getRedeNivelInferior().getTransicoesProperties( prefixoPlayer1Player2In, lugarPontoEntrada, lugarPontoEntrada );
			RedeHierarquica redeHierarquicaInf = redeHierarquica.getSubRedeHierarquica( transicaoProperties.getRedeNivelInferior().getNome() );
			for( TransicaoProperties t : transicoes )
			{
				if( redeHierarquicaInf.isTransicaoHabilitada( t.getNome() ) )
				{
					redeHierarquicaInf.executeTransicao( t.getNome() );
					inOk = true;
					break;
				} // fim de if
			} // fim de for
						
			if( inOk )
			{
				// executa out do player 1 e 2 na rede superior
				transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1Player2Out, transicaoProperties.getLugarOrigemProperties(),
					transicaoProperties.getLugarOrigemProperties() );
				for( TransicaoProperties t : transicoes )
				{
					if( redeHierarquicaSup.isTransicaoHabilitada( t.getNome() ) )
					{
						redeHierarquicaSup.executeTransicao( t.getNome() );
						break;
					} // fim de if
				} // fim de for
				
				
				percentualXRelPlayer1 = 0.35;
				percentualYRelPlayer1 = 0.5;
				percentualXRelPlayer2 = 0.65;
				percentualYRelPlayer2 = 0.5;
	
				// guarda referencias para as redes superiores
				pilhaRedesSuperioresPlayer1.push( redeHierarquicaSup.getNome() );
				pilhaRetornoLugarRedeSuperiorPlayer1.push( transicaoProperties.getLugarDestinoProperties().getNome() );
				pilhaRedesSuperioresPlayer2.push( redeHierarquicaSup.getNome() );
				pilhaRetornoLugarRedeSuperiorPlayer2.push( transicaoProperties.getLugarDestinoProperties().getNome() );
				checkReturnPlayer1Player2();
			} // fim de if
		} // fim de if
		else
		{
			// n�o existindo ponto de entrada na rede inferior � executado a transi��o para o estado de sa�da da transi��o de n�vel
			redeHierarquicaSup.executeTransicao( transicaoProperties.getNome() );
			
			// executa anima��o
			player1EmTransicao = true;							
			new AnimationThread( 1, 3, direcao ).start();	
			player2EmTransicao = true;							
			new AnimationThread( 2, 3, direcao ).start();			
		} // fim de else
	} // fim de m�todo

	private void ajustarPosicaoPlayers()
	{
		// recupera a rede dos players
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( getRedeProperties(), getRedeHierarquica() );
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( getRedeProperties(), getRedeHierarquica() );
				
		// recupera os lugares dos players
		LugarProperties lugarPlayer1 = null;
		LugarProperties lugarPlayer2 = null;
				
		if( redePlayer1 != null )
			lugarPlayer1 = redePlayer1.getLugarPropertiesPlayer1( getRedeHierarquica().getSubRedeHierarquica( redePlayer1.getNome() ) );
		if( redePlayer2 != null )
			lugarPlayer2 = redePlayer2.getLugarPropertiesPlayer2( getRedeHierarquica().getSubRedeHierarquica( redePlayer2.getNome() ) );
						
		// configura posi��o inicial dos players
		if( lugarPlayer1 == lugarPlayer2 && lugarPlayer1 != null )
		{
			setPercentualXRelPlayer1( 0.35 );
			setPercentualXRelPlayer2( 0.65 );
		} // fim de if
		else
		{
			setPercentualXRelPlayer1( 0.5 );
			setPercentualXRelPlayer2( 0.5 );			
		} // fimd de else
				
		setPercentualYRelPlayer1( 0.5 );
		setPercentualYRelPlayer2( 0.5 );
	} // fim de m�todo
	
	private void outPlayer1( RedeProperties redePlayer1, RedeHierarquica redeHierarquica1, LugarProperties lugarPlayer1 )
	{
		// executa out do player 1 na rede inferior
		List<TransicaoProperties> transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1Out, lugarPlayer1, lugarPlayer1 );
		for( TransicaoProperties t : transicoes )
		{
			if( redeHierarquica1.isTransicaoHabilitada( t.getNome() ) )
			{
				redeHierarquica1.executeTransicao( t.getNome() );
				break;
			} // fim de if
		} // fim de for
	} // fim de m�todo
	
	private void returnPlayer1()
	{
		RedeProperties redePlayer1;
		RedeHierarquica redeHierarquica1;
		LugarProperties lugarPlayer1;
		EstadoLugar estadoLugarPlayer1, eProx;

		// busca o personagem do player
		redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		redeHierarquica1 = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		lugarPlayer1 = redePlayer1.getLugarPropertiesPlayer1( redeHierarquica1 );			
		estadoLugarPlayer1 = lugarPlayer1.getEstadoLugar( redeHierarquica1.getValorFichaAtualNoLugar( lugarPlayer1.getNome() ) );
		Personagem p = estadoLugarPlayer1.getPersonagemPresente1();
		
		// procura um estado n�o final nas redes superiores 
		do
		{
			if( pilhaRedesSuperioresPlayer1.isEmpty() )
			{
				// exeuta o return do player 1 na rede superior		
				List<TransicaoProperties> transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1In, lugarPlayer1, lugarPlayer1 );
				for( TransicaoProperties t : transicoes )
				{
					if( redeHierarquica1.isTransicaoHabilitada( t.getNome() ) )
					{
						redeHierarquica1.executeTransicao( t.getNome() );
						break;
					} // fim de if
				} // fim de for

				setPercentualXRelPlayer1( 0.5 );
				setPercentualYRelPlayer1( 0.5 );
				// finaliza o jogo
				gameOver = true;
				return;
			} // fim de if
			
			String nomeRede = pilhaRedesSuperioresPlayer1.pop();
			String lugar = pilhaRetornoLugarRedeSuperiorPlayer1.pop();
			// busca a rede superior
			redePlayer1 = RedeProperties.getSubRedeProperties( redeProperties, nomeRede );
			redeHierarquica1 = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );	
			lugarPlayer1 = redePlayer1.getLugarProperties( lugar );					
			// verifica o pr�ximo estado da rede superior
			estadoLugarPlayer1 = lugarPlayer1.getEstadoLugar( redeHierarquica1.getValorFichaAtualNoLugar( lugarPlayer1.getNome() ) );			
			eProx = redePlayer1.getEstadoLugar( p, estadoLugarPlayer1.getPersonagemPresente2(), 
				estadoLugarPlayer1.getObjetoPresente(), estadoLugarPlayer1, lugarPlayer1 );
		} while( eProx.isFinal() || lugarPlayer1.isPontoSaida() );
		
		// exeuta o return do player 1 na rede superior		
		List<TransicaoProperties> transicoes = redePlayer1.getTransicoesProperties( prefixoPlayer1In, lugarPlayer1, lugarPlayer1 );
		for( TransicaoProperties t : transicoes )
		{
			if( redeHierarquica1.isTransicaoHabilitada( t.getNome() ) )
			{
				redeHierarquica1.executeTransicao( t.getNome() );
				break;
			} // fim de if
		} // fim de for

		setPercentualXRelPlayer1( 0.5 );
		setPercentualYRelPlayer1( 0.5 );
	} // fim de m�todo
	
	private void outPlayer2( RedeProperties redePlayer2, RedeHierarquica redeHierarquica2, LugarProperties lugarPlayer2 )
	{
		// executa out do player 1 na rede inferior
		List<TransicaoProperties> transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2Out, lugarPlayer2, lugarPlayer2 );
		for( TransicaoProperties t : transicoes )
		{
			if( redeHierarquica2.isTransicaoHabilitada( t.getNome() ) )
			{
				redeHierarquica2.executeTransicao( t.getNome() );
				break;
			} // fim de if
		} // fim de for
	} // fim de m�todo

	private void returnPlayer2()
	{		
		RedeProperties redePlayer2;
		RedeHierarquica redeHierarquica2;
		LugarProperties lugarPlayer2;
		EstadoLugar estadoLugarPlayer2, eProx;

		// busca o personagem do player
		redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
		lugarPlayer2 = redePlayer2.getLugarPropertiesPlayer2( redeHierarquica2 );			
		estadoLugarPlayer2 = lugarPlayer2.getEstadoLugar( redeHierarquica2.getValorFichaAtualNoLugar( lugarPlayer2.getNome() ) );
		Personagem p = estadoLugarPlayer2.getPersonagemPresente2();
		
		// procura um estado n�o final nas redes superiores 
		do
		{
			if( pilhaRedesSuperioresPlayer2.isEmpty() )
			{
				// exeuta o return do player 1 na rede superior		
				List<TransicaoProperties> transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2In, lugarPlayer2, lugarPlayer2 );
				for( TransicaoProperties t : transicoes )
				{
					if( redeHierarquica2.isTransicaoHabilitada( t.getNome() ) )
					{
						redeHierarquica2.executeTransicao( t.getNome() );
						break;
					} // fim de if
				} // fim de for

				setPercentualXRelPlayer2( 0.5 );
				setPercentualYRelPlayer2( 0.5 ); 
				// finaliza o jogo
				gameOver = true;
				return;
			} // fim de if
			
			String nomeRede = pilhaRedesSuperioresPlayer2.pop();
			String lugar = pilhaRetornoLugarRedeSuperiorPlayer2.pop();
			// busca a rede superior
			redePlayer2 = RedeProperties.getSubRedeProperties( redeProperties, nomeRede );
			redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );	
			lugarPlayer2 = redePlayer2.getLugarProperties( lugar );					
			// verifica o pr�ximo estado da rede superior
			estadoLugarPlayer2 = lugarPlayer2.getEstadoLugar( redeHierarquica2.getValorFichaAtualNoLugar( lugarPlayer2.getNome() ) );			
			eProx = redePlayer2.getEstadoLugar( estadoLugarPlayer2.getPersonagemPresente1(), p, 
				estadoLugarPlayer2.getObjetoPresente(), estadoLugarPlayer2, lugarPlayer2 );
		} while( eProx.isFinal() || lugarPlayer2.isPontoSaida() );
		
		// exeuta o return do player 1 na rede superior		
		List<TransicaoProperties> transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer2In, lugarPlayer2, lugarPlayer2 );
		for( TransicaoProperties t : transicoes )
		{
			if( redeHierarquica2.isTransicaoHabilitada( t.getNome() ) )
			{
				redeHierarquica2.executeTransicao( t.getNome() );
				break;
			} // fim de if
		} // fim de for

		setPercentualXRelPlayer2( 0.5 );
		setPercentualYRelPlayer2( 0.5 ); 
	} // fim de m�todo

	private void outPlayer1Player2( RedeProperties redePlayer, RedeHierarquica redeHierarquica, LugarProperties lugarPlayer )
	{
		// executa out do player 1 e 2 na rede inferior
		List<TransicaoProperties> transicoes = redePlayer.getTransicoesProperties( prefixoPlayer1Player2Out, lugarPlayer, lugarPlayer );
		for( TransicaoProperties t : transicoes )
		{
			if( redeHierarquica.isTransicaoHabilitada( t.getNome() ) )
			{
				redeHierarquica.executeTransicao( t.getNome() );
				break;
			} // fim de if
		} // fim de for
	} // fim de m�todo

	private void returnPlayer1Player2()
	{
		RedeProperties redePlayer2;
		RedeHierarquica redeHierarquica2;
		LugarProperties lugarPlayer2;
		EstadoLugar estadoLugarPlayer2, eProx;

		// busca o personagem do player
		redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
		lugarPlayer2 = redePlayer2.getLugarPropertiesPlayer2( redeHierarquica2 );			
		estadoLugarPlayer2 = lugarPlayer2.getEstadoLugar( redeHierarquica2.getValorFichaAtualNoLugar( lugarPlayer2.getNome() ) );
		Personagem p1 = estadoLugarPlayer2.getPersonagemPresente1();
		Personagem p2 = estadoLugarPlayer2.getPersonagemPresente2();
		
		// procura um estado n�o final nas redes superiores 
		do
		{
			if( pilhaRedesSuperioresPlayer1.isEmpty() || pilhaRedesSuperioresPlayer2.isEmpty() )
			{
				// executa o return do player 1 e 2 na rede superior
				List<TransicaoProperties> transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer1Player2In, lugarPlayer2, lugarPlayer2 );
				for( TransicaoProperties t : transicoes )
				{
					if( redeHierarquica2.isTransicaoHabilitada( t.getNome() ) )
					{
						redeHierarquica2.executeTransicao( t.getNome() );
						break;
					} // fim de if
				} // fim de for
					
				setPercentualXRelPlayer1( 0.35 );
				setPercentualYRelPlayer1( 0.5 );
							
				setPercentualXRelPlayer2( 0.65 );
				setPercentualYRelPlayer2( 0.5 );
				// finaliza o jogo
				gameOver = true;
				return;
			} // fim de if
			
			String nomeRede1 = pilhaRedesSuperioresPlayer1.pop();
			String lugar1 = pilhaRetornoLugarRedeSuperiorPlayer1.pop();
			String nomeRede2 = pilhaRedesSuperioresPlayer2.pop();
			String lugar2 = pilhaRetornoLugarRedeSuperiorPlayer2.pop();
			// verifica se as redes superiores e lugares s�o coincidentes
			if( nomeRede1.equals( nomeRede2 ) && lugar1.equals( lugar2 ) )
			{
				// busca a rede superior
				redePlayer2 = RedeProperties.getSubRedeProperties( redeProperties, nomeRede1 );
				redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );	
				lugarPlayer2 = redePlayer2.getLugarProperties( lugar1 );					
				// verifica o pr�ximo estado da rede superior
				estadoLugarPlayer2 = lugarPlayer2.getEstadoLugar( redeHierarquica2.getValorFichaAtualNoLugar( lugarPlayer2.getNome() ) );			
				eProx = redePlayer2.getEstadoLugar( p1, p2, 
					estadoLugarPlayer2.getObjetoPresente(), estadoLugarPlayer2, lugarPlayer2 );
			} // fim de if
			else // redes n�o coincid�ntes, basta retornar individualmente
			{
				pilhaRedesSuperioresPlayer1.push( nomeRede1 );
				pilhaRetornoLugarRedeSuperiorPlayer1.push( lugar1 );
				pilhaRedesSuperioresPlayer2.push( nomeRede2 );
				pilhaRetornoLugarRedeSuperiorPlayer2.push( lugar2 );
				// exeuta o return do player 1 e 2 na rede superior		
				returnPlayer1();
				returnPlayer2();
				return;
			} // fim de else
		} while( eProx.isFinal() || lugarPlayer2.isPontoSaida() );
		
		// executa o return do player 1 e 2 na rede superior
		List<TransicaoProperties> transicoes = redePlayer2.getTransicoesProperties( prefixoPlayer1Player2In, lugarPlayer2, lugarPlayer2 );
		for( TransicaoProperties t : transicoes )
		{
			if( redeHierarquica2.isTransicaoHabilitada( t.getNome() ) )
			{
				redeHierarquica2.executeTransicao( t.getNome() );
				break;
			} // fim de if
		} // fim de for
			
		setPercentualXRelPlayer1( 0.35 );
		setPercentualYRelPlayer1( 0.5 );
					
		setPercentualXRelPlayer2( 0.65 );
		setPercentualYRelPlayer2( 0.5 );
	} // fim de m�todo
	
	/**
	 * Verifica se os players devem retornar simultaneamente para a rede superior.
	 * Este m�todo deve ser chamado ap�s ocorrer uma transi��o de player 1 e 2 simult�nea.
	 */
	private synchronized void checkReturnPlayer1Player2()
	{
		if( gameOver )
			return;
		
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		
		RedeHierarquica redeHierarquica1 = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		LugarProperties lugarPlayer1 = redePlayer1.getLugarPropertiesPlayer1( redeHierarquica1 );		
		EstadoLugar estadoLugarPlayer1 = lugarPlayer1.getEstadoLugar( redeHierarquica1.getValorFichaAtualNoLugar( lugarPlayer1.getNome() ) );

		RedeHierarquica redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
		LugarProperties lugarPlayer2 = redePlayer2.getLugarPropertiesPlayer2( redeHierarquica2 );		
					
		// os players est�o na mesma rede e nos mesmos lugares
		if( redePlayer1 == redePlayer2 && lugarPlayer1 == lugarPlayer2 )
		{
			// verifica se terminou para os dois
			if( estadoLugarPlayer1.isFinal() || lugarPlayer1.isPontoSaida() )
			{
				returnPlayer1Player2();
				outPlayer1Player2( redePlayer1, redeHierarquica1, lugarPlayer1 );

				playMusicGame();
				;
			} // fim de if
		} // fim de if
	} // fim de m�todo
	
	/**
	 * Verifica se os players devem retornar individualemente para a rede superior.
	 * Este m�todo deve ser chamado ap�s uma transi��o de player 1 ou 2 n�o simult�nea.
	 */
	private synchronized void checkReturnPlayer()
	{
		if( gameOver )
			return;
		
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		RedeHierarquica redeHierarquica1 = null;
		LugarProperties lugarPlayer1 = null;
		EstadoLugar estadoLugarPlayer1 = null;
		RedeHierarquica redeHierarquica2 = null;
		LugarProperties lugarPlayer2 = null;
		EstadoLugar estadoLugarPlayer2 = null;
		
		if( redePlayer1 != null )
		{
			redeHierarquica1 = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
			lugarPlayer1 = redePlayer1.getLugarPropertiesPlayer1( redeHierarquica1 );		
			estadoLugarPlayer1 = lugarPlayer1.getEstadoLugar( redeHierarquica1.getValorFichaAtualNoLugar( lugarPlayer1.getNome() ) );
		}

		if( redePlayer2 != null )
		{
			redeHierarquica2 = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
			lugarPlayer2 = redePlayer2.getLugarPropertiesPlayer2( redeHierarquica2 );		
			estadoLugarPlayer2 = lugarPlayer2.getEstadoLugar( redeHierarquica2.getValorFichaAtualNoLugar( lugarPlayer2.getNome() ) );
		} // fim de if
		
		// verifica se terminou para o player 1 
		if( ( estadoLugarPlayer1 != null && estadoLugarPlayer1.isFinal() ) || ( lugarPlayer1 != null && lugarPlayer1.isPontoSaida() ) )
		{
			returnPlayer1();
			if( !gameOver )
				outPlayer1( redePlayer1, redeHierarquica1, lugarPlayer1 );
										
			playMusicGame();
			;
		} // fim de else if
		// verifica se terminou para o player 2
		if( ( estadoLugarPlayer2 != null && estadoLugarPlayer2.isFinal() ) || ( lugarPlayer2 != null && lugarPlayer2.isPontoSaida() ) )
		{
			returnPlayer2();
			if( !gameOver )
				outPlayer2( redePlayer2, redeHierarquica2, lugarPlayer2 );

			playMusicGame();
			;
		} // fim de else if
		
	} // fim de m�todo
	/*
	private synchronized boolean isGameOverPlayer1()
	{
		LinkedList<String> pilha = new LinkedList<String>();
		LinkedList<String> pilhaLugar = new LinkedList<String>();
		pilha.addAll( pilhaRedesSuperioresPlayer1 );
		pilhaLugar.addAll( pilhaRetornoLugarRedeSuperiorPlayer1 );

		// analisa o estado do lugar atual do player
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeHierarquica rh = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		LugarProperties l = redePlayer1.getLugarPropertiesPlayer1( rh );			
		EstadoLugar e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );		
		if( !e.isFinal() && !l.isPontoSaida() )
			return false;

		Personagem p = e.getPersonagemPresente1();
		while( !pilha.isEmpty() )
		{
			String nomeRede = pilha.pop();
			String nomeLugar = pilhaLugar.pop();
			
			RedeProperties r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede );
			rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );

			l = r.getLugarProperties( nomeLugar );			
			e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
			
			EstadoLugar eProx = r.getEstadoLugar( p, e.getPersonagemPresente2(), e.getObjetoPresente(), e, l );
			if( !eProx.isFinal() && !l.isPontoSaida() )
				return false;
		} // fim de while

		return true;
	} // fim de m�todo

	private synchronized boolean isGameOverPlayer2()
	{
		LinkedList<String> pilha = new LinkedList<String>();
		LinkedList<String> pilhaLugar = new LinkedList<String>();
		pilha.addAll( pilhaRedesSuperioresPlayer2 );
		pilhaLugar.addAll( pilhaRetornoLugarRedeSuperiorPlayer2 );

		// analisa o estado do lugar atual do player
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica );
		RedeHierarquica rh = redeHierarquica.getSubRedeHierarquica( redePlayer2.getNome() );
		LugarProperties l = redePlayer2.getLugarPropertiesPlayer2( rh );			
		EstadoLugar e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );		
		if( !e.isFinal() && !l.isPontoSaida() )
			return false;
		
		Personagem p = e.getPersonagemPresente2();
		while( !pilha.isEmpty() )
		{
			String nomeRede = pilha.pop();
			String nomeLugar = pilhaLugar.pop();
			
			RedeProperties r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede );
			rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );
			
			l = r.getLugarProperties( nomeLugar );
			e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
			
			EstadoLugar eProx = r.getEstadoLugar( e.getPersonagemPresente1(), p, e.getObjetoPresente(), e, l );
			if( !eProx.isFinal() && !l.isPontoSaida() )
				return false;
		} // fim de while

		return true;
	} // fim de m�todo
	
	private synchronized boolean isGameOverPlayer1Player2()
	{
		LinkedList<String> pilha1 = new LinkedList<String>();
		LinkedList<String> pilhaLugar1 = new LinkedList<String>();
		pilha1.addAll( pilhaRedesSuperioresPlayer1 );
		pilhaLugar1.addAll( pilhaRetornoLugarRedeSuperiorPlayer1 );

		LinkedList<String> pilha2 = new LinkedList<String>();
		LinkedList<String> pilhaLugar2 = new LinkedList<String>();
		pilha1.addAll( pilhaRedesSuperioresPlayer2 );
		pilhaLugar1.addAll( pilhaRetornoLugarRedeSuperiorPlayer2 );

		// analisa o estado do lugar atual do player
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica );
		RedeHierarquica rh = redeHierarquica.getSubRedeHierarquica( redePlayer1.getNome() );
		LugarProperties l = redePlayer1.getLugarPropertiesPlayer1( rh );			
		EstadoLugar e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );		
		if( !e.isFinal() && !l.isPontoSaida() )
			return false;

		Personagem p1 = e.getPersonagemPresente1();
		Personagem p2 = e.getPersonagemPresente2();
		while( !pilha1.isEmpty() && !pilha2.isEmpty() )
		{
			String nomeRede1 = pilha1.pop();
			String nomeLugar1 = pilhaLugar1.pop();
			
			String nomeRede2 = pilha1.pop();
			String nomeLugar2 = pilhaLugar1.pop();
			
			// verifica se est�o na mesma rede superior e no mesmo lugar
			if( nomeRede1.equals( nomeRede2 ) && nomeLugar1.equals( nomeLugar2 ) )
			{
				RedeProperties r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede1 );
				rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );
	
				l = r.getLugarProperties( nomeLugar1 );			
				e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
				
				EstadoLugar eProx = r.getEstadoLugar( p1, p2, e.getObjetoPresente(), e, l );
				if( !eProx.isFinal() && !l.isPontoSaida() )
					return false;
			} // fim de if
			else // players em redes diferentes
			{
				// verifica se terminou para o player 1
				nomeRede1 = pilha1.pop();
				nomeLugar1 = pilhaLugar1.pop();
				
				RedeProperties r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede1 );
				rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );

				l = r.getLugarProperties( nomeLugar1 );			
				e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
				
				EstadoLugar eProx = r.getEstadoLugar( p1, e.getPersonagemPresente2(), e.getObjetoPresente(), e, l );
				if( !eProx.isFinal() && !l.isPontoSaida() )
					return false;

				// verifica se terminou para o player 2
				nomeRede2 = pilha2.pop();
				nomeLugar2 = pilhaLugar2.pop();
				
				r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede2 );
				rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );

				l = r.getLugarProperties( nomeLugar2 );			
				e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
				
				eProx = r.getEstadoLugar( e.getPersonagemPresente1(), p2, e.getObjetoPresente(), e, l );
				if( !eProx.isFinal() && !l.isPontoSaida() )
					return false;
			} // fim de else
		} // fim de while

		// verifica game over do player1
		while( !pilha1.isEmpty() )
		{			
			String nomeRede = pilha1.pop();
			String nomeLugar = pilhaLugar1.pop();
			
			RedeProperties r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede );
			rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );

			l = r.getLugarProperties( nomeLugar );			
			e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
			
			EstadoLugar eProx = r.getEstadoLugar( p1, e.getPersonagemPresente2(), e.getObjetoPresente(), e, l );
			if( !eProx.isFinal() && !l.isPontoSaida() )
				return false;
		} // fim de while

		// verifica game over do player2
		while( !pilha2.isEmpty() )
		{			
			String nomeRede = pilha2.pop();
			String nomeLugar = pilhaLugar2.pop();
			
			RedeProperties r = RedeProperties.getSubRedeProperties( redeProperties, nomeRede );
			rh = redeHierarquica.getSubRedeHierarquica( r.getNome() );

			l = r.getLugarProperties( nomeLugar );			
			e = l.getEstadoLugar( rh.getValorFichaAtualNoLugar( l.getNome() ) );
			
			EstadoLugar eProx = r.getEstadoLugar( e.getPersonagemPresente1(), p2, e.getObjetoPresente(), e, l );
			if( !eProx.isFinal() && !l.isPontoSaida() )
				return false;
		} // fim de while
		
		return true;
	} // fim de m�todo
	*/
	public void init()
	{
		gameOver = false;
		pilhaRedesSuperioresPlayer1.clear();
		pilhaRetornoLugarRedeSuperiorPlayer1.clear();
		pilhaRedesSuperioresPlayer2.clear();
		pilhaRetornoLugarRedeSuperiorPlayer2.clear();
		ajustarPosicaoPlayers();
		;
		playMusicGame();
	} // fim de m�todo
	
	public void destroy()
	{
		stopMusicGame();
	} // fim de m�todo
	
	public boolean isGameOver() 
	{
		return gameOver;
	}

	private void playMusicGame() 
	{
		RedeProperties redePlayer1 = RedeProperties.getRedePropertiesPlayer1Presente( getRedeProperties(), getRedeHierarquica() );
		RedeProperties redePlayer2 = RedeProperties.getRedePropertiesPlayer2Presente( getRedeProperties(), getRedeHierarquica() );

		if( redePlayer1 != null && redePlayer1.getMusicaMundo() != null )
		{
			if( musicaTocando == null )
			{
				musicaTocando = redePlayer1.getMusicaMundo();
				musicaTocando.loop();
			} // fim de if
			else if( musicaTocando != null && musicaTocando != redePlayer1.getMusicaMundo() )
			{
				musicaTocando.stop();
				musicaTocando = redePlayer1.getMusicaMundo();
				musicaTocando.loop();
			} // fim de else if
		} // fim de if
		else if( redePlayer2 != null && redePlayer2.getMusicaMundo() != null )
		{
			if( musicaTocando == null )
			{
				musicaTocando = redePlayer2.getMusicaMundo();
				musicaTocando.loop();
			} // fim de if
			else if( musicaTocando != null && musicaTocando != redePlayer2.getMusicaMundo() )
			{
				musicaTocando.stop();
				musicaTocando = redePlayer2.getMusicaMundo();
				musicaTocando.loop();
			} // fim de else if
		} // fim de else if
		else
			stopMusicGame();
	} // fim de m�todo
		
	private void stopMusicGame() 
	{
		if( musicaTocando != null )
		{
			musicaTocando.stop();
			musicaTocando = null;
		} // fim de if
	} // fim de m�todo

	private class AnimationThread extends Thread
	{
		private int player;
		private int idDisparo;
		private int direction;
		
		public AnimationThread( int player, int idDisparo, int direction )
		{
			this.player = player;
			this.idDisparo = idDisparo;
			this.direction = direction;
		} // fim de construtor
		
		public void run()
		{
			if( player == 1 )
			{
				if( direction == DIRECTION_LEFT )
				{					
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualXRelPlayer1 = 1.0;
					while( percentualXRelPlayer1 > 1 - percentualParaDisparo - 0.05 )
					{
						leftPersonagem1();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualYRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						upPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						downPersonagem1();
						sleepAndRepaint();
					} // fim de while
				} // fim de else if
				else if( direction == DIRECTION_RIGHT )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualXRelPlayer1 = 0.0;
					while( percentualXRelPlayer1 < percentualParaDisparo + 0.05 )
					{
						rightPersonagem1();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualYRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						upPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						downPersonagem1();
						sleepAndRepaint();
					} // fim de while
				} // fim de else if
				else if( direction == DIRECTION_UP )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualYRelPlayer1 = 1.0;
					while( percentualYRelPlayer1 > 1 - percentualParaDisparo - 0.05 )
					{
						upPersonagem1();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualXRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						rightPersonagem1();
						sleepAndRepaint();
					} // fim de while
				} // fim de else if
				else if( direction == DIRECTION_DOWN )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualYRelPlayer1 = 0.0;
					while( percentualYRelPlayer1 < percentualParaDisparo + 0.05 )
					{
						downPersonagem1();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualXRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						rightPersonagem1();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_RIGHT_TOP )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						rightTopPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						rightTopPersonagem1();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_LEFT_TOP )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftTopPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftTopPersonagem1();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_RIGHT_BOTTOM )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						rightBottomPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						rightBottomPersonagem1();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_LEFT_BOTTOM )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer1 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftBottomPersonagem1();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer1 <= percentualParaDisparo + 0.05 )
					{
						leftBottomPersonagem1();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if

				if( idDisparo == 3 )
					checkReturnPlayer1Player2();
				else
					checkReturnPlayer();
				player1EmTransicao = false;
				verificarDisparoPersonagem2();
			} // fim de if
			else if( player == 2 )
			{
				if( direction == DIRECTION_LEFT )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualXRelPlayer2 = 1.0;
					while( percentualXRelPlayer2 > 1 - percentualParaDisparo - 0.05 )
					{
						leftPersonagem2();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualYRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						upPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						downPersonagem2();
						sleepAndRepaint();
					} // fim de while
				} // fim de else if
				else if( direction == DIRECTION_RIGHT )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualXRelPlayer2 = 0.0;
					while( percentualXRelPlayer2 < percentualParaDisparo + 0.05 )
					{
						rightPersonagem2();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualYRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						upPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						downPersonagem2();
						sleepAndRepaint();
					} // fim de while
				} // fim de else if
				else if( direction == DIRECTION_UP )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualYRelPlayer2 = 1.0;
					while( percentualYRelPlayer2 > 1 - percentualParaDisparo - 0.05 )
					{
						upPersonagem2();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualXRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						rightPersonagem2();
						sleepAndRepaint();
					} // fim de while
				} // fim de else if
				else if( direction == DIRECTION_DOWN )
				{
					// desloca o personagem at� uma possi��o 'confort�vel'
					percentualYRelPlayer2 = 0.0;
					while( percentualYRelPlayer2 < percentualParaDisparo + 0.05 )
					{
						downPersonagem2();
						sleepAndRepaint();
					} // fim de while
					
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualXRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						rightPersonagem2();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_RIGHT_TOP )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						rightTopPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						rightTopPersonagem2();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_LEFT_TOP )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftTopPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftTopPersonagem2();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_RIGHT_BOTTOM )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						rightBottomPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						rightBottomPersonagem2();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				else if( direction == DIRECTION_LEFT_BOTTOM )
				{
					// ajusta para n�o efetuar uma transi��o n�o pretendida
					while( percentualXRelPlayer2 >= 1 - percentualParaDisparo - 0.05 )
					{
						leftBottomPersonagem2();
						sleepAndRepaint();
					} // fim de while					
					while( percentualYRelPlayer2 <= percentualParaDisparo + 0.05 )
					{
						leftBottomPersonagem2();
						sleepAndRepaint();
					} // fim de while					
				} // fim de else if
				
				if( idDisparo == 3 )
					checkReturnPlayer1Player2();
				else
					checkReturnPlayer();
				player2EmTransicao = false;
				verificarDisparoPersonagem1();
			} // fim de if
		} // fim de m�todo
			
		public void rightTopPersonagem1()
		{
			if( percentualXRelPlayer1 < 1.0 && percentualYRelPlayer1 > 0 )
			{
				percentualXRelPlayer1 += passoAndar;
				percentualYRelPlayer1 -= passoAndar;
				direcaoCorrente1 = DIRECTION_RIGHT_TOP;
				rigthTop1 = ( rigthTop1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void leftTopPersonagem1()
		{
			if( percentualXRelPlayer1 > 0 && percentualYRelPlayer1 > 0 )
			{
				percentualXRelPlayer1 -= passoAndar;
				percentualYRelPlayer1 -= passoAndar;
				direcaoCorrente1 = DIRECTION_LEFT_TOP;
				leftTop1 = ( leftTop1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo

		public void rightBottomPersonagem1()
		{
			if( percentualXRelPlayer1 < 1.0 && percentualYRelPlayer1 < 1.0 )
			{
				percentualXRelPlayer1 += passoAndar;
				percentualYRelPlayer1 += passoAndar;
				direcaoCorrente1 = DIRECTION_RIGHT_BOTTOM;
				rigthBottom1 = ( rigthBottom1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo

		public void leftBottomPersonagem1()
		{
			if( percentualXRelPlayer1 > 0 && percentualYRelPlayer1 < 1.0 )
			{
				percentualXRelPlayer1 -= passoAndar;
				percentualYRelPlayer1 += passoAndar;
				direcaoCorrente1 = DIRECTION_LEFT_BOTTOM;
				leftBottom1 = ( leftBottom1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void rightPersonagem1()
		{
			if( percentualXRelPlayer1 < 1.0 )
			{
				percentualXRelPlayer1 += passoAndar;
				direcaoCorrente1 = DIRECTION_RIGHT;
				right1 = ( right1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void leftPersonagem1()
		{
			if( percentualXRelPlayer1 > 0.0 )
			{
				percentualXRelPlayer1 -= passoAndar;
				direcaoCorrente1 = DIRECTION_LEFT;
				left1 = ( left1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void upPersonagem1()
		{
			if( percentualYRelPlayer1 > 0.0 )
			{
				percentualYRelPlayer1 -= passoAndar;
				direcaoCorrente1 = DIRECTION_UP;
				up1 = ( up1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void downPersonagem1()
		{
			if( percentualYRelPlayer1 < 1.0 )
			{
				percentualYRelPlayer1 += passoAndar;
				direcaoCorrente1 = DIRECTION_DOWN;
				down1 = ( down1 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void rightTopPersonagem2()
		{
			if( percentualXRelPlayer2 < 1.0 && percentualYRelPlayer2 > 0 )
			{
				percentualXRelPlayer2 += passoAndar;
				percentualYRelPlayer2 -= passoAndar;
				direcaoCorrente2 = DIRECTION_RIGHT_TOP;
				rigthTop2 = ( rigthTop2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void leftTopPersonagem2()
		{
			if( percentualXRelPlayer2 > 0 && percentualYRelPlayer2 > 0 )
			{
				percentualXRelPlayer2 -= passoAndar;
				percentualYRelPlayer2 -= passoAndar;
				direcaoCorrente2 = DIRECTION_LEFT_TOP;
				leftTop2 = ( leftTop2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo

		public void rightBottomPersonagem2()
		{
			if( percentualXRelPlayer2 < 1.0 && percentualYRelPlayer2 < 1.0 )
			{
				percentualXRelPlayer2 += passoAndar;
				percentualYRelPlayer2 += passoAndar;
				direcaoCorrente2 = DIRECTION_RIGHT_BOTTOM;
				rigthBottom2 = ( rigthBottom2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo

		public void leftBottomPersonagem2()
		{
			if( percentualXRelPlayer2 > 0 && percentualYRelPlayer2 < 1.0 )
			{
				percentualXRelPlayer2 -= passoAndar;
				percentualYRelPlayer2 += passoAndar;
				direcaoCorrente2 = DIRECTION_LEFT_BOTTOM;
				leftBottom2 = ( leftBottom2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void rightPersonagem2()
		{
			if( percentualXRelPlayer2 < 1.0 )
			{
				percentualXRelPlayer2 += passoAndar;
				direcaoCorrente2 = DIRECTION_RIGHT;
				right2 = ( right2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void leftPersonagem2()
		{
			if( percentualXRelPlayer2 > 0.0 )
			{
				percentualXRelPlayer2 -= passoAndar;
				direcaoCorrente2 = DIRECTION_LEFT;
				left2 = ( left2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void upPersonagem2()
		{
			if( percentualYRelPlayer2 > 0.0 )
			{
				percentualYRelPlayer2 -= passoAndar;
				direcaoCorrente2 = DIRECTION_UP;
				up2 = ( up2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		public void downPersonagem2()
		{
			if( percentualYRelPlayer2 < 1.0 )
			{
				percentualYRelPlayer2 += passoAndar;
				direcaoCorrente2 = DIRECTION_DOWN;
				down2 = ( down2 + 1 ) % 3;
			} // fim de if
		} // fim de m�todo
		
		private void sleepAndRepaint()
		{
			long current = System.currentTimeMillis(), sleepTime = 30;
			
			while( System.currentTimeMillis() - current < sleepTime )
				;
		} // fim de m�todo
	} // fim de classe
	
} // fim de classe
