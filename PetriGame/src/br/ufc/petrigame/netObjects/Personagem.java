package br.ufc.petrigame.netObjects;

import java.io.Serializable;

/**
 * @author Joel
 */
public class Personagem  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private boolean player1;
	private boolean player2;
	private String figuraDefault;
	private String figuraCima1;
	private String figuraCima2;
	private String figuraCima3;
	private String figuraBaixo1;
	private String figuraBaixo2;
	private String figuraBaixo3;
	private String figuraEsquerda1;
	private String figuraEsquerda2;
	private String figuraEsquerda3;
	private String figuraDireita1;
	private String figuraDireita2;
	private String figuraDireita3;
	private String figuraDiagSupDir1;
	private String figuraDiagSupDir2;
	private String figuraDiagSupDir3;
	private String figuraDiagSupEsq1;
	private String figuraDiagSupEsq2;
	private String figuraDiagSupEsq3;
	private String figuraDiagInfDir1;
	private String figuraDiagInfDir2;
	private String figuraDiagInfDir3;
	private String figuraDiagInfEsq1;
	private String figuraDiagInfEsq2;
	private String figuraDiagInfEsq3;
	private int numeroPersonagem;
	
	public Personagem()
	{	
	}
	
	public Personagem( String nome, String figuraDefault )
	{
		this.setNome(nome);
		this.figuraDefault = figuraDefault;
	} // fim de construtor

	public int getNumeroPersonagem() 
	{
		return numeroPersonagem;
	}

	public void setNumeroPersonagem(int numeroPersonagem) 
	{
		this.numeroPersonagem = numeroPersonagem;
	}

	public boolean isPlayer1() 
	{
		return player1;
	}

	public void setPlayer1(boolean player1) 
	{
		this.player1 = player1;
	}

	public boolean isPlayer2() 
	{
		return player2;
	}

	public void setPlayer2(boolean player2) 
	{
		this.player2 = player2;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public String getFiguraDefault() 
	{
		return figuraDefault;
	}

	public void setFiguraDefault(String figuraDefault) 
	{
		this.figuraDefault = figuraDefault;
	}

	public String getFiguraCima1() 
	{
		return figuraCima1;
	}

	public void setFiguraCima1(String figuraCima1) 
	{
		this.figuraCima1 = figuraCima1;
	}

	public String getFiguraCima2() 
	{
		return figuraCima2;
	}

	public void setFiguraCima2(String figuraCima2) 
	{
		this.figuraCima2 = figuraCima2;
	}

	public String getFiguraCima3() 
	{
		return figuraCima3;
	}

	public void setFiguraCima3(String figuraCima3) 
	{
		this.figuraCima3 = figuraCima3;
	}

	public String getFiguraBaixo1() 
	{
		return figuraBaixo1;
	}

	public void setFiguraBaixo1(String figuraBaixo1) 
	{
		this.figuraBaixo1 = figuraBaixo1;
	}

	public String getFiguraBaixo2() 
	{
		return figuraBaixo2;
	}

	public void setFiguraBaixo2(String figuraBaixo2) 
	{
		this.figuraBaixo2 = figuraBaixo2;
	}

	public String getFiguraBaixo3() 
	{
		return figuraBaixo3;
	}

	public void setFiguraBaixo3(String figuraBaixo3) 
	{
		this.figuraBaixo3 = figuraBaixo3;
	}

	public String getFiguraEsquerda1() 
	{
		return figuraEsquerda1;
	}

	public void setFiguraEsquerda1(String figuraEsquerda1) 
	{
		this.figuraEsquerda1 = figuraEsquerda1;
	}

	public String getFiguraEsquerda2() 
	{
		return figuraEsquerda2;
	}

	public void setFiguraEsquerda2(String figuraEsquerda2) 
	{
		this.figuraEsquerda2 = figuraEsquerda2;
	}

	public String getFiguraEsquerda3() 
	{
		return figuraEsquerda3;
	}

	public void setFiguraEsquerda3(String figuraEsquerda3)
	{
		this.figuraEsquerda3 = figuraEsquerda3;
	}

	public String getFiguraDireita1() 
	{
		return figuraDireita1;
	}

	public void setFiguraDireita1(String figuraDireita1) 
	{
		this.figuraDireita1 = figuraDireita1;
	}

	public String getFiguraDireita2() 
	{
		return figuraDireita2;
	}

	public void setFiguraDireita2(String figuraDireita2)
	{
		this.figuraDireita2 = figuraDireita2;
	}

	public String getFiguraDireita3() 
	{
		return figuraDireita3;
	}

	public void setFiguraDireita3(String figuraDireita3) 
	{
		this.figuraDireita3 = figuraDireita3;
	}

	public String getFiguraDiagSupDir1() {
		return figuraDiagSupDir1;
	}

	public void setFiguraDiagSupDir1(String figuraDiagSupDir1) {
		this.figuraDiagSupDir1 = figuraDiagSupDir1;
	}

	public String getFiguraDiagSupDir2() {
		return figuraDiagSupDir2;
	}

	public void setFiguraDiagSupDir2(String figuraDiagSupDir2) {
		this.figuraDiagSupDir2 = figuraDiagSupDir2;
	}

	public String getFiguraDiagSupDir3() {
		return figuraDiagSupDir3;
	}

	public void setFiguraDiagSupDir3(String figuraDiagSupDir3) {
		this.figuraDiagSupDir3 = figuraDiagSupDir3;
	}

	public String getFiguraDiagSupEsq1() {
		return figuraDiagSupEsq1;
	}

	public void setFiguraDiagSupEsq1(String figuraDiagSupEsq1) {
		this.figuraDiagSupEsq1 = figuraDiagSupEsq1;
	}

	public String getFiguraDiagSupEsq2() {
		return figuraDiagSupEsq2;
	}

	public void setFiguraDiagSupEsq2(String figuraDiagSupEsq2) {
		this.figuraDiagSupEsq2 = figuraDiagSupEsq2;
	}

	public String getFiguraDiagSupEsq3() {
		return figuraDiagSupEsq3;
	}

	public void setFiguraDiagSupEsq3(String figuraDiagSupEsq3) {
		this.figuraDiagSupEsq3 = figuraDiagSupEsq3;
	}

	public String getFiguraDiagInfDir1() {
		return figuraDiagInfDir1;
	}

	public void setFiguraDiagInfDir1(String figuraDiagInfDir1) {
		this.figuraDiagInfDir1 = figuraDiagInfDir1;
	}

	public String getFiguraDiagInfDir2() {
		return figuraDiagInfDir2;
	}

	public void setFiguraDiagInfDir2(String figuraDiagInfDir2) {
		this.figuraDiagInfDir2 = figuraDiagInfDir2;
	}

	public String getFiguraDiagInfDir3() {
		return figuraDiagInfDir3;
	}

	public void setFiguraDiagInfDir3(String figuraDiagInfDir3) {
		this.figuraDiagInfDir3 = figuraDiagInfDir3;
	}

	public String getFiguraDiagInfEsq1() {
		return figuraDiagInfEsq1;
	}

	public void setFiguraDiagInfEsq1(String figuraDiagInfEsq1) {
		this.figuraDiagInfEsq1 = figuraDiagInfEsq1;
	}

	public String getFiguraDiagInfEsq2() {
		return figuraDiagInfEsq2;
	}

	public void setFiguraDiagInfEsq2(String figuraDiagInfEsq2) {
		this.figuraDiagInfEsq2 = figuraDiagInfEsq2;
	}

	public String getFiguraDiagInfEsq3() {
		return figuraDiagInfEsq3;
	}

	public void setFiguraDiagInfEsq3(String figuraDiagInfEsq3) {
		this.figuraDiagInfEsq3 = figuraDiagInfEsq3;
	}
	
	
} // fim de classe Personagem
