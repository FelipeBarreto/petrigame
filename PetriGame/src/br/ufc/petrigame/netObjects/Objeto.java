package br.ufc.petrigame.netObjects;

import java.io.Serializable;

/**
 * @author Joel
 */
public class Objeto  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String figura;
	private int numeroObjeto;
	
	public Objeto()
	{	
	}
	
	public Objeto( String nome, String figura )
	{
		this.setNome(nome);
		this.figura = figura;
	} // fim de construtor

	public int getNumeroObjeto() 
	{
		return numeroObjeto;
	}

	public void setNumeroObjeto(int numeroPersonagem) 
	{
		this.numeroObjeto = numeroPersonagem;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public String getFigura() 
	{
		return figura;
	}

	public void setFigura(String figura) 
	{
		this.figura = figura;
	}
	
} // fim de classe Personagem
