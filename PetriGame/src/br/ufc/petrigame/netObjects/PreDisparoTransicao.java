package br.ufc.petrigame.netObjects;

import java.io.Serializable;

public class PreDisparoTransicao  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private EstadoLugar estadoLugarOrigem;
	private EstadoLugar estadoLugarDestino;
	
	public PreDisparoTransicao()
	{		
	}
	
	public PreDisparoTransicao( EstadoLugar estadoLugarOrigem,
		EstadoLugar estadoLugarDestino )
	{
		this.estadoLugarDestino = estadoLugarDestino;
		this.estadoLugarOrigem = estadoLugarOrigem;
	} // fim de construtor

	public EstadoLugar getEstadoLugarOrigem() 
	{
		return estadoLugarOrigem;
	}

	public void setEstadoLugarOrigem(EstadoLugar estadoLugarOrigem) 
	{
		this.estadoLugarOrigem = estadoLugarOrigem;
	}

	public EstadoLugar getEstadoLugarDestino() 
	{
		return estadoLugarDestino;
	}

	public void setEstadoLugarDestino(EstadoLugar estadoLugarDestino) 
	{
		this.estadoLugarDestino = estadoLugarDestino;
	}
	
}
