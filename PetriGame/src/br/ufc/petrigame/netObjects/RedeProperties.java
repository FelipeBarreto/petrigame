package br.ufc.petrigame.netObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static br.ufc.petrigame.netObjects.PlayGame.*;

/**
 * @author Joel
 *
 * Adiciona atributos de propriedades a uma rede hirarquica, possibilitando
 * uma maior abstra��o deste tipo de rede. 
 */
public class RedeProperties  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private int numeroLinhas;
	private int numeroColunas;
	private List<LugarProperties> lugaresPropertiesList = new ArrayList<LugarProperties>();
	private List<TransicaoProperties> transicoesPropertiesList = new ArrayList<TransicaoProperties>();
	private int proximoValorTransicao = 0; 
	private int proximoValorLugar = 0;  

	private String transicaoSubstituicaVerticalPath;
	private String transicaoSubstituicaHorizontalPath;
	
	private Audio musicaMundo;
		
	public RedeProperties()
	{		
	} // fim de construtor
	
	public RedeProperties( String nome, int numeroLinhas, int numeroColunas )
	{		
		this.nome = nome;
		this.numeroColunas = numeroColunas;
		this.numeroLinhas = numeroLinhas;
	} // fim de construtor
	
	public void setLugarPontoEntrada( LugarProperties lugarPontoEntrada, List<Personagem> personagens )
	{
		for( LugarProperties l : lugaresPropertiesList )
			l.setPontoEntrada( false );
		
		lugarPontoEntrada.setPontoEntrada( true );
		
		criarTransicoesIn( lugarPontoEntrada, personagens );
	} // fim de m�todo

	private void removerTransicoesIn( LugarProperties lugarPontoEntrada )
	{
		// remove transi��es de entrada anteriores
		for( int i = 0; i < transicoesPropertiesList.size(); i++ )
		{
			if( transicoesPropertiesList.get( i ).getLugarOrigemProperties() == transicoesPropertiesList.get( i ).getLugarDestinoProperties() &&
				transicoesPropertiesList.get( i ).getLugarOrigemProperties() == lugarPontoEntrada &&
				( transicoesPropertiesList.get( i ).getNome().contains( prefixoPlayer1In ) || 
				  transicoesPropertiesList.get( i ).getNome().contains( prefixoPlayer2In ) || 
				  transicoesPropertiesList.get( i ).getNome().contains( prefixoPlayer1Player2In ) ) )
				transicoesPropertiesList.remove( i-- );				
		} // fim de for
	} // fim de m�todo
	
	public void criarTransicoesIn( LugarProperties lugarPontoEntrada, List<Personagem> personagens )
	{
		removerTransicoesIn( lugarPontoEntrada );
		
		// cria novas transi��es de entrada
		for( EstadoLugar estadoOrigem : lugarPontoEntrada.getEstadosPossiveisLugar() )
		{
			// cria transi��es para player1
			if( estadoOrigem.getPersonagemPresente1() == null )
			{				
				for( Personagem p1 : personagens )
				{
					if( p1.isPlayer1() )
					{
						TransicaoProperties t = new TransicaoProperties();
						t.setNome( prefixoPlayer1In + getProximoValorTransicao() );
						t.setLugarOrigemProperties( lugarPontoEntrada );
						t.setLugarDestinoProperties( lugarPontoEntrada );
								
						PreDisparoTransicao pre = new PreDisparoTransicao( estadoOrigem, estadoOrigem );
						t.setPreDisparoTransicao( pre );
						
						EstadoLugar estadoDestino = getEstadoLugar( p1, estadoOrigem.getPersonagemPresente2(), 
							estadoOrigem.getObjetoPresente(), estadoOrigem, lugarPontoEntrada );						
						PosDisparoTransicao pos = new PosDisparoTransicao( estadoDestino, estadoDestino );
						t.setPosDisparoTransicao( pos );
								
						this.transicoesPropertiesList.add( t );
					} // fim de if
				} // fim de for
				
			} // fim de if
			// cria transi��es para player2
			if( estadoOrigem.getPersonagemPresente2() == null )
			{				
				for( Personagem p2 : personagens )
				{
					if( p2.isPlayer2() )
					{
						TransicaoProperties t = new TransicaoProperties();
						t.setNome( prefixoPlayer2In + getProximoValorTransicao() );
						t.setLugarOrigemProperties( lugarPontoEntrada );
						t.setLugarDestinoProperties( lugarPontoEntrada );
								
						PreDisparoTransicao pre = new PreDisparoTransicao( estadoOrigem, estadoOrigem );
						t.setPreDisparoTransicao( pre );
								
						EstadoLugar estadoDestino = getEstadoLugar( estadoOrigem.getPersonagemPresente1(), p2, 
							estadoOrigem.getObjetoPresente(), estadoOrigem, lugarPontoEntrada );						
						PosDisparoTransicao pos = new PosDisparoTransicao( estadoDestino, estadoDestino );
						t.setPosDisparoTransicao( pos );
								
						this.transicoesPropertiesList.add( t );
					} // fim de if
				} // fim de for
						
			} // fim de if
			// cria transi��es para player1 e player2
			if( estadoOrigem.getPersonagemPresente1() == null && estadoOrigem.getPersonagemPresente2() == null )
			{				
				for( Personagem p1 : personagens )
				{
					for( Personagem p2 : personagens )
					{
						if( p1.isPlayer1() && p2.isPlayer2() )
						{
							TransicaoProperties t = new TransicaoProperties();
							t.setNome( prefixoPlayer1Player2In + getProximoValorTransicao() );
							t.setLugarOrigemProperties( lugarPontoEntrada );
							t.setLugarDestinoProperties( lugarPontoEntrada );
												
							PreDisparoTransicao pre = new PreDisparoTransicao( estadoOrigem, estadoOrigem );
							t.setPreDisparoTransicao( pre );
												
							EstadoLugar estadoDestino = getEstadoLugar( p1, p2, 
								estadoOrigem.getObjetoPresente(), estadoOrigem, lugarPontoEntrada );						
							PosDisparoTransicao pos = new PosDisparoTransicao( estadoDestino, estadoDestino );
							t.setPosDisparoTransicao( pos );
												
							this.transicoesPropertiesList.add( t );
						} // fim de if
					} // fim de for
				} // fim de for
							
			} // fim de if
		} // fim de for
	} // fim de m�todo

	private void removerTransicoesOut( LugarProperties lugarPontoSaida )
	{
		// remove transi��es de entrada anteriores
		for( int i = 0; i < transicoesPropertiesList.size(); i++ )
		{
			if( transicoesPropertiesList.get( i ).getLugarOrigemProperties() == lugarPontoSaida &&
				transicoesPropertiesList.get( i ).getLugarDestinoProperties() == lugarPontoSaida && 
				( transicoesPropertiesList.get( i ).getNome().contains( prefixoPlayer1Out ) || 
				  transicoesPropertiesList.get( i ).getNome().contains( prefixoPlayer2Out ) || 
				  transicoesPropertiesList.get( i ).getNome().contains( prefixoPlayer1Player2Out ) ) )
				transicoesPropertiesList.remove( i-- );	
		} // fim de for
	} // fim de m�todo
	
	public void criarTransicoesOut( LugarProperties lugarPontoSaida, List<Personagem> personagens )
	{
		// remove transi��es de entrada anteriores
		removerTransicoesOut( lugarPontoSaida );
				
		// cria novas transi��es de entrada
		for( EstadoLugar estadoOrigem : lugarPontoSaida.getEstadosPossiveisLugar() )
		{
			// cria transi��es para player1
			if( estadoOrigem.getPersonagemPresente1() != null )
			{				
				TransicaoProperties t = new TransicaoProperties();
				t.setNome( prefixoPlayer1Out + getProximoValorTransicao() );
				t.setLugarOrigemProperties( lugarPontoSaida );
				t.setLugarDestinoProperties( lugarPontoSaida );
						
				PreDisparoTransicao pre = new PreDisparoTransicao( estadoOrigem, estadoOrigem );
				t.setPreDisparoTransicao( pre );
					
				EstadoLugar estadoDestino = getEstadoLugar( 
					null, estadoOrigem.getPersonagemPresente2(), estadoOrigem.getObjetoPresente(), estadoOrigem, lugarPontoSaida );						
				PosDisparoTransicao pos = new PosDisparoTransicao( estadoDestino, estadoDestino );
				t.setPosDisparoTransicao( pos );
								
				this.transicoesPropertiesList.add( t );				
			} // fim de if
			// cria transi��es para player2
			if( estadoOrigem.getPersonagemPresente2() != null )
			{				
				TransicaoProperties t = new TransicaoProperties();
				t.setNome( prefixoPlayer2Out + getProximoValorTransicao() );
				t.setLugarOrigemProperties( lugarPontoSaida );
				t.setLugarDestinoProperties( lugarPontoSaida );
						
				PreDisparoTransicao pre = new PreDisparoTransicao( estadoOrigem, estadoOrigem );
				t.setPreDisparoTransicao( pre );
					
				EstadoLugar estadoDestino = getEstadoLugar( 
					estadoOrigem.getPersonagemPresente1(), null, estadoOrigem.getObjetoPresente(), estadoOrigem, lugarPontoSaida );						
				PosDisparoTransicao pos = new PosDisparoTransicao( estadoDestino, estadoDestino );
				t.setPosDisparoTransicao( pos );
								
				this.transicoesPropertiesList.add( t );								
			} // fim de if
			// cria transi��es para player1 e player2
			if( estadoOrigem.getPersonagemPresente1() != null && estadoOrigem.getPersonagemPresente2() != null )
			{				
				TransicaoProperties t = new TransicaoProperties();
				t.setNome( prefixoPlayer1Player2Out + getProximoValorTransicao() );
				t.setLugarOrigemProperties( lugarPontoSaida );
				t.setLugarDestinoProperties( lugarPontoSaida );
						
				PreDisparoTransicao pre = new PreDisparoTransicao( estadoOrigem, estadoOrigem );
				t.setPreDisparoTransicao( pre );
					
				EstadoLugar estadoDestino = getEstadoLugar( 
					null, null, estadoOrigem.getObjetoPresente(), estadoOrigem, lugarPontoSaida );						
				PosDisparoTransicao pos = new PosDisparoTransicao( estadoDestino, estadoDestino );
				t.setPosDisparoTransicao( pos );
								
				this.transicoesPropertiesList.add( t );	
			} // fim de if
		} // fim de for
	} // fim de m�todo
	
	public int getProximoValorLugar() 
	{
		return proximoValorLugar++;
	}

	public int getProximoValorTransicao() 
	{
		return proximoValorTransicao++;
	}

	public LugarProperties getLugarPropertiesPontoEntrada()
	{
		for( LugarProperties l : lugaresPropertiesList )
			if( l.isPontoEntrada() )
				return l;
		
		return null;
	} // fim de m�todo

	public LugarProperties getLugarPropertiesPontoSaida()
	{
		for( LugarProperties l : lugaresPropertiesList )
			if( l.isPontoSaida() )
				return l;
		
		return null;
	} // fim de m�todo
	
	public boolean existsTransicaoNivel( LugarProperties lugar1, LugarProperties lugar2 )
	{
		if( lugar1 == null || lugar2 == null )
			return false;
		
		for( TransicaoProperties t : this.transicoesPropertiesList )
		{
			if( t.getRedeNivelInferior() != null &&
				( ( t.getLugarOrigemProperties() == lugar1 && t.getLugarDestinoProperties() == lugar2 ) ||
				  ( t.getLugarOrigemProperties() == lugar2 && t.getLugarDestinoProperties() == lugar1 ) ) )
				return true;
		} // fim de if
		
		return false;
	} // fim de m�todo
	
	/**
	 * @param personagemPlayer1 o personagem 1 presente no estado a ser buscado
	 * @param personagemPlayer2 o personagem 2 presente no estado a ser buscado
	 * @param objeto o objeto presente no estado a ser buscado
	 * @param estadoBase o estado base para a busca, ou seja, a imagem deste estado � tomada como crit�rio de busca
	 * @param lugarProperties o lugar onde ser� feita a busca pelo estado.
	 * @return o estado do lugar passado no argumento que satisfaz as condi��es de buscar, ou seja, possui
	 * 		os personagens passados nos argumentos, a mesma figura que o estado base
	 */
	public EstadoLugar getEstadoLugar( Personagem personagemPlayer1, Personagem personagemPlayer2, Objeto objeto, EstadoLugar estadoBase, LugarProperties lugarProperties )
	{
		for( EstadoLugar e : lugarProperties.getEstadosPossiveisLugar() )
		{
			if( e.getObjetoPresente() == objeto && e.getPersonagemPresente1() == personagemPlayer1 && e.getPersonagemPresente2() == personagemPlayer2 &&
				e.getFigura().equals( estadoBase.getFigura() ) )
				return e;
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	/**
	 * @param lugarProperties lugar a ser utilizado como par�metro de busca.
	 * @return retorna a lista de transi��es que possuem origem no lugar definido no par�metro.
	 */
	public List<TransicaoProperties> getTransicoesPropertiesOrigem( LugarProperties lugarProperties )
	{
		List<TransicaoProperties> lista = new ArrayList<TransicaoProperties>();
		
		for( TransicaoProperties t : this.transicoesPropertiesList )
		{
			if( t.getLugarOrigemProperties() == lugarProperties )
				lista.add( t );
		} // fim de for
		
		return lista;
	} // fim de m�todo

	/**
	 * @param lugarProperties lugar a ser utilizado como par�metro de busca.
	 * @return retorna a lista de transi��es que possuem destino no lugar definido no par�metro.
	 */
	public List<TransicaoProperties> getTransicoesPropertiesDestino( LugarProperties lugarProperties )
	{
		List<TransicaoProperties> lista = new ArrayList<TransicaoProperties>();
		
		for( TransicaoProperties t : this.transicoesPropertiesList )
		{
			if( t.getLugarDestinoProperties() == lugarProperties )
				lista.add( t );
		} // fim de for
		
		return lista;
	} // fim de m�todo
	
	public List<TransicaoProperties> getTransicoesProperties( String prefix, LugarProperties lugarOrigem, LugarProperties lugarDestino )
	{
		List<TransicaoProperties> lista = new ArrayList<TransicaoProperties>();

		for( TransicaoProperties t : getTransicoesProperties( lugarOrigem, lugarDestino ) )
		{ 
			if( t.getNome().contains( prefix ) )
				lista.add( t );
		} // fim de for
		
		return lista;
	} // fim de m�todo
	
	/**
	 * @param lugarOrigem o lugar de origem da transi��o
	 * @param lugarDestino o lugar de destino da transi��o
	 * @return retorna a lista de transi��es associadas ao lugares de origem e destino
	 */
	public List<TransicaoProperties> getTransicoesProperties( LugarProperties lugarOrigem, LugarProperties lugarDestino )
	{
		List<TransicaoProperties> lista = new ArrayList<TransicaoProperties>();
		
		for( TransicaoProperties t : this.transicoesPropertiesList )
		{
			if( t.getLugarOrigemProperties() == lugarOrigem &&
				t.getLugarDestinoProperties() == lugarDestino )
				lista.add( t );
		} // fim de for
		
		return lista;
	} // fim de m�todo

	/**
	 * @param lugarOrigem o lugar de origem da transi��o
	 * @param lugarDestino o lugar de destino da transi��o
	 */
	public void removerTransicoesProperties( LugarProperties lugarOrigem, LugarProperties lugarDestino )
	{
		for( int i = 0; i < this.transicoesPropertiesList.size(); )
		{
			if( transicoesPropertiesList.get( i ).getLugarOrigemProperties() == lugarOrigem &&
				transicoesPropertiesList.get( i ).getLugarDestinoProperties() == lugarDestino )
				transicoesPropertiesList.remove( i );
			else
				i++;
		} // fim de for
	} // fim de m�todo
	
	public LugarProperties getLugarProperties( String nome )
	{
		for( int i = 0; i < getLugaresPropertiesList().size(); i++ )
		{
			if( getLugaresPropertiesList().get( i ).getNome().equals( nome ) )
				return getLugaresPropertiesList().get( i );
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	/**
	 * @param indexLinha index da linha associada ao lugar clicado
	 * @param indexColuna index da coluna associada ao lugar clicado
	 */
	public LugarProperties getLugarProperties( int indexLinha, int indexColuna )
	{
		for( int i = 0; i < getLugaresPropertiesList().size(); i++ )
		{
			if( getLugaresPropertiesList().get( i ).getPosLinha() == indexLinha &&
				getLugaresPropertiesList().get( i ).getPosColuna() == indexColuna )
				return getLugaresPropertiesList().get( i );
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	/**
	 * @param redeProperties a rede properties considerada 
	 * @param redeHierarquica a rede hier�rquica correspondente a rede properties
	 * @return returna a subrede onde o player 1 est� presente.
	 */
	public static RedeProperties getRedePropertiesPlayer1Presente( RedeProperties redeProperties, RedeHierarquica redeHierarquica )
	{	
		return getRedePropertiesPlayer1Presente( redeProperties, redeHierarquica, new ArrayList<RedeProperties>() );
	}
	
	private static RedeProperties getRedePropertiesPlayer1Presente( RedeProperties redeProperties, RedeHierarquica redeHierarquica, List<RedeProperties> listRedes )
	{				
		// verifica se o personagem est� nesta rede
		listRedes.add( redeProperties );
		for( LugarProperties lugarProperties : redeProperties.getLugaresPropertiesList() )
		{
			// recupera o valor atual da ficha do lugar
			int valorFicha = redeHierarquica.getValorFichaAtualNoLugar( lugarProperties.getNome() );
			
			for( EstadoLugar estadoLugar : lugarProperties.getEstadosPossiveisLugar() )
			{
				if( estadoLugar.getValor() == valorFicha && estadoLugar.getPersonagemPresente1() != null )
					return redeProperties;
			} // fim de for interno
		} // fim de for

		// procura nas subredes
		for( RedeProperties r : redeProperties.getRedesInferiores() )
		{
			if( !listRedes.contains( r ) )
			{
				// busca a rede hier�rquica 
				RedeProperties aux = getRedePropertiesPlayer1Presente( r, redeHierarquica.getSubRedeHierarquica( r.getNome() ), listRedes );
				if( aux != null )
					return aux;
			} // fim de if
		} // fim de for
		
		return null;
	} // fim de m�todo
		
	/**
	 * @param redeProperties a subrede a ser considerada na busca
	 * @param nomeRede o nome da rede a ser procurada
	 * @return returna a subrede que possui o identificador passado
	 */
	public static RedeProperties getSubRedeProperties( RedeProperties redeProperties, String nomeRede )
	{	
		return getSubRedeProperties( redeProperties, nomeRede, new ArrayList<RedeProperties>() );
	}
	
	private static RedeProperties getSubRedeProperties( RedeProperties redeProperties, String nomeRede, List<RedeProperties> listRedes )
	{		
		// verifica � esta rede
		listRedes.add( redeProperties );
		if( redeProperties.getNome().equals( nomeRede ) )
			return redeProperties;

		// procura nas subredes
		for( RedeProperties r : redeProperties.getRedesInferiores() )
		{
			if( !listRedes.contains( r ) )
			{
				RedeProperties aux = getSubRedeProperties( r, nomeRede, listRedes );
				if( aux != null )
					return aux;
			} // fim de if
		} // fim de for
		
		return null;
	} // fim de m�todo
	
	/**
	 * @param redeProperties a rede properties considerada 
	 * @param redeHierarquica a rede hier�rquica correspondente a rede properties
	 * @return returna a subrede onde o player 1 est� presente.
	 */
	public static RedeProperties getRedePropertiesPlayer2Presente( RedeProperties redeProperties, RedeHierarquica redeHierarquica )
	{	
		return getRedePropertiesPlayer2Presente( redeProperties, redeHierarquica, new ArrayList<RedeProperties>() );
	}
	
	private static RedeProperties getRedePropertiesPlayer2Presente( RedeProperties redeProperties, RedeHierarquica redeHierarquica, List<RedeProperties> listRedes )
	{				
		// verifica se o personagem est� nesta rede
		listRedes.add( redeProperties );
		for( LugarProperties lugarProperties : redeProperties.getLugaresPropertiesList() )
		{
			// recupera o valor atual da ficha do lugar
			int valorFicha = redeHierarquica.getValorFichaAtualNoLugar( lugarProperties.getNome() );
			
			for( EstadoLugar estadoLugar : lugarProperties.getEstadosPossiveisLugar() )
			{
				if( estadoLugar.getValor() == valorFicha && estadoLugar.getPersonagemPresente2() != null )
					return redeProperties;
			} // fim de for interno
		} // fim de for

		// procura nas subredes
		for( RedeProperties r : redeProperties.getRedesInferiores() )
		{
			if( !listRedes.contains( r ) )
			{
				RedeProperties aux = getRedePropertiesPlayer2Presente( r, redeHierarquica.getSubRedeHierarquica( r.getNome() ), listRedes );
				if( aux != null )
					return aux;
			} // fim de if
		} // fim de for
		
		return null;
	} // fim de m�todo

	/**
	 * @param redeHierarquica a rede hierarquica correspondente a esta rede
	 * @return o lugar onde o player 1 est� presente
	 */
	public LugarProperties getLugarPropertiesPlayer1( RedeHierarquica redeHierarquica )
	{		
		RedeProperties redeProperties = this;//getRedePropertiesPlayer1Presente( this, redeHierarquica );
		
		for( int i = 0; i < redeProperties.getNumeroLinhas(); i++ )
		{
			for( int j = 0; j < redeProperties.getNumeroColunas(); j++ )
			{	
				LugarProperties lugarProperties = redeProperties.getLugarProperties( i, j );	
				
				if( lugarProperties != null )
				{
					int valorFicha = redeHierarquica.getValorFichaAtualNoLugar( lugarProperties.getNome() );					
					EstadoLugar estadoLugar = lugarProperties.getEstadoLugar( valorFicha );
					
					// desenha o personagem
					if( estadoLugar != null && estadoLugar.getPersonagemPresente1() != null )
						return lugarProperties;
				} // fim de if
			} // fim de for
		} // fim de for
		
		return null;
	} // fim de m�todo

	/**
	 * @param redeHierarquica a rede hierarquica correspondente a esta rede
	 * @return o lugar onde o player 2 est� presente
	 */
	public LugarProperties getLugarPropertiesPlayer2( RedeHierarquica redeHierarquica )
	{
		RedeProperties redeProperties = this;//getRedePropertiesPlayer2Presente( this, redeHierarquica );
		
		for( int i = 0; i < redeProperties.getNumeroLinhas(); i++ )
		{
			for( int j = 0; j < redeProperties.getNumeroColunas(); j++ )
			{	
				LugarProperties lugarProperties = redeProperties.getLugarProperties( i, j );	
				
				if( lugarProperties != null )
				{
					int valorFicha = redeHierarquica.getValorFichaAtualNoLugar( lugarProperties.getNome() );					
					EstadoLugar estadoLugar = lugarProperties.getEstadoLugar( valorFicha );
					
					// desenha o personagem
					if( estadoLugar != null && estadoLugar.getPersonagemPresente2() != null )
						return lugarProperties;
				} // fim de if
			} // fim de for
		} // fim de for
		
		return null;
	} // fim de m�todo
		
	public void removerTodasTransicoesOrigemDestino( LugarProperties lugarOrigem, LugarProperties lugarDestino )
	{
		for( int i = 0; i < transicoesPropertiesList.size(); i++ )
		{
			if( transicoesPropertiesList.get( i ).getLugarOrigemProperties() == lugarOrigem &&
				transicoesPropertiesList.get( i ).getLugarDestinoProperties() == lugarDestino )
				transicoesPropertiesList.remove( i-- );
		} // fim de for
	} // fim de m�todo
	
	public void removerTodasTransicoesOrigemDestino( String prefixo, LugarProperties lugarOrigem, LugarProperties lugarDestino )
	{
		for( int i = 0; i < transicoesPropertiesList.size(); i++ )
		{
			if( transicoesPropertiesList.get( i ).getNome().contains( prefixo ) &&
				transicoesPropertiesList.get( i ).getLugarOrigemProperties() == lugarOrigem &&
				transicoesPropertiesList.get( i ).getLugarDestinoProperties() == lugarDestino )
				transicoesPropertiesList.remove( i-- );
		} // fim de for
	} // fim de m�todo
	
	public Audio getMusicaMundo() 
	{
		return musicaMundo;
	}

	public void setMusicaMundo(Audio musicaMundo) 
	{
		this.musicaMundo = musicaMundo;
	}

	public String getTransicaoSubstituicaVerticalPath() {
		return transicaoSubstituicaVerticalPath;
	}

	public void setTransicaoSubstituicaVerticalPath(
			String transicaoSubstituicaVerticalPath) {
		this.transicaoSubstituicaVerticalPath = transicaoSubstituicaVerticalPath;
	}

	public String getTransicaoSubstituicaHorizontalPath() {
		return transicaoSubstituicaHorizontalPath;
	}

	public void setTransicaoSubstituicaHorizontalPath(
			String transicaoSubstituicaHorizontalPath) {
		this.transicaoSubstituicaHorizontalPath = transicaoSubstituicaHorizontalPath;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}
	
	public List<RedeProperties> getRedesInferiores()
	{
		List<RedeProperties> lista = new ArrayList<RedeProperties>();
		
		for( TransicaoProperties t : transicoesPropertiesList )
		{
			if( t.getRedeNivelInferior() != null )
				lista.add( t.getRedeNivelInferior() );
		} // fim de for
		
		return lista;
	} // fim de m�todo

	public int getNumeroLinhas() 
	{
		return numeroLinhas;
	}

	public void setNumeroLinhas(int numeroLinhas) 
	{
		this.numeroLinhas = numeroLinhas;
	}

	public int getNumeroColunas() 
	{
		return numeroColunas;
	}

	public void setNumeroColunas(int numeroColunas) 
	{
		this.numeroColunas = numeroColunas;
	}

	public List<LugarProperties> getLugaresPropertiesList() 
	{
		return lugaresPropertiesList;
	}

	public void setLugaresPropertiesList(List<LugarProperties> lugaresPropertiesList) 
	{
		this.lugaresPropertiesList = lugaresPropertiesList;
	}

	public List<TransicaoProperties> getTransicoesPropertiesList() 
	{
		return transicoesPropertiesList;
	}

	public void setTransicoesPropertiesList(List<TransicaoProperties> transicoesPropertiesList) 
	{
		this.transicoesPropertiesList = transicoesPropertiesList;
	}
} // fim de classe RedeProperties
