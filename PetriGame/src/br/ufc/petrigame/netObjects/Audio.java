package br.ufc.petrigame.netObjects;

import java.io.Serializable;

public class Audio implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String audioPath;
	private transient PlayMp3Interface playMp3Interface;
	
	public Audio()
	{		
	} // fim de construtor

	public Audio( String audioPath )
	{
		this(audioPath, null);
	} // fim de construtor

	public Audio( String audioPath, PlayMp3Interface playMp3Interface )
	{
		this.setAudioPath(audioPath);
		this.playMp3Interface = playMp3Interface;
	} // fim de construtor
	
	public PlayMp3Interface getPlayMp3Interface() 
	{
		return playMp3Interface;
	}

	public void setPlayMp3Interface(PlayMp3Interface playMp3Interface) 
	{
		this.playMp3Interface = playMp3Interface;
	}

	public void loop()
	{
		if( playMp3Interface != null )
			playMp3Interface.loop();
	} // fim de m�todo

	public void stop()
	{

		if( playMp3Interface != null )
			playMp3Interface.stop();
	} // fim de m�todo

	public void play()
	{
		if( playMp3Interface != null )
			playMp3Interface.play();
	} // fim de m�todo
	
	public String getAudioPath() 
	{
		return audioPath;
	}

	public void setAudioPath(String audioPath) 
	{
		this.audioPath = audioPath;
	}
	
} // fim de classe
