package br.ufc.petrigame.netObjects;

import java.io.Serializable;

/**
 * @author Joel
 */
public class EstadoLugar implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private int valor;
	private String figura;
	private String nome;
	private Personagem personagemPresente1;
	private Personagem personagemPresente2;
	private Objeto objetoPresente;
	private boolean ehInicial;
	private boolean ehFinal;
	
	public EstadoLugar()
	{		
	}
	
	public EstadoLugar( int valor, String figura, String nome, Personagem personagemPresente1,
		Personagem personagemPresente2 )
	{
		this.valor = valor;
		this.figura = figura;
		this.nome = nome;
		this.personagemPresente1 = personagemPresente1;
		this.personagemPresente2 = personagemPresente2;
	} 	

	public boolean isInicial() 
	{
		return ehInicial;
	}

	public void setInicial(boolean inicial) 
	{
		this.ehInicial = inicial;
	}

	public boolean isFinal() 
	{
		return ehFinal;
	}

	public void setFinal(boolean ehFinal) 
	{
		this.ehFinal = ehFinal;
	}
	
	public int getValor() 
	{
		return valor;
	}

	public void setValor(int valor) 
	{
		this.valor = valor;
	}

	public String getFigura() 
	{
		return figura;
	}

	public void setFigura(String figura) 
	{
		this.figura = figura;
	}

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public Objeto getObjetoPresente() 
	{
		return objetoPresente;
	}

	public void setObjetoPresente(Objeto objetoPresente) 
	{
		this.objetoPresente = objetoPresente;
	}

	public Personagem getPersonagemPresente1() 
	{
		return personagemPresente1;
	}

	public void setPersonagemPresente1(Personagem personagemPresente1) 
	{
		this.personagemPresente1 = personagemPresente1;
	}

	public Personagem getPersonagemPresente2() 
	{
		return personagemPresente2;
	}

	public void setPersonagemPresente2(Personagem personagemPresente2) 
	{
		this.personagemPresente2 = personagemPresente2;
	}

} // fim de classe EstadoLugar
