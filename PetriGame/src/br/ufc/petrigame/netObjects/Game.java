package br.ufc.petrigame.netObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Joel 
 */
public class Game  implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private RedeProperties redeProperties; // raiz
	private List<RedeProperties> redesPropertiesList; // todas as redes 
	private List<Personagem> personagens = new ArrayList<Personagem>();
	private List<Objeto> objetos = new ArrayList<Objeto>();;
	private int proximoNumeroPersonagem1 = 1;  
	private int proximoNumeroPersonagem2 = 1; 
	private int proximoNumeroObjeto = 1; 
	
	public Game( String nome, RedeProperties redeProperties )
	{		
		this.nome = nome;
		this.redeProperties = redeProperties;
		this.redesPropertiesList = new ArrayList<RedeProperties>();
	} // fim de construtor

	public int getProximoNumeroObjeto() 
	{
		return proximoNumeroObjeto++;
	}
	
	public int getProximoNumeroPersonagem1() 
	{
		return proximoNumeroPersonagem1++;
	}

	public int getProximoNumeroPersonagem2() 
	{
		return proximoNumeroPersonagem2++;
	}
	
	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public RedeProperties getRedeProperties() 
	{
		return redeProperties;
	}

	public void setRedeProperties(RedeProperties redeProperties) 
	{
		this.redeProperties = redeProperties;
	}

	public List<Personagem> getPersonagens() 
	{
		return personagens;
	}

	public void setPersonagens(List<Personagem> personagens) 
	{
		this.personagens = personagens;
	}
	
	public List<Objeto> getObjetos() 
	{
		return objetos;
	}

	public void setObjetos(List<Objeto> objetos) 
	{
		this.objetos = objetos;
	}

	public List<RedeProperties> getRedesPropertiesList() 
	{
		return redesPropertiesList;
	}

	public void setRedesPropertiesList(List<RedeProperties> redesPropertiesList) 
	{
		this.redesPropertiesList = redesPropertiesList;
	}
	
} // fim de classe Game
