package br.ufc.petrigame.netObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author Joel
 *
 * Abstrai uma rede de petri colorida hierarquica utilizando a representa��o matricial, onde
 * as principais restri��es das redes representadas giram em torno do tipo de fichas utilizadas,
 * fichas inteiras, e da quantidade de fichas presentes nos lugares, uma �nica ficha. 
 * Tal classe permite que a din�mica da rede possa ser aplicada a partir de um conjunto de m�todos utilit�rios.
 */
public class RedeHierarquica 
{
	private String nome;
	private int[][] matrizPre; // define os arcos que divergem dos lugares
	private int[][] matrizPost; // define os arcos que convergem para os lugares
	private String[] colunasIdTransicao; 
	private String[] linhasIdLugares;
	private int[] marcacaoInicial;
	private int[] marcacaoAtual;
	private Map<String, RedeHierarquica> relacaoHierarquicaMap = new HashMap<String, RedeHierarquica>(); // mapeia o id da transi��o com a rede do pr�ximo n�vel
	
	public RedeHierarquica( String nome, String[] colunasIdTransicao, String[] linhasIdLugares )
	{
		this.nome = nome;
		this.colunasIdTransicao = colunasIdTransicao;
		this.linhasIdLugares = linhasIdLugares;
		this.matrizPre = new int[ linhasIdLugares.length ][ colunasIdTransicao.length ];
		this.matrizPost = new int[ linhasIdLugares.length ][ colunasIdTransicao.length ];
		this.marcacaoInicial = new int[ linhasIdLugares.length ];
		this.marcacaoAtual = new int[ linhasIdLugares.length ];
		
		// inicializa matriz pre e post com arcos nulos nos lugares
		for( int i = 0; i < this.matrizPre.length; i++ )
		{
			for( int j = 0; j < this.matrizPre[ i ].length; j++ )
			{
				this.matrizPre[ i ][ j ] = -1;
				this.matrizPost[ i ][ j ] = -1;
			} // fim de for interno
		} // fim de for externo
	} // fim de m�todo

	/**
	 * Define a marca��o corrente com a configura��o inicial.
	 */
	public void reiniciar()
	{
		for( int i = 0; i < marcacaoInicial.length; i++ )
			marcacaoAtual[ i ] = marcacaoInicial[ i ];
	} // fim de m�todo
	
	public List<String> getTransicoesHabilitadas()
	{
		List<String> habilitadas = new ArrayList<String>();
		
		for( int j = 0, i; j < matrizPre[ 0 ].length; j++ )
		{
			boolean habilitada = true;
			for( i = 0; i < matrizPre.length; i++ )
			{
				if( matrizPre[ i ][ j ] != -1 && matrizPre[ i ][ j ] != marcacaoAtual[ i ] )
				{
					habilitada = false;
					break;
				} // fim de if
			} // fim de for interno
			
			if( habilitada )
				habilitadas.add( linhasIdLugares[ i ] );
			
		} // fim de for externo
		
		return habilitadas;
	} // fim de m�todo
	
	public List<String> getTransicoesHabilitadas( String prefixo, String lugarOrigem, String lugarDestino )
	{
		List<String> habilitadas = new ArrayList<String>();
		
		for( int j = 0, i; j < matrizPre[ 0 ].length; j++ )
		{
			boolean habilitada = true;
			
			for( i = 0; i < matrizPre.length; i++ )
			{
				if( matrizPre[ i ][ j ] != -1 && matrizPre[ i ][ j ] != marcacaoAtual[ i ] )
				{
					habilitada = false;
					break;
				} // fim de if
			} // fim de for interno
			
			// verifica se os lugares desejados s�o origem e destino da transi��o
			if( habilitada && colunasIdTransicao[ j ].contains( prefixo ) )
			{
				int indexOrigem = getIndexLugar( lugarOrigem );
				int indexDestino = getIndexLugar( lugarDestino );
				
				if( indexOrigem != -1 && indexDestino != -1 )
				{
					// verifica se a matriz pr� possui arcos entre os lugares desejados
					if( matrizPre[ indexOrigem ][ j ] != -1 && matrizPre[ indexDestino ][ j ] != -1 )
						habilitadas.add( colunasIdTransicao[ j ] );
				} // fim de if
			} // fim de if
			
		} // fim de for externo
		
		return habilitadas;
	} // fim de m�todo

	public boolean isTransicaoHabilitada( String transicao )
	{
		int index = getIndexTransicao( transicao );
		
		for( int i = 0; i < matrizPre.length; i++ )
		{
			if( matrizPre[ i ][ index ] != -1 && matrizPre[ i ][ index ] != marcacaoAtual[ i ] )
				return false;
		} // fim de for interno
		
		return true;
	} // fim de m�todo
	
	public boolean isTransicaoHabilitada( String prefixo, String lugarOrigem, String lugarDestino )
	{
		for( int j = 0, i; j < matrizPre[ 0 ].length; j++ )
		{
			boolean habilitada = true;
			
			for( i = 0; i < matrizPre.length; i++ )
			{
				if( matrizPre[ i ][ j ] != -1 && matrizPre[ i ][ j ] != marcacaoAtual[ i ] )
				{
					habilitada = false;
					break;
				} // fim de if
			} // fim de for interno
			
			// verifica se os lugares desejados s�o origem e destino da transi��o
			if( habilitada && colunasIdTransicao[ j ].contains( prefixo ) )
			{
				int indexOrigem = getIndexLugar( lugarOrigem );
				int indexDestino = getIndexLugar( lugarDestino );
				
				if( indexOrigem != -1 && indexDestino != -1 )
				{
					// verifica se a matriz pr� possui arcos entre os lugares desejados
					if( matrizPre[ indexOrigem ][ j ] != -1 && matrizPre[ indexDestino ][ j ] != -1 )
						return true;
				} // fim de if
			} // fim de if
			
		} // fim de for externo
		
		return false;
	} // fim de m�todo
	
	/**
	 * @param transicao a transi��o a ser executada
	 */
	public void executeTransicao( String transicao )
	{
		int index = getIndexTransicao( transicao );
		
		// executa a transi��o
		for( int k = 0; k < matrizPost.length; k++ )
		{
			if( matrizPost[ k ][ index ] != -1 )
				marcacaoAtual[ k ] = matrizPost[ k ][ index ];
		} // fim de for
		
		/*
		// verifica se � transi��o de n�vel
		Iterator<String> it = relacaoHierarquicaMap.keySet().iterator();
		while( it.hasNext() )
		{
			String t = it.next();
			if( t.equals( transicao ) )
				return relacaoHierarquicaMap.get( t ).getNome();
		} // fim de while
		
		return null;*/
	} // fim de m�todo
	
	/*
	public void executeTransicao( String prefixo, String lugarOrigem, String lugarDestino )
	{		
		for( int j = 0, i; j < matrizPre[ 0 ].length; j++ )
		{
			boolean habilitada = true;
			
			for( i = 0; i < matrizPre.length; i++ )
			{
				if( matrizPre[ i ][ j ] != -1 && matrizPre[ i ][ j ] != marcacaoAtual[ i ] )
				{
					habilitada = false;
					break;
				} // fim de if
			} // fim de for interno

			// verifica se os lugares desejados s�o origem e destino da transi��o
			if( habilitada && colunasIdTransicao[ j ].contains( prefixo ) )
			{
				int indexOrigem = getIndexLugar( lugarOrigem );
				int indexDestino = getIndexLugar( lugarDestino );
				
				if( indexOrigem != -1 && indexDestino != -1 )
				{
					// verifica se a matriz pr� possui arcos entre os lugares desejados
					if( matrizPre[ indexOrigem ][ j ] != -1 && matrizPre[ indexDestino ][ j ] != -1 )
					{
						// executa a transi��o
						for( int k = 0; k < matrizPost.length; k++ )
						{
							if( matrizPost[ k ][ j ] != -1 )
								marcacaoAtual[ k ] = matrizPost[ k ][ j ];
						} // fim de for
					} // fim de 
				} // fim de if
			} // fim de if
			
		} // fim de for externo
		
	} // fim de m�todo
	*/
	public int getIndexTransicao( String idTransicao )
	{
		for( int i = 0; i < colunasIdTransicao.length; i++ )
		{
			if( colunasIdTransicao[ i ].equals( idTransicao ) )
				return i;
		} // fim de for
		
		return -1;
	} // fim de m�todo
	
	public int getIndexLugar( String idLugar )
	{
		for( int i = 0; i < linhasIdLugares.length; i++ )
		{
			if( linhasIdLugares[ i ].equals( idLugar ) )
				return i;
		} // fim de for
		
		return -1;
	} // fim de m�todo
	
	public void dispararTransicao( String idTransicao )
	{
		for( int j = 0, i; j < colunasIdTransicao.length; j++ )
		{
			if( colunasIdTransicao[ j ].equals( idTransicao ) )
			{
				for( i = 0; i < matrizPost.length; i++ )
				{
					if( matrizPost[ i ][ j ] != -1 )
						marcacaoAtual[ j ] = matrizPost[ i ][ j ];
				} // fim de for interno
				break;
			} // fim de if			
		} // fim de for externo
	} // fim de m�todo
	
	public void addRelacaoHierarquica( String idTransicao, RedeHierarquica rede )
	{
		this.relacaoHierarquicaMap.put( idTransicao, rede );
	} // fim de m�todo
	
	public RedeHierarquica getRelacaoHierarquica( String idTransicao )
	{
		return this.relacaoHierarquicaMap.get( idTransicao );
	} // fim de m�todo
	
	public int[][] getMatrizPre() 
	{
		return matrizPre;
	}

	public int[][] getMatrizPost() 
	{
		return matrizPost;
	}
	
	public int getValueMatrizPre( String idTransicao, String idLugar )
	{
		int i, j;
		
		// busca os indices
		for( j = 0; j < colunasIdTransicao.length; j++ )
			if( colunasIdTransicao[ j ].equals( idTransicao ) )
				break;			
		for( i = 0; i < linhasIdLugares.length; i++ )
			if( linhasIdLugares[ i ].equals( idLugar ) )
				break;
		
		if( j >= colunasIdTransicao.length || i >= linhasIdLugares.length )
			return -1;
		return matrizPre[ i ][ j ];
	} // fim de m�todo
	
	public void setValueMatrizPre( int value, String idTransicao, String idLugar )
	{
		int i, j;
		
		// busca os indices
		for( j = 0; j < colunasIdTransicao.length; j++ )
			if( colunasIdTransicao[ j ].equals( idTransicao ) )
				break;			
		for( i = 0; i < linhasIdLugares.length; i++ )
			if( linhasIdLugares[ i ].equals( idLugar ) )
				break;
		
		if( j < colunasIdTransicao.length && i < linhasIdLugares.length )
			matrizPre[ i ][ j ] = value;			
	} // fim de m�todo

	public int getValueMatrizPost( String idTransicao, String idLugar )
	{
		int i, j;
		
		// busca os indices
		for( j = 0; j < colunasIdTransicao.length; j++ )
			if( colunasIdTransicao[ j ].equals( idTransicao ) )
				break;			
		for( i = 0; i < linhasIdLugares.length; i++ )
			if( linhasIdLugares[ i ].equals( idLugar ) )
				break;
		
		if( j >= colunasIdTransicao.length || i >= linhasIdLugares.length )
			return -1;
		return matrizPost[ i ][ j ];
	} // fim de m�todo
	
	public void setValueMatrizPost( int value, String idTransicao, String idLugar )
	{
		int i, j;
		
		// busca os indices
		for( j = 0; j < colunasIdTransicao.length; j++ )
			if( colunasIdTransicao[ j ].equals( idTransicao ) )
				break;			
		for( i = 0; i < linhasIdLugares.length; i++ )
			if( linhasIdLugares[ i ].equals( idLugar ) )
				break;
		
		if( j < colunasIdTransicao.length && i < linhasIdLugares.length )
			matrizPost[ i ][ j ] = value;			
	} // fim de m�todo
		
	public String[] getColunasIdTransicao() 
	{
		return colunasIdTransicao;
	}

	public String[] getLinhasIdLugares() 
	{
		return linhasIdLugares;
	}

	public Map<String, RedeHierarquica> getrelacaoHierarquicaMap() {
		return relacaoHierarquicaMap;
	}		

	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public void setValorFichaInicialNoLugar( String idLugar, int valorFicha )
	{
		for( int i = 0; i < linhasIdLugares.length; i++ )
			if( this.linhasIdLugares[ i ].equals( idLugar ) )
			{
				marcacaoInicial[ i ] = valorFicha;
				break;
			} // fim de if
	} // fim de m�todo
	
	public int getValorFichaInicialNoLugar( String idLugar )
	{
		for( int i = 0; i < linhasIdLugares.length; i++ )
			if( this.linhasIdLugares[ i ].equals( idLugar ) )
				return marcacaoInicial[ i ];
		return -1;
	} // fim de m�todo

	public void setValorFichaAtualNoLugar( String idLugar, int valorFicha )
	{
		for( int i = 0; i < linhasIdLugares.length; i++ )
			if( this.linhasIdLugares[ i ].equals( idLugar ) )
			{
				marcacaoAtual[ i ] = valorFicha;
				break;
			} // fim de if
	} // fim de m�todo
	
	public int getValorFichaAtualNoLugar( String idLugar )
	{
		for( int i = 0; i < linhasIdLugares.length; i++ )
			if( this.linhasIdLugares[ i ].equals( idLugar ) )
				return marcacaoAtual[ i ];
		return -1;
	} // fim de m�todo
	
	public void setMarcacaoInicial( int[] marcacaoInicial )
	{
		for( int i = 0; i < this.marcacaoInicial.length; i++ )
			this.marcacaoInicial[ i ] = marcacaoInicial[ i ];
	}
	
	public void setMarcacaoAtual( int[] marcacaoAtual )
	{
		for( int i = 0; i < this.marcacaoAtual.length; i++ )
			this.marcacaoAtual[ i ] = marcacaoAtual[ i ];
	}
	
	public int[] getMarcacaoInicial() 
	{
		return marcacaoInicial;
	}

	public int[] getMarcacaoAtual() 
	{
		return marcacaoAtual;
	}

	public Map<String, RedeHierarquica> getRelacaoHierarquicaMap() {
		return relacaoHierarquicaMap;
	}

	public List<String> getTransicoesDeNivel()
	{
		List<String> lista = new ArrayList<String>();
		
		Iterator<String> it = relacaoHierarquicaMap.keySet().iterator();
		while( it.hasNext() )
			lista.add( it.next() );
		
		return lista;
	} // fim de m�todo
	
	private String toString( List<String> redes )
	{
		String text = "------------------\n";
		
		text += String.format( "Nome: %s\n", nome );
		text += "Marcacao Atual: [ ";
		for( int i = 0; i < marcacaoAtual.length; i++ )
			text += "" + marcacaoAtual[ i ] + " ";
		text += "]\nMarcacao Inicial: [ ";
		for( int i = 0; i < marcacaoInicial.length; i++ )
			text += "" + marcacaoInicial[ i ] + " ";
		text += "]\n";
		text += "Relacoes hierarquicas: ";
		Iterator<String> it = relacaoHierarquicaMap.keySet().iterator();
		while( it.hasNext() )
		{
			String transicao = it.next();
			String rede = relacaoHierarquicaMap.get( transicao ).getNome();
			
			text += String.format( "%s -> %s    ", transicao, rede );
		} // fim de while

		text += String.format( "\nMatriz Pre: \n%16s", "" );
		for( int j = 0; j < colunasIdTransicao.length; j++ )
		{
			text += String.format( "%16s", colunasIdTransicao[ j ] );
		} // fim de for

		for( int i = 0; i < linhasIdLugares.length; i++ )
		{
			text += String.format( "\n%16s", linhasIdLugares[ i ] );
			for( int j = 0; j < colunasIdTransicao.length; j++ )
			{
				text += String.format( "%16d", matrizPre[ i ][ j ] );				
			} // fim de for
		} // fim de for

		text += String.format( "\nMatriz Post: \n%16s", "" );
		for( int j = 0; j < colunasIdTransicao.length; j++ )
		{
			text += String.format( "%16s", colunasIdTransicao[ j ] );
		} // fim de for
		
		for( int i = 0; i < linhasIdLugares.length; i++ )
		{
			text += String.format( "\n%16s", linhasIdLugares[ i ] );
			for( int j = 0; j < colunasIdTransicao.length; j++ )
			{
				text += String.format( "%16d", matrizPost[ i ][ j ] );				
			} // fim de for
		} // fim de for
		
		text += "\n";		
		it = relacaoHierarquicaMap.keySet().iterator();
		redes.add( getNome() );
		while( it.hasNext() )
		{
			String transicao = it.next();
			if( !redes.contains( relacaoHierarquicaMap.get( transicao ).getNome() ) )
				text += relacaoHierarquicaMap.get( transicao ).toString( redes );
		} // fim de while
		
		return text;
	} // fim de m�todo
	
	public String toString()
	{
		return this.toString( new ArrayList<String>() );
	}
	
	public RedeHierarquica getSubRedeHierarquica( String nome )
	{
		return getSubRedeHierarquica( nome, new ArrayList<String>() );
	} 
	
	private RedeHierarquica getSubRedeHierarquica( String nome, List<String> redes )
	{
		if( this.getNome().equals( nome ) )
			return this;
		
		redes.add( getNome() );
		Iterator<String> it = relacaoHierarquicaMap.keySet().iterator();
		while( it.hasNext() )
		{
			String transicao = it.next();
			RedeHierarquica r = relacaoHierarquicaMap.get( transicao );
			
			if( !redes.contains( r.getNome() ) )
			{
				r = r.getSubRedeHierarquica( nome, redes );
				if( r != null )
					return r;
			} // fim de if
		} // fim de while
		
		return null;
	} // fim de m�todo
	
	private static RedeHierarquica getRedeHierarquica( RedeProperties redeProperties, List<RedeHierarquica> redesHierarquicas )
	{
		String colunasIdTransicao[] = new String[ redeProperties.getTransicoesPropertiesList().size() ];
		String linhasIdLugares[] = new String[ redeProperties.getLugaresPropertiesList().size() ];
		
		RedeHierarquica redeHierarquica = new RedeHierarquica( redeProperties.getNome(), colunasIdTransicao, linhasIdLugares );
		
		// inicializa os lugares
		for( int i = 0; i < redeProperties.getLugaresPropertiesList().size(); i++ )
			linhasIdLugares[ i ] = redeProperties.getLugaresPropertiesList().get( i ).getNome();
		
		// inicializa as transi��es
		for( int j = 0; j < redeProperties.getTransicoesPropertiesList().size(); j++ )
			colunasIdTransicao[ j ] = redeProperties.getTransicoesPropertiesList().get( j ).getNome();
		
		// define as matrizes pre e post
		for( int j = 0; j < redeProperties.getTransicoesPropertiesList().size(); j++ )
		{
			String transicao = redeProperties.getTransicoesPropertiesList().get( j ).getNome();
			
			// define valores da matriz pre
			String lugar = redeProperties.getTransicoesPropertiesList().get( j ).getLugarOrigemProperties().getNome();		
			int valor = redeProperties.getTransicoesPropertiesList().get( j ).getPreDisparoTransicao().getEstadoLugarOrigem().getValor();
			redeHierarquica.setValueMatrizPre( valor, transicao, lugar );
			
			lugar = redeProperties.getTransicoesPropertiesList().get( j ).getLugarDestinoProperties().getNome();
			valor = redeProperties.getTransicoesPropertiesList().get( j ).getPreDisparoTransicao().getEstadoLugarDestino().getValor();
			redeHierarquica.setValueMatrizPre( valor, transicao, lugar );
			
			// define valores da matriz post
			lugar = redeProperties.getTransicoesPropertiesList().get( j ).getLugarOrigemProperties().getNome();			
			valor = redeProperties.getTransicoesPropertiesList().get( j ).getPosDisparoTransicao().getEstadoLugarOrigem().getValor();
			redeHierarquica.setValueMatrizPost( valor, transicao, lugar );
						
			lugar = redeProperties.getTransicoesPropertiesList().get( j ).getLugarDestinoProperties().getNome();
			valor = redeProperties.getTransicoesPropertiesList().get( j ).getPosDisparoTransicao().getEstadoLugarDestino().getValor();
			redeHierarquica.setValueMatrizPost( valor, transicao, lugar );
		} // fim de for
				
		// busca a marca��o inicial
		for( int i = 0; i < redeProperties.getLugaresPropertiesList().size(); i++ )
		{
			String lugar = redeProperties.getLugaresPropertiesList().get( i ).getNome();
			int valor = 0;
			
			if( redeProperties.getLugaresPropertiesList().get( i ).getEstadoInicialLugar() != null )
				valor = redeProperties.getLugaresPropertiesList().get( i ).getEstadoInicialLugar().getValor();

			redeHierarquica.setValorFichaInicialNoLugar( lugar, valor );
			redeHierarquica.setValorFichaAtualNoLugar( lugar, valor );
		} // fim de for
		
		// configura hierarquia
		redesHierarquicas.add( redeHierarquica );
		for( int j = 0; j < redeProperties.getTransicoesPropertiesList().size(); j++ )
		{
			if( redeProperties.getTransicoesPropertiesList().get( j ).getRedeNivelInferior() != null )
			{
				String transicao = redeProperties.getTransicoesPropertiesList().get( j ).getNome();
				RedeHierarquica r = getRedeHierarquica( redeProperties.getTransicoesPropertiesList().get( j ).getRedeNivelInferior().getNome(), 
										redesHierarquicas );
				
				if( r == null )
					r = getRedeHierarquica( redeProperties.getTransicoesPropertiesList().get( j ).getRedeNivelInferior(), redesHierarquicas );
				
				redeHierarquica.addRelacaoHierarquica( transicao, r );
			} // fim de if
		} // fim de for
		
		return redeHierarquica;
	} // fim de m�todo
	
	private static RedeHierarquica getRedeHierarquica( String nomeRede, List<RedeHierarquica> redesHierarquicas )
	{
		for( RedeHierarquica r : redesHierarquicas )
			if( r.getNome().equals( nomeRede ) )
				return r;
		
		return null;
	} // fim de for
	
	public static RedeHierarquica getRedeHierarquica( RedeProperties redeProperties )
	{
		return getRedeHierarquica( redeProperties, new ArrayList<RedeHierarquica>() );
	} // fim de m�todo
	
} // fim de classe RedeHierarquica
