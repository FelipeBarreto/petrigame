package br.ufc.petrigame.netObjects;

import java.io.Serializable;

/**
 * @author Joel
 */
public class TransicaoProperties implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private PreDisparoTransicao preDisparoTransicao;
	private PosDisparoTransicao posDisparoTransicao;
	private LugarProperties lugarOrigemProperties;
	private LugarProperties lugarDestinoProperties;
	private RedeProperties redeNivelInferior;
	private Audio efeitoSonoro;
	
	public TransicaoProperties()
	{		
	}
	
	public TransicaoProperties(String nome, RedeProperties redeNivelInferior) 
	{
		this.nome = nome;
		this.setRedeNivelInferior(redeNivelInferior);
	}
	
	public String getNome() 
	{
		return nome;
	}

	public void setNome(String nome) 
	{
		this.nome = nome;
	}

	public PosDisparoTransicao getPosDisparoTransicao() 
	{
		return posDisparoTransicao;
	}

	public void setPosDisparoTransicao(PosDisparoTransicao posDisparoTransicao) 
	{
		this.posDisparoTransicao = posDisparoTransicao;
	}

	public LugarProperties getLugarOrigemProperties() 
	{
		return lugarOrigemProperties;
	}

	public void setLugarOrigemProperties(LugarProperties lugarOrigemProperties) 
	{
		this.lugarOrigemProperties = lugarOrigemProperties;
	}

	public LugarProperties getLugarDestinoProperties() 
	{
		return lugarDestinoProperties;
	}

	public void setLugarDestinoProperties(LugarProperties lugarDestinoProperties) 
	{
		this.lugarDestinoProperties = lugarDestinoProperties;
	}

	public Audio getEfeitoSonoro() 
	{
		return efeitoSonoro;
	}

	public void setEfeitoSonoro(Audio efeitoSonoro) 
	{
		this.efeitoSonoro = efeitoSonoro;
	}

	public RedeProperties getRedeNivelInferior() 
	{
		return redeNivelInferior;
	}

	public void setRedeNivelInferior(RedeProperties redeNivelInferior) 
	{
		this.redeNivelInferior = redeNivelInferior;
	}

	public PreDisparoTransicao getPreDisparoTransicao() 
	{
		return preDisparoTransicao;
	}

	public void setPreDisparoTransicao(PreDisparoTransicao preDisparoTransicao) 
	{
		this.preDisparoTransicao = preDisparoTransicao;
	}

} // fim de classe
