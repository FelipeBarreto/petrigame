package br.ufc.petrigame;

import br.ufc.petrigame.helpers.AssetLoader;
import br.ufc.petrigame.ui.SplashScreen;

import com.badlogic.gdx.Game;

public class PetriGame extends Game {

	@Override
	public void create() {		
		AssetLoader.loadMisc();
		setScreen(new SplashScreen(this));		
	}
}
