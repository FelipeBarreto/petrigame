package br.ufc.petrigame.messages;

import java.io.Serializable;

public class ClientMessage implements Serializable {

	/**
	 * @author FelipeBarreto
	 */
	private static final long serialVersionUID = 1L;

        private String netName;
        private String gameName;
        private String playerFigure;
        private int playerPositionX, playerPositionY;
	
	public ClientMessage(String netName, String gameName, String figure, int x, int y){
		this.gameName = gameName;
        this.netName = netName;
        this.playerPositionX = x;
        this.playerPositionY = y;
        this.playerFigure = figure;
	}

    public String getPlayerFigure() {
        return playerFigure;
    }

    public int getPlayerPositionX() {
        return playerPositionX;
    }

    public int getPlayerPositionY() {
        return playerPositionY;
    }

    public String getNetName() {
        return netName;
    }
    
    public String getGameName() {
    	return gameName;
    }
		
}

