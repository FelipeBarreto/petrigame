/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufc.petrigame.messages;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Felipe
 */
public class ServerMessage implements Serializable{

    private static final long serialVersionUID = 1L;

    private final Map<String, ClientMessage> playersGames;

    public ServerMessage(){
            playersGames = Collections.synchronizedMap(new HashMap<String, ClientMessage>());
    }

    public Map<String, ClientMessage> getPlayersGame() {
            return playersGames;
    }

    public void add(String client, ClientMessage petriGame) {
           playersGames.put(client, petriGame);
    }
    
    public void remove(String client){
        playersGames.remove(client);
    }
    
    public void clear(){
        playersGames.clear();
    }

}
