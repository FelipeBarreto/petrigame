/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package br.ufc.petrigame.ui;

import java.awt.Checkbox;

import br.ufc.petrigame.PetriGame;
import br.ufc.petrigame.screens.LoadingScreen;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;

public class GameChoose implements Screen {
	Object[] listEntries;
	Skin skin;
	Stage stage;
	CheckBox checkBox;
	Texture gameImage;
	List gameList;
	
	PetriGame myGame;
	
	public GameChoose(PetriGame game){
		myGame = game;
		create();
	}

	public void create() {
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		gameImage = getGameImage();
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		initListEntries();
		initUI();
	}
	
	private void initListEntries() {
		
		Array<String> gameNames = new Array<String>();
		if(Gdx.app.getType() == ApplicationType.WebGL || Gdx.app.getType() == ApplicationType.Applet){
			gameNames.add("rede5.cpn");
		}
		else{
			FileHandle[] list = Gdx.files.internal("data/Games").list(".cpn");
			if(list.length != 0){
				for(FileHandle file : list){
					gameNames.add(file.name());
				}
			}
			else{
				gameNames.add("rede4.cpn");
				gameNames.add("rede5.cpn");
				gameNames.add("rede6.cpn");
			}
		}

		
		listEntries = gameNames.toArray();
	}

	private Texture getGameImage() {
		Texture gameImage = new Texture(Gdx.files.internal("data/imageNotFound.jpg"));
		gameImage.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		return gameImage;
	}

	public void initUI(){
		Table table = new Table();
		table.setFillParent(true);
		table.top();
		table.pad(15.0f, 15.0f, 15.0f, 15.0f);
		stage.addActor(table);
		
		gameList = new List(listEntries, skin);
		ScrollPane listScroll = new ScrollPane(gameList);
		
		table.add(listScroll)
			.left()
			.top()
			.width((Gdx.graphics.getWidth()/2) - 10)
			.height((Gdx.graphics.getHeight()/2) -10)
			.fill()
			.expandX();
		
		table.add(new Image(new TextureRegion(gameImage)))
			.left()
			.fill()
			.expandX()
			.pad(.0f, 5.0f, .0f, 5.0f)
			.width((Gdx.graphics.getWidth()/2) - 10)
			.height((Gdx.graphics.getHeight()/2) -10);
		
		table.row();
		TextButton gameChoose = new TextButton("Escolher Este Jogo",skin);
		table.add(gameChoose)
			.center()
			.padTop(Gdx.graphics.getHeight()/10);
		
		checkBox = new CheckBox("Usar Acelerômetro?", skin);
		table.add(checkBox);
		
		//Handlers
		
		gameList.addListener(new ChangeListener(){

			@Override
			public void changed(ChangeEvent event, Actor actor) {
			
			}
			
		});
		
		gameChoose.addListener(new ChangeListener(){

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				String gameName = gameList.getSelection();
				if(gameName != null){				
					myGame.setScreen(new LoadingScreen(gameName, myGame, checkBox.isChecked()));
				}
			}
			
		});
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
		Table.drawDebug(stage);
	}

	@Override
	public void resize (int width, int height) {
		create();
		//stage.setViewport(width, height, false);
		//initUI();
		
	}

	@Override
	public void dispose () {
		stage.dispose();
		skin.dispose();
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
}
