package br.ufc.petrigame.gameworld;

import java.util.ArrayList;
import java.util.List;

import br.ufc.petrigame.helpers.AssetLoader;
import br.ufc.petrigame.messages.ClientMessage;
import br.ufc.petrigame.multiplayer.ServerHandler;
import br.ufc.petrigame.netObjects.DrawGameInterface;
import br.ufc.petrigame.netObjects.EstadoLugar;
import br.ufc.petrigame.netObjects.LugarProperties;
import br.ufc.petrigame.netObjects.Personagem;
import br.ufc.petrigame.netObjects.PlayGame;
import br.ufc.petrigame.netObjects.RedeHierarquica;
import br.ufc.petrigame.netObjects.RedeProperties;
import br.ufc.petrigame.screens.LoadingScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class GameRenderer implements DrawGameInterface{
	
	private GameWorld myWorld;
	
	private PlayGame playGame;
	private RedeProperties redeProperties;
	private List<ClientMessage> playersGames;
	
	private OrthographicCamera cam;
	private ShapeRenderer shapeRenderer;
	private SpriteBatch batcher;
	
	private List<Vector2> posicoesLugar = new ArrayList<Vector2>();

	public GameRenderer(GameWorld world){
		myWorld = world;
		
		cam = new OrthographicCamera();
		cam.setToOrtho(true, 400, 300);
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);
		batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);
		
		initGameObjects();
	}
	
	private void initGameObjects() {
		playGame = myWorld.getPlayGame();
		redeProperties = myWorld.getRedeProperties();
	}

	public void render(){
		Gdx.gl.glClearColor(0 / 255.0f, 0 / 255.0f, 0 / 255.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		redeProperties = myWorld.getRedeProperties();
		playersGames = myWorld.getPlayersGames();

		renderGame(playGame.getRedeHierarquica().getSubRedeHierarquica( redeProperties.getNome() ));	
		if(!myWorld.useAccel()){
			renderControls();			
		}
		
		if(playGame.isGameOver()){
			renderGameOver();
		}
	}

	private void renderMultiPlayer(List<ClientMessage> playersGames2) {
		if(playersGames2 != null && ServerHandler.isConnected()){
			if(!playersGames2.isEmpty()){
				batcher.begin();
				batcher.enableBlending();
				for(ClientMessage message : playersGames2){
					if(message == null)
						continue;
					if(message.getNetName().equals(redeProperties.getNome())){
						batcher.draw(AssetLoader.personagem.get(message.getPlayerFigure()), 
							     message.getPlayerPositionX() - 10, 
							     message.getPlayerPositionY() - 10, 
							     10.0f, 
							     10.0f, 
							     20.0f, 
							     20.0f, 
							     1, 
							     1, 
							     0);
					}
				}
				batcher.end();
			}	
		}
	}

	private void renderGameOver() {
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(50 / 255.0f, 50 / 255.0f, 50 / 255.0f, 0.4f);
		shapeRenderer.rect(0, 0, 400, 300);
		shapeRenderer.end();
		
		Gdx.gl.glDisable(GL10.GL_BLEND);
		
		batcher.begin();
		batcher.draw(AssetLoader.gameOver, cam.position.x - 75, cam.position.y - 64);
		batcher.end();
	}

	private void renderGame(RedeHierarquica redeHierarquica) {
		computePositions1Mundo( redeProperties );
		
		batcher.setProjectionMatrix(cam.combined);
		for( int i = 0, k = 0; i < redeProperties.getNumeroLinhas(); i++ )
		{
			for( int j = 0; j < redeProperties.getNumeroColunas(); j++, k++ )
			{	
				LugarProperties lugarProperties = redeProperties.getLugarProperties( i, j );	
				
				if( lugarProperties != null )
				{
					int valorFicha = redeHierarquica.getValorFichaAtualNoLugar( lugarProperties.getNome() );					
					EstadoLugar estadoLugar = lugarProperties.getEstadoLugar( valorFicha );
					
					// desenha o lugar corrente			
					if( estadoLugar != null )
					{		
						batcher.begin();
						batcher.enableBlending();
						batcher.draw(AssetLoader.lugares.get(estadoLugar.getNome()), 
								    (float) posicoesLugar.get( k ).x, 
								    (float) posicoesLugar.get( k ).y, 
								    75.0f,
								    75.0f,
								    150.0f, 
								    150.0f,
								    1,
								    1,
								    180.0f);
						batcher.end();
					
						// desenha o objeto
						if( estadoLugar.getObjetoPresente() != null )
						{
							//FIXME
						} // fim de if
					}
					
					// verifica se h� vizinho da esquerda e existe alguma transi��o de substitui��o
					if( redeProperties.existsTransicaoNivel( lugarProperties, redeProperties.getLugarProperties( i, j - 1 ) ) )
					{
						int l = ( int ) ( 0.25 * 150 );
						String transicaoNivelHorizontal = redeProperties.getTransicaoSubstituicaHorizontalPath();
						batcher.begin();
						batcher.enableBlending();
						batcher.draw(AssetLoader.transicoes.get(transicaoNivelHorizontal), 
								    (float) posicoesLugar.get( k ).x -l/2, 
								    (float) posicoesLugar.get( k ).y, 
								    l/2,
								    75.0f,
								    l, 
								    150.0f,
								    1,
								    1,
								    180.0f);
						batcher.end();
						
					} // fim de if
					// verifica se h� vizinho de cima e existe alguma transi��o de substitui��o
					if( redeProperties.existsTransicaoNivel( lugarProperties, redeProperties.getLugarProperties( i - 1, j ) ) )
					{
						int a = ( int ) ( 0.25 * 150 );
						String transicaoNivelVertical = redeProperties.getTransicaoSubstituicaVerticalPath();
						batcher.begin();
						batcher.enableBlending();
						batcher.draw(AssetLoader.transicoes.get(transicaoNivelVertical), 
								    (float) posicoesLugar.get( k ).x, 
								    (float) posicoesLugar.get( k ).y - a/2, 
								    75.0f,
								    a/2,
								    150.0f, 
								    a,
								    1,
								    1,
								    180.0f);
						batcher.end();
					} // fim de if
					// verifica o vizinho da diagonal superior-esquerda
					if( redeProperties.existsTransicaoNivel( lugarProperties, redeProperties.getLugarProperties( i - 1, j - 1 ) ) )
					{
						int a = ( int ) ( 0.25 * 150 );
						String transicaoNivelVertical = redeProperties.getTransicaoSubstituicaVerticalPath();
						batcher.begin();
						batcher.enableBlending();
						batcher.draw(AssetLoader.transicoes.get(transicaoNivelVertical), 
								    (float) posicoesLugar.get( k ).x - a/2, 
								    (float) posicoesLugar.get( k ).y - a/2, 
								    a/2,
								    a/2,
								    a, 
								    a,
								    1,
								    1,
								    180.0f);
						batcher.end();
					} // fim de if
					// verifica o vizinho da diagonal superior-direita
					if( redeProperties.existsTransicaoNivel( lugarProperties, redeProperties.getLugarProperties( i - 1, j + 1 ) ) )
					{
						int a = ( int ) ( 0.25 * 150 );
						String transicaoNivelVertical = redeProperties.getTransicaoSubstituicaVerticalPath();
						batcher.begin();
						batcher.enableBlending();
						batcher.draw(AssetLoader.transicoes.get(transicaoNivelVertical), 
								    (float) posicoesLugar.get( k ).x + 150 - a/2, 
								    (float) posicoesLugar.get( k ).y - a/2, 
								    a/2,
								    a/2,
								    a, 
								    a,
								    1,
								    1,
								    180.0f);
						batcher.end();
					} // fim de if
				} 
			} 
		} 
		
		//Desenha os outros personagens (Multiplayer)
		renderMultiPlayer(playersGames);

		// desenha o personagem
		for( int i = 0, k = 0; i < redeProperties.getNumeroLinhas(); i++ )
		{
			for( int j = 0; j < redeProperties.getNumeroColunas(); j++, k++ )
			{	
				LugarProperties lugarProperties = redeProperties.getLugarProperties( i, j );	
				
				if( lugarProperties != null )
				{
					int valorFicha = redeHierarquica.getValorFichaAtualNoLugar( lugarProperties.getNome() );					
					EstadoLugar estadoLugar = lugarProperties.getEstadoLugar( valorFicha );
					
					// desenha o personagem
					if( estadoLugar != null && estadoLugar.getPersonagemPresente1() != null )
						drawPersonagem(estadoLugar.getPersonagemPresente1(), 
								       1, 
								       (int) (posicoesLugar.get( k ).x +  ( myWorld.getPercentualXRelPlayer1() * 150.0f )), 
								       (int) (posicoesLugar.get( k ).y +  ( myWorld.getPercentualYRelPlayer1() * 150.0f )), 
							           (int) 150.0f, 
							           (int) 150.0f );
				} // fim de if
			} // fim de for
		} // fim de for
	}
	
	public void drawPersonagem(Personagem personagem, int idPersonagem, int x, int y, int width, int height )
	{
		String img = "";
		
		batcher.begin();
		batcher.enableBlending();
		
		if( idPersonagem == 1 )
		{
			// recupera a figura
			if( playGame.getDirecaoCorrente1() == DIRECTION_UP )
			{
				if( playGame.getUp1() == 0 && personagem.getFiguraCima1() != null )
					img = personagem.getFiguraCima1();
				if( ( img.equals( "" ) || playGame.getUp1() == 1 ) && ( personagem.getFiguraCima2() != null ) )
					img = personagem.getFiguraCima2();
				if( ( img.equals( "" ) || playGame.getUp1() == 2 ) && ( personagem.getFiguraCima3() != null ) )
					img = personagem.getFiguraCima3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_DOWN )
			{
				if( playGame.getDown1() == 0 && personagem.getFiguraBaixo1() != null )
					img = personagem.getFiguraBaixo1();
				if( ( img.equals( "" ) || playGame.getDown1() == 1 ) && personagem.getFiguraBaixo2() != null )
					img = personagem.getFiguraBaixo2();
				if( ( img.equals( "" ) || playGame.getDown1() == 2 ) && personagem.getFiguraBaixo3() != null )
					img = personagem.getFiguraBaixo3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_RIGHT )
			{
				if( playGame.getRight1() == 0 && personagem.getFiguraDireita1() != null )
					img = personagem.getFiguraDireita1();
				if( ( img.equals( "" ) || playGame.getRight1() == 1 ) && personagem.getFiguraDireita2() != null )
					img = personagem.getFiguraDireita2();
				if( ( img.equals( "" ) || playGame.getRight1() == 2 ) && personagem.getFiguraDireita3() != null )
					img = personagem.getFiguraDireita3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_LEFT )
			{
				if( playGame.getLeft1() == 0 && personagem.getFiguraEsquerda1() != null )
					img = personagem.getFiguraEsquerda1();
				if( ( img.equals( "" ) || playGame.getLeft1() == 1 ) && personagem.getFiguraEsquerda2() != null )
					img = personagem.getFiguraEsquerda2();
				if( ( img.equals( "" ) || playGame.getLeft1() == 2 ) && personagem.getFiguraEsquerda3() != null )
					img = personagem.getFiguraEsquerda3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_RIGHT_TOP )
			{
				if( playGame.getRigthTop1() == 0 && personagem.getFiguraDiagSupDir1() != null )
					img = personagem.getFiguraDiagSupDir1();
				if( ( img.equals( "" ) || playGame.getRigthTop1() == 1 ) && personagem.getFiguraDiagSupDir2() != null )
					img = personagem.getFiguraDiagSupDir2();
				if( ( img.equals( "" ) || playGame.getRigthTop1() == 2 ) && personagem.getFiguraDiagSupDir3() != null )
					img = personagem.getFiguraDiagSupDir3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_LEFT_TOP )
			{
				if( playGame.getLeftTop1() == 0 && personagem.getFiguraDiagSupEsq1() != null )
					img = personagem.getFiguraDiagSupEsq1();
				if( ( img.equals( "" ) || playGame.getLeftTop1() == 1 ) && personagem.getFiguraDiagSupEsq2() != null )
					img = personagem.getFiguraDiagSupEsq2();
				if( ( img.equals( "" ) || playGame.getLeftTop1() == 2 ) && personagem.getFiguraDiagSupEsq3() != null )
					img = personagem.getFiguraDiagSupEsq3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_RIGHT_BOTTOM )
			{
				if( playGame.getRigthBottom1() == 0 && personagem.getFiguraDiagInfDir1() != null )
					img = personagem.getFiguraDiagInfDir1();
				if( ( img.equals( "" ) || playGame.getRigthBottom1() == 1 ) && personagem.getFiguraDiagInfDir2() != null )
					img = personagem.getFiguraDiagInfDir2();
				if( ( img.equals( "" ) || playGame.getRigthBottom1() == 2 ) && personagem.getFiguraDiagInfDir3() != null )
					img = personagem.getFiguraDiagInfDir3();
			} // fim de if
			else if( playGame.getDirecaoCorrente1() == DIRECTION_LEFT_BOTTOM )
			{
				if( playGame.getLeftBottom1() == 0 && personagem.getFiguraDiagInfEsq1() != null )
					img = personagem.getFiguraDiagInfEsq1();
				if( ( img.equals( "" ) || playGame.getLeftBottom1() == 1 ) && personagem.getFiguraDiagInfEsq2() != null )
					img = personagem.getFiguraDiagInfEsq2();
				if( ( img.equals( "" ) || playGame.getLeftBottom1() == 2 ) && personagem.getFiguraDiagInfEsq3() != null )
					img = personagem.getFiguraDiagInfEsq3();
			}
			if( img.equals("") ){
				img = personagem.getFiguraDefault();
			}
			
			batcher.draw(AssetLoader.personagem.get(img), 
					     x - 10, 
					     y - 10, 
					     10.0f, 
					     10.0f, 
					     20.0f, 
					     20.0f, 
					     1, 
					     1, 
					     0);
	} // fim de if
		batcher.end();
		cam.position.set((float)x,(float)y, 0);
		cam.update();
		
		if(ServerHandler.isConnected()){
			ServerHandler.getInstance(null).notifyServer(redeProperties.getNome(), LoadingScreen.gameName, img, x, y);
		}
	} // fim de m�todo

	private void computePositions1Mundo( RedeProperties redeProperties )
	{			
		posicoesLugar.clear();
		
		if( redeProperties != null )
		{
			// computa as posi��es dos retangulos
			for( int i = 0; i < redeProperties.getNumeroLinhas(); i++ )
			{
				for( int j = 0; j < redeProperties.getNumeroColunas(); j++ )
				{
					Vector2 p = new Vector2( j * 150, i * 150 );
					posicoesLugar.add( p );					
				} // fim de for interno
			} // fim de for
		} // fim de if		
	} // fim de m�todo

	private void renderControls() {
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(255 / 255.0f, 255 / 255.0f, 255 / 255.0f, 0.5f);
		shapeRenderer.triangle(15.0f, 245.0f, 40.0f, 232.5f, 40.0f, 257.5f);
		shapeRenderer.triangle(47.5f, 265.0f, 72.5f, 265.0f, 60.0f, 290.0f);
		shapeRenderer.triangle(80.0f, 257.5f, 105.0f, 245.0f, 80.0f, 232.5f);
		shapeRenderer.triangle(47.5f, 225.0f, 60.0f, 200.0f, 72.5f, 225.0f);
		
		shapeRenderer.end();
		
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		
	}
}
