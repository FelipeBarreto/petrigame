package br.ufc.petrigame.gameworld;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Peripheral;

import br.ufc.petrigame.helpers.AccelerometerListener;
import br.ufc.petrigame.messages.ClientMessage;
import br.ufc.petrigame.netObjects.PlayGame;
import br.ufc.petrigame.netObjects.RedeProperties;

public class GameWorld {
	
	public static final int DIRECTION_UP = 0;
	public static final int DIRECTION_DOWN = 1;
	public static final int DIRECTION_LEFT = 2;
	public static final int DIRECTION_RIGHT = 3;
	public static final int DIRECTION_TOP_RIGHT = 4;
	public static final int DIRECTION_TOP_LEFT = 5;
	public static final int DIRECTION_BOTTON_RIGHT = 6;
	public static final int DIRECTION_BOTTON_LEFT = 7;
	public static final int STOP_MOVING = -1;
	
	private PlayGame myGame;
	private AccelerometerListener accel;
	private boolean useAccel;
	
	private Map<String, ClientMessage> playersGames;
	private Map<String, ClientMessage> playersGamesCache;
	private Map<String, Integer> disconnectCount;
	private boolean needUpdateCache = false;
	
	private int direction = -1;
	
	public GameWorld(PlayGame game, boolean useAccel){
		myGame = game;
		if(Gdx.app.getType() == ApplicationType.Android)
			this.useAccel = useAccel;
		else
			this.useAccel = false;
		accel = new AccelerometerListener(this);
	
		playersGames = new HashMap<String, ClientMessage>();
		playersGamesCache = new HashMap<String, ClientMessage>();
		disconnectCount = new HashMap<String, Integer>();
	}
	
	public RedeProperties getRedeProperties(){
		return RedeProperties.getRedePropertiesPlayer1Presente( 
				myGame.getRedeProperties(), myGame.getRedeHierarquica() );
	}

	public void update(float delta){
		if(useAccel){
			accel.update(); //FIXME
		}
		
		if(direction == DIRECTION_UP){
			myGame.upPersonagem1();
		}
		if(direction == DIRECTION_DOWN){
			myGame.downPersonagem1();
		}
		if(direction == DIRECTION_LEFT){
			myGame.leftPersonagem1();
		}
		if(direction == DIRECTION_RIGHT){
			myGame.rightPersonagem1();
		}
		if(direction == DIRECTION_TOP_LEFT){
			myGame.leftTopPersonagem1();
		}
		if(direction == DIRECTION_TOP_RIGHT){
			myGame.rightTopPersonagem1();
		}
		if(direction == DIRECTION_BOTTON_LEFT){
			myGame.leftBottomPersonagem1();
		}
		if(direction == DIRECTION_BOTTON_RIGHT){
			myGame.rightBottomPersonagem1();
		}
	}

	public float getPercentualYRelPlayer1() {
		return (float) myGame.getPercentualYRelPlayer1();
	}

	public float getPercentualXRelPlayer1() {
		return (float) myGame.getPercentualXRelPlayer1();
	}

	public PlayGame getPlayGame() {
		return myGame;
	}
	
	public void setDirection(int d){
		direction = d;
	}

	public void pause() {
		// TODO Auto-generated method stub
		
	}

	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
	public void updateMultiPlayer(Map<String, ClientMessage> playersGames){
		this.playersGames = playersGames;

		if(playersGamesCache.isEmpty()){
			playersGamesCache = new HashMap<String, ClientMessage>(playersGames);
		}
	}
	
	public List<ClientMessage> getPlayersGames(){
		List<ClientMessage> list = new ArrayList<ClientMessage>();
		ArrayList<String> copy = new ArrayList<String>(playersGames.keySet());
		for(String key : copy){
			list.add(playersGames.get(key));
		}
		
		ArrayList<String> cachedCopy = new ArrayList<String>(playersGamesCache.keySet());	
		for(String key : cachedCopy){
			if(!playersGames.containsKey(key)){
				if(!disconnectCount.containsKey(key)){
					disconnectCount.put(key, 1);
				}
				else{
					disconnectCount.put(key, disconnectCount.get(key) + 1);
					
					if(disconnectCount.get(key) == 10){
						System.out.println("renew");
						disconnectCount.remove(key);
						needUpdateCache = true;
					}
					else{
						list.add(playersGamesCache.get(key));
					}
				}
				
			}
		}
		
		if(needUpdateCache){
			playersGamesCache = new HashMap<String, ClientMessage>(playersGames);
			needUpdateCache = false;
		}

		return Collections.unmodifiableList(list);
	}
	
	public boolean useAccel(){
		return useAccel;
	}

}
